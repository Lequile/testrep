﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PSV.Utility
{
    /// <summary>
    /// Safe area implementation for notched mobile devices. Usage:
    ///  (1) Add this component to the top level of any GUI panel. 
    ///  (2) If the panel uses a full screen background image, then create an immediate child and put the component on that instead, with all other elements childed below it.
    ///      This will allow the background image to stretch to the full extents of the screen behind the notch, which looks nicer.
    ///  (3) For other cases that use a mixture of full horizontal and vertical background stripes, use the Ignore X & Y controls on separate elements as needed.
    ///  (4) If refresh maunual dont forget update you rect.
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(RectTransform))]
    [AddComponentMenu("Layout/SafeArea Container")]
    public class SafeAreaContainer : MonoBehaviour
    {
        public static float left => MinSafeAreaWorld.x;
        
        public static float right => MaxSafeAreaWorld.x;

        public static float bottom => MinSafeAreaWorld.y;

        public static float top => MaxSafeAreaWorld.y;

        public static Vector3 CenterSafeAreaWorld => GetScreen.FindCamera().ScreenToWorldPoint(SafeArea.center);

        public static Vector3 MinSafeAreaWorld => GetScreen.FindCamera().ScreenToWorldPoint(SafeArea.position);

        public static Vector3 MaxSafeAreaWorld
        {
            get
            {
                var safeArea = SafeArea;
                return GetScreen.FindCamera().ScreenToWorldPoint(safeArea.position + safeArea.size);
            }
        }

        public static Rect SafeArea
        {
            get
            {
#if UNITY_EDITOR
                return SafeAreaSimulation.safeArea;
#else
                return Screen.safeArea;
#endif
            }
        }

        [UnityEngine.Serialization.FormerlySerializedAs( "IgnoreX" )]
        public bool ignoreX;
        [UnityEngine.Serialization.FormerlySerializedAs( "IgnoreY" )]
        public bool ignoreY;

        private RectTransform container;

        private Rect lastSafeArea = Rect.zero;
        //private ScreenOrientation lastOrientation;

        private void Awake()
        {
            container = GetComponent<RectTransform>();

            if (container == null)
            {
                Debug.LogError( "Cannot apply safe area - no RectTransform found on " + name );
                return;
            }

            Refresh();
        }

        private void Update()
        {
            Refresh();
        }

        public void Refresh()
        {
            if (SafeArea != lastSafeArea )//|| Screen.orientation != lastOrientation)
            {
                //lastOrientation = Screen.orientation;
                lastSafeArea = SafeArea;
                // Use lastSafeArea to skip double calculate safe area rect
                Refresh( lastSafeArea, container, ignoreX, ignoreY );
            }
        }

        public static void Refresh( RectTransform rect, bool IgnoreX = false, bool IgnoreY = false )
        {
            Refresh( SafeArea, rect, IgnoreX, IgnoreY );
        }

        private static void Refresh( Rect safeArea, RectTransform rect, bool IgnoreX, bool IgnoreY )
        {
            int w = Screen.width;
            int h = Screen.height;

            if (IgnoreX)
            {
                safeArea.x = 0;
                safeArea.width = w;
            }

            if (IgnoreY)
            {
                safeArea.y = 0;
                safeArea.height = h;
            }

            if (w > 0 && h > 0)
            {
                Vector2 anchorMin = safeArea.position;
                Vector2 anchorMax = safeArea.position + safeArea.size;

                anchorMin.x /= w;
                anchorMax.x /= w;
                anchorMin.y /= h;
                anchorMax.y /= h;

                rect.anchorMin = anchorMin;
                rect.anchorMax = anchorMax;

                // Fix for some Samsung devices (e.g. Note 10+, A71, S20) where Refresh gets called twice and the first time returns NaN anchor coordinates
                // See https://forum.unity.com/threads/569236/page-2#post-6199352
                if (anchorMin.x >= 0 && anchorMin.y >= 0 && anchorMax.x >= 0 && anchorMax.y >= 0)
                {
                    rect.anchorMin = anchorMin;
                    rect.anchorMax = anchorMax;
                }
            }
            else
            {
                NextFrameCall.Add( () => { Refresh( rect, IgnoreX, IgnoreY ); } );
            }

            //Debug.LogFormat(rect, "New safe area applied to {0}: x={1}, y={2}, w={3}, h={4} on full extents w={5}, h={6}",
            //rect.name, safeArea.x, safeArea.y, safeArea.width, safeArea.height, Screen.width, Screen.height);
        }
    }
}