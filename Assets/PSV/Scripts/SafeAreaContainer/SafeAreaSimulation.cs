﻿#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEngine;

namespace PSV.Utility
{
    [AddComponentMenu( "" )]
    public sealed class SafeAreaSimulation : AgentObject<SafeAreaSimulation>
    {
#pragma warning disable 67
        public static event Action onUpdateSafeAreaSimulation;
#pragma warning restore 67

        private const string deviceIndexPref = "psv_safeAreaDevice";
        private const string switchKeyPref = "psv_safeAreaSwitchKey";

        private readonly DeviceInfo[] devices = CreateDeviceList();
        private int currentDeviceIndex;
        private KeyCode currentKeyCode;

        public static DeviceInfo[] CreateDeviceList()
        {
            return new DeviceInfo[]
            {
            /// <summary>
            /// Simulation mode for use in editor only. This can be edited at runtime to toggle between different safe areas.
            /// </summary>
            new DeviceInfo("None", 0, 0, Rect.zero, Rect.zero),
            /// <summary>
            /// Normalised safe areas for iPhone X with Home indicator (ratios are identical to Xs, 11 Pro). Absolute values:
            ///  PortraitU x=0, y=102, w=1125, h=2202 on full extents w=1125, h=2436;
            ///  PortraitD x=0, y=102, w=1125, h=2202 on full extents w=1125, h=2436 (not supported, remains in Portrait Up);
            ///  LandscapeL x=132, y=63, w=2172, h=1062 on full extents w=2436, h=1125;
            ///  LandscapeR x=132, y=63, w=2172, h=1062 on full extents w=2436, h=1125.
            ///  Aspect Ratio: ~19.5:9.
            /// </summary>
            new DeviceInfo("iPhoneX", 1125, 2436, new Rect(0, 102, 0, 132), new Rect(132, 63, 0, 132)),
            /// <summary>
            /// Normalised safe areas for iPhone Xs Max with Home indicator (ratios are identical to XR, 11, 11 Pro Max). Absolute values:
            ///  PortraitU x=0, y=102, w=1242, h=2454 on full extents w=1242, h=2688;
            ///  PortraitD x=0, y=102, w=1242, h=2454 on full extents w=1242, h=2688 (not supported, remains in Portrait Up);
            ///  LandscapeL x=132, y=63, w=2424, h=1179 on full extents w=2688, h=1242;
            ///  LandscapeR x=132, y=63, w=2424, h=1179 on full extents w=2688, h=1242.
            ///  Aspect Ratio: ~19.5:9.
            /// </summary>
            //new DeviceInfo("iPhone Xs Max", 1242, 2454, new Rect(0, 102, 0, 132), new Rect(132, 63, 0, 132)),
            };
        }

        public static Rect safeArea
        {
            get
            {
                CheckAgent();
                return agent.devices[agent.currentDeviceIndex].SafeArea;
            }
        }

        public static int deviceIndex
        {
            get
            {
                if (agent)
                    return agent.currentDeviceIndex;
                return EditorPrefs.GetInt( deviceIndexPref, 0 );
            }
            set
            {
                EditorPrefs.SetInt( deviceIndexPref, value );
                if (agent)
                    agent.currentDeviceIndex = value;
            }
        }

        public static KeyCode switchKey
        {
            get { return ( KeyCode )EditorPrefs.GetInt( switchKeyPref, ( int )KeyCode.F10 ); }
            set
            {
                if (agent)
                    agent.currentKeyCode = value;
                EditorPrefs.SetInt( switchKeyPref, ( int )value );
                KeyListener.AddKey( value );
            }
        }


        [RuntimeInitializeOnLoadMethod( RuntimeInitializeLoadType.AfterSceneLoad )]
        private static void InitSimulation()
        {
            CheckAgent();
        }

        private new void Awake()
        {
            base.Awake();
            currentDeviceIndex = EditorPrefs.GetInt( deviceIndexPref, 0 );
            currentKeyCode = switchKey;
        }

        private void Update()
        {
            if (Input.GetKeyDown( currentKeyCode ))
                SimulateNextDevice();
        }

        private void OnGUI()
        {
            SafeAreaOnGUI();
        }

        private void SimulateNextDevice()
        {
            currentDeviceIndex = ( currentDeviceIndex + 1 ) % devices.Length;
            EditorPrefs.SetInt( deviceIndexPref, currentDeviceIndex );
            var safeAreaAlignments = FindObjectsOfType<SafeAreaAlignment>();

            foreach (var obj in safeAreaAlignments)
            {
                obj.SetAlignment();
            }
        }

        private void SafeAreaOnGUI()
        {
            var screenWidth = Screen.width;
            var screenHeight = Screen.height;
            if (currentDeviceIndex == 0)
            {
                var btnYPos = screenHeight * 0.65f;
                var btnHeight = screenHeight * 0.2f;
                if (GUI.Button( new Rect( 0.0f, btnYPos, screenWidth * 0.03f, btnHeight ),
                        "S\na\nf\ne\nON" ))
                    SimulateNextDevice();

                if (GUI.Button( new Rect( 0.0f, btnYPos + btnHeight + 2, screenWidth * 0.03f, screenHeight * 0.05f ), "X" ))
                    Destroy( this );
                return;
            }
            var device = devices[currentDeviceIndex];
            var safeArea = device.SafeArea;

            Vector2 safeAreaMin = safeArea.position;
            Vector2 safeAreaMax = safeArea.position + safeArea.size;

            //left
            if (safeAreaMin.x != 0)
            {
                if (GUI.Button( new Rect( 0, 0, safeAreaMin.x, screenHeight ),
                        "S\na\nf\ne\n\nA\nr\ne\na" ))
                    SimulateNextDevice();
            }
            //bottom
            if (safeAreaMin.y != 0)
            {
                if (GUI.Button( new Rect( safeAreaMin.x, screenHeight - safeAreaMin.y,
                                          screenWidth - safeAreaMin.x - safeAreaMin.x, safeAreaMin.y ), "" ))
                    SimulateNextDevice();
            }
            //right
            if (screenWidth - safeAreaMax.x > 0)
            {
                if (GUI.Button( new Rect( safeAreaMax.x, 0, screenWidth - safeAreaMax.x, screenHeight ),
                         "S\na\nf\ne\n\nA\nr\ne\na" ))
                    SimulateNextDevice();
            }
            //top
            if (screenHeight - safeAreaMax.y > 0)
            {
                if (GUI.Button( new Rect( safeAreaMin.x, 0, screenWidth - safeAreaMin.x - safeAreaMin.x, screenHeight - safeAreaMax.y ), "" ))
                {
                    SimulateNextDevice();
                }
            }
        }

        public class DeviceInfo
        {
            public readonly string name;
            public readonly Rect portraitSafeArea;
            public readonly Rect landscapeSafeArea;

            public DeviceInfo( string name, int width, int height, Rect portrait, Rect landscape )
            {
                this.name = name;
                if (width == 0 || height == 0)
                {
                    portraitSafeArea = Rect.zero;
                    landscapeSafeArea = Rect.zero;
                    return;
                }
                portraitSafeArea = new Rect(
                    x: portrait.x / width,
                    y: portrait.y / height,
                    width: ( width - ( portrait.width + portrait.x ) ) / width,
                    height: ( height - ( portrait.height + portrait.y ) ) / height );

                landscapeSafeArea = new Rect(
                    x: landscape.x / height,
                    y: landscape.y / width,
                    width: ( height - ( landscape.height + landscape.x ) ) / height,
                    height: ( width - ( landscape.width + landscape.y ) ) / width );
            }

            public Rect SafeArea
            {
                get
                {
                    Vector2 screenSize = new Vector2( Screen.width, Screen.height );
                    Rect safeArea;
                    if (screenSize.y > screenSize.x)
                        safeArea = portraitSafeArea;
                    else
                        safeArea = landscapeSafeArea;
                    if (safeArea == Rect.zero)
                        return new Rect( Vector2.zero, screenSize );
                    return new Rect( safeArea.position * screenSize, safeArea.size * screenSize );
                }
            }
        }
    }

    [CustomEditor( typeof( SafeAreaContainer ) )]
    public class SafeAreaContainerEditor : SafeAreaSimulationEditor
    {
        public SerializedProperty ignoreX;
        public SerializedProperty ignoreY;
        private new void OnEnable()
        {
            serializedObject.Update();
            ignoreX = serializedObject.FindProperty( "ignoreX" );
            ignoreY = serializedObject.FindProperty( "ignoreY" );
            serializedObject.ApplyModifiedProperties();
            base.OnEnable();
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.PropertyField( ignoreX );
            EditorGUILayout.PropertyField( ignoreY );
            serializedObject.ApplyModifiedProperties();
            base.OnInspectorGUI();
        }
    }

    [CustomEditor( typeof( SafeAreaSimulation ) )]
    public class SafeAreaSimulationEditor : Editor
    {
        private int deviceIndex;
        private KeyCode safeAreaSwitchKey;
        private string[] deviceNames;

        public void OnEnable()
        {
            deviceIndex = SafeAreaSimulation.deviceIndex;
            safeAreaSwitchKey = SafeAreaSimulation.switchKey;
            var devices = SafeAreaSimulation.CreateDeviceList();
            deviceNames = new string[devices.Length];
            for (int i = 0; i < devices.Length; i++)
            {
                deviceNames[i] = devices[i].name;
            }
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.LabelField( "SafeArea Simulation in Editor:", EditorStyles.boldLabel );
            var newDevice = EditorGUILayout.Popup( "Device", deviceIndex, deviceNames );
            if (newDevice != deviceIndex)
            {
                deviceIndex = newDevice;
                SafeAreaSimulation.deviceIndex = newDevice;
            }
            var newKey = ( KeyCode )EditorGUILayout.EnumPopup( "Switch Key", safeAreaSwitchKey );
            if (newKey != safeAreaSwitchKey)
            {
                safeAreaSwitchKey = newKey;
                SafeAreaSimulation.switchKey = newKey;
            }
        }
    }
}
#endif