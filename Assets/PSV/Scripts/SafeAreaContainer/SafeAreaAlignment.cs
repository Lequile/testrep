using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace PSV.Utility
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Layout/SafeArea Alignment Position", 5001)]
    public class SafeAreaAlignment : MonoBehaviour
    {
        public enum vAlignment
        {
            none,
            top,
            center,
            bottom,
        }
        public enum hAlignment
        {
            none,
            left,
            center,
            right,
        }

        public bool awakeApply = true;
        public bool relativeSafeArea = true;

        public vAlignment vAlign = vAlignment.none;
        public hAlignment hAlign = hAlignment.none;

        public float xOffset = 0f;
        public float yOffset = 0f;

        protected void Awake()
        {
            if (awakeApply)
                SetAlignment();
        }

        protected void Start()
        {
            if (!awakeApply)
                SetAlignment();
        }

        public void SetAlignment()
        {
            hAlignment targetH = hAlign;
            vAlignment targetV = vAlign;

            transform.position = CalculatePosition(transform.position, GetScreen.FindCamera(), targetH, targetV, xOffset, yOffset, relativeSafeArea);
        }

        public static Vector2 CalculateOffsetForPosition(Vector3 position, Camera mainCamera, hAlignment horizontal, vAlignment vertical)
        {
            Vector3 centerCamera = mainCamera.transform.position;
            Vector3 minWorl = mainCamera.MinWorld();
            Vector3 maxWorl = mainCamera.MaxWorld();
            Vector2 result = Vector2.zero;

            switch (horizontal)
            {
                case hAlignment.left:
                    result.x = position.x - minWorl.x;
                    break;
                case hAlignment.right:
                    result.x = position.x - maxWorl.x;
                    break;
                case hAlignment.center:
                    result.x = position.x - centerCamera.x;
                    break;
            }

            switch (vertical)
            {
                case vAlignment.bottom:
                    result.y = position.y - minWorl.y;
                    break;
                case vAlignment.top:
                    result.y = position.y - maxWorl.y;
                    break;
                case vAlignment.center:
                    result.y = position.y - centerCamera.y;
                    break;
            }
            return result;
        }


        public static Vector3 CalculatePosition(Vector3 current, Camera mainCamera, hAlignment horizontal, vAlignment vertical, float xOffset, float yOffset, bool relativeSafeArea)
        {
            Vector3 centerWorld;
            Vector3 minWorld;
            Vector3 maxWorld;

            if (relativeSafeArea)
            {
                Rect safeArea = SafeAreaContainer.SafeArea;
               
                centerWorld = mainCamera.ScreenToWorldPoint(safeArea.center);
                minWorld = mainCamera.ScreenToWorldPoint(safeArea.position);
                maxWorld = mainCamera.ScreenToWorldPoint(safeArea.position + safeArea.size);
            }
            else
            {
                centerWorld = mainCamera.transform.position;
                minWorld = mainCamera.MinWorld();
                maxWorld = mainCamera.MaxWorld();
            }

            switch (horizontal)
            {
                case hAlignment.left:
                    current.x = minWorld.x + xOffset;
                    break;
                case hAlignment.right:
                    current.x = maxWorld.x + xOffset;
                    break;
                case hAlignment.center:
                    current.x = centerWorld.x + xOffset;
                    break;
            }

            switch (vertical)
            {
                case vAlignment.bottom:
                    current.y = minWorld.y + yOffset;
                    break;
                case vAlignment.top:
                    current.y = maxWorld.y + yOffset;
                    break;
                case vAlignment.center:
                    current.y = centerWorld.y + yOffset;
                    break;
            }
            return current;
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(SafeAreaAlignment), true)]
    [CanEditMultipleObjects]
    public class SafeAreaAlignmentInspector : Editor
    {
        private Camera mainCamera;
        private Transform transform;
        private SerializedProperty relativeSafeAreaProp;
        private SerializedProperty awakeApplyProp;
        private SerializedProperty vAlignTypeProp;
        private SerializedProperty hAlignTypeProp;
        private SerializedProperty xOffsetProp;
        private SerializedProperty yOffsetProp;
        private GUIContent tempContent;

        private void OnEnable()
        {
            tempContent = new GUIContent();
            mainCamera = GetScreen.FindCamera();
            transform = ((SafeAreaAlignment)target).transform;
            var serialized = serializedObject;
            relativeSafeAreaProp = serialized.FindProperty("relativeSafeArea");
            awakeApplyProp = serialized.FindProperty("awakeApply");
            vAlignTypeProp = serialized.FindProperty("vAlign");
            hAlignTypeProp = serialized.FindProperty("hAlign");
            xOffsetProp = serialized.FindProperty("xOffset");
            yOffsetProp = serialized.FindProperty("yOffset");
        }

        public override void OnInspectorGUI()
        {
            var serialized = serializedObject;
            serialized.Update();
            EditorGUILayout.PropertyField(relativeSafeAreaProp);

            EditorGUILayout.PropertyField(awakeApplyProp);
            if (!((MonoBehaviour)target).enabled && awakeApplyProp.boolValue)
            {
                EditorGUILayout.HelpBox("A change of position will occur even if the component is not activated, " +
                    "since the 'Awake Apply' is activated.", MessageType.Info);
            }

            DrawAxisSettings("Horizontally", hAlignTypeProp, xOffsetProp);
            DrawAxisSettings("Vertically", vAlignTypeProp, yOffsetProp);

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Set position by offset", EditorStyles.miniButton))
                RefreshPosition();
            if (GUILayout.Button("Save offset by position", EditorStyles.miniButton))
                SetOffsetForPosition();
            EditorGUILayout.EndHorizontal();
            serialized.ApplyModifiedProperties();
        }

        private void DrawAxisSettings(string label, SerializedProperty typeProp, SerializedProperty offsetProp)
        {
            tempContent.text = label;
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(typeProp, tempContent);
            if (EditorGUI.EndChangeCheck())
            {
                SetOffsetForPosition();
            }
            EditorGUI.indentLevel++;
            if (typeProp.enumValueIndex == 0)
            {
                EditorGUILayout.HelpBox("The object will not be moved horizontally." + label, MessageType.None);
            }
            else
            {
                EditorGUI.BeginChangeCheck();
                tempContent.text = "Offset";
                EditorGUILayout.PropertyField(offsetProp, tempContent);
                if (EditorGUI.EndChangeCheck())
                {
                    RefreshPosition();
                }
            }
            EditorGUI.indentLevel--;
        }

        private void RefreshPosition()
        {
            Undo.RecordObject(target, "Set position by offset");
            transform.position = SafeAreaAlignment.CalculatePosition(
                        transform.position, mainCamera,
                        (SafeAreaAlignment.hAlignment)hAlignTypeProp.enumValueIndex,
                        (SafeAreaAlignment.vAlignment)vAlignTypeProp.enumValueIndex,
                        xOffsetProp.floatValue, yOffsetProp.floatValue, false);
        }

        private void SetOffsetForPosition()
        {
            Undo.RecordObject(target, "Set Offset by position");
            var offset = SafeAreaAlignment.CalculateOffsetForPosition(
                   transform.position, mainCamera,
                   (SafeAreaAlignment.hAlignment)hAlignTypeProp.enumValueIndex,
                   (SafeAreaAlignment.vAlignment)vAlignTypeProp.enumValueIndex);
            xOffsetProp.floatValue = offset.x;
            yOffsetProp.floatValue = offset.y;
        }
    }
#endif
}
