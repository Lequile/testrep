﻿using UnityEngine;

namespace PSV
{
    [DisallowMultipleComponent]
    [AddComponentMenu( "UI/PSV/Pause Toggle Button" )]
    public class PauseToggleButton : ButtonClickHandler
    {
        protected override void OnButtonClick()
        {
            PauseManager.TogglePause();
        }
    }
}
