﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace PSV
{
    [DisallowMultipleComponent]
    [AddComponentMenu( "UI/PSV/Open Privacy URL Button" )]
    public class OpenPolicyURL : PointerClickBase
    {
        public bool openTermsOfUse = false;

        protected void Start()
        {
            if (string.IsNullOrEmpty( GetURL() ))
                gameObject.SetActive( false );
        }

        public override void Click( PointerEventData eventData = null )
        {
            Application.OpenURL( GetURL() );
        }

        public string GetURL()
        {
            if (Application.platform == RuntimePlatform.IPhonePlayer)
                return openTermsOfUse ? PSVSettings.settings.ios_terms_url : PSVSettings.settings.ios_policy_url;
            return openTermsOfUse ? PSVSettings.settings.android_terms_url : PSVSettings.settings.android_policy_url;
        }
    }

#if UNITY_EDITOR
    [UnityEditor.CustomEditor( typeof( OpenPolicyURL ) )]
    public class OpenPolicyURLEditor : UnityEditor.Editor
    {
        private UnityEditor.SerializedProperty toggle;
        private void OnEnable()
        {
            toggle = serializedObject.FindProperty( "openTermsOfUse" );
        }

        public override void OnInspectorGUI()
        {
            var obj = serializedObject;
            obj.Update();

            UnityEditor.EditorGUILayout.BeginHorizontal();
            var toggleVal = toggle.boolValue;
            if (!toggleVal != GUILayout.Toggle( !toggleVal, "Privacy Policy", "Button" ))
            {
                toggleVal = !toggleVal;
                toggle.boolValue = toggleVal;
            }
            if (toggleVal != GUILayout.Toggle( toggleVal, "Terms Of Use", "Button" ))
            {
                toggleVal = !toggleVal;
                toggle.boolValue = toggleVal;
            }
            UnityEditor.EditorGUILayout.EndHorizontal();

            var androidLink = toggleVal ? PSVSettings.settings.android_terms_url  : PSVSettings.settings.android_policy_url;
            var iosLink = toggleVal ? PSVSettings.settings.ios_terms_url : PSVSettings.settings.ios_policy_url;

            GUILayout.Label( "Android: " + androidLink, UnityEditor.EditorStyles.wordWrappedMiniLabel );
            GUILayout.Label( "iOS: " + iosLink, UnityEditor.EditorStyles.wordWrappedMiniLabel );
            GUILayout.Label( "Change links in PSV Settings window", UnityEditor.EditorStyles.centeredGreyMiniLabel );

            obj.ApplyModifiedProperties();
        }
    }

#endif
}