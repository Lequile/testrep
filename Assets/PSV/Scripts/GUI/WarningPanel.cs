﻿
namespace PSV.Linker
{
    [UnityEngine.AddComponentMenu( "UI/PSV/Warning Panel" )]
    public class WarningPanel : PSV.WarningPanel
    {
        // Can not be removed because PSV.Core.dll doesn't know about AgetnObject in PSV.Basement
    }
}