﻿
namespace PSV.Linker
{
    [UnityEngine.AddComponentMenu( "UI/PSV/Service Message" )]
    public class ServiceMessage : PSV.ServiceMessage
    {
        // Can not be removed because PSV.Core.dll doesn't know about AgetnObject in PSV.Basement
    }
}