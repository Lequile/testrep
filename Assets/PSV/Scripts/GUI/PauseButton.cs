﻿using UnityEngine;

namespace PSV
{
    [DisallowMultipleComponent]
    [AddComponentMenu( "UI/PSV/Pause Button" )]
    public class PauseButton : ButtonClickHandler
    {
        public bool pause = false;

        protected override void OnButtonClick()
        {
            PauseManager.SetPause( pause );
        }
    }
}
