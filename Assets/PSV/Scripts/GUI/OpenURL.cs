﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace PSV
{
    [DisallowMultipleComponent]
    [AddComponentMenu( "UI/PSV/Open URL Button" )]
    public class OpenURL : PointerClickBase
    {
        public string URL = "http://www.psvgamestudio.com/";
        public string overrideForIOS = "";

        public override void Click( PointerEventData eventData = null )
        {
            if (overrideForIOS.Length > 0 && Application.platform == RuntimePlatform.IPhonePlayer)
                Application.OpenURL( overrideForIOS );
            else
                Application.OpenURL( URL );
        }
    }
}