﻿//using UnityEngine.Scripting;

//[assembly: AlwaysLinkAssembly]

namespace PSV.Localization
{
    public static class InitLocalization
    {
        [UnityEngine.RuntimeInitializeOnLoadMethod(
#if UNITY_EDITOR
            UnityEngine.RuntimeInitializeLoadType.BeforeSceneLoad
#else
            UnityEngine.RuntimeInitializeLoadType.AfterSceneLoad
#endif
            )]
        public static void Init()
        {
            AwakeStatic.Done( typeof( InitLocalization ) );
            LocalizationStorage handler;
#if ASSET_BUNDLES_STORAGE
            if (PSVSettings.settings.asset_bundle_storage_enabled)
            {
#if UNITY_EDITOR
                if (!EditorPSVSettings.settings.allow_asset_bundles_in_editor)
                    handler = new EditorLocalizationAssets( true );
                else
#endif
                    handler = new AssetBundles.LocalizationBundles();
            }
            else
#endif
            {
                handler = new LocalizationResources();
            }
            LocalizationManager.Initialize( handler );
        }
    }
}
