﻿using PSV.Audio;
using UnityEngine;

namespace PSV
{
    public partial class AudioController : AudioControllerBase
    {
        #region Stream Groups
        /// <summary>
        /// Returns an AudioStream pool, assigned to the group.
        /// </summary>
        /// <param name="group">StreamGroup to select</param>
        public static AudioStreamContainer GetGroup( StreamGroup group )
        {
            return groups[( int )group];
        }

        /// <summary>
        /// Will change volume override for streams of the group. It will immediately apply to all streams from that group.
        /// </summary>
        /// <param name="group">A group of streams to change volume</param>
        /// <param name="volume"></param>
        public static void SetStreamsVolume( StreamGroup group, float volume )
        {
            GetGroup( group ).Volume = volume;
        }

        /// <summary>
        /// Will mute/unmute streams in the group.
        /// Muting background sounds (music or ambient) without muting other sounds is not allowed.
        /// </summary>
        /// <param name="group">a group of streams to mute</param>
        /// <param name="param">true - mute, false - unmute</param>
        /// <param name="save_settings">Is Save value to PlayerPrefs</param>
        public static void MuteStreams( StreamGroup group, bool param, bool save_settings = false )
        {
            GetGroup( group ).Mute( param, save_settings );
        }

        /// <summary>
        /// Stops streams in all available groups that are playing the sound with name snd_name.  Returns count of streams that were playing that sound
        /// </summary>
        /// <param name="snd_name">AudioCLip name</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <returns></returns>
        public static int StopSound( string snd_name, StreamGroup group )
        {
            return GetGroup( group ).StopSound( snd_name );
        }

        /// <summary>
        /// Stops all streams for selected streams group.
        /// </summary>
        /// <param name="group">Released streams group</param>
        public static void Release( StreamGroup group )
        {
            GetGroup( group ).StopSounds();
        }

        /// <summary>
        /// Returns volume for the group multiplied by volume_override.
        /// </summary>
        /// <param name="group">StreamGroup to select</param>
        /// <returns>Volume level</returns>
        public static float GetGroupVolume( StreamGroup group )
        {
            return GetGroup( group ).Volume;
        }

        /// <summary>
        /// Will tell true if at least one stream plays the sound
        /// </summary>
        /// <param name="snd_name">AudioClip name</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <returns>Whether at least one stream in group plays the sound</returns>
        public static bool IsSoundPlaying( string snd_name, StreamGroup group )
        {
            return GetGroup( group ).IsSoundPlaying( snd_name );
        }
        #endregion

        #region PlaySound methods
        
        /// <summary>
        /// Plays clip using own stream from the group.
        /// </summary>
        /// <param name="clip">AudioClip</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sound</param>
        /// <returns>Length of sound</returns>
        public static PlayingSound PlaySound( AudioClip clip, StreamGroup group = StreamGroup.FX, float volume_override = 1, bool loop = false )
        {
            return Play( clip )
                    .Group( group )
                    .Volume( volume_override )
                    .Loop( loop )
                    .Start();
        }

        /// <summary>
        /// Plays sound by AudioClip's name and returns it's length
        /// </summary>
        /// <param name="snd_name">AudioClip name</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sound</param>
        /// <returns>Length of sound</returns>
        public static PlayingSound PlaySound( string snd_name, StreamGroup group = StreamGroup.FX, float volume_override = 1, bool loop = false )
        {
            return Play( snd_name )
                    .Group( group )
                    .Volume( volume_override )
                    .Loop( loop )
                    .Start();
        }

        /// <summary>
        /// Plays a set of sounds placed in a queue. Used for dialogs containing separate phrases.
        /// </summary>
        /// <param name="clips">sounds to play in a row</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="intervals">Pause before next AudioClip will be played, should be of the same length as clips or null to make no pause</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sequence</param>
        /// <returns>Stack length</returns>
        public static PlayingSound PlaySound( AudioClip[] clips, StreamGroup group = StreamGroup.FX, float[] intervals = null, float volume_override = 1, bool loop = false )
        {
            return Play( clips )
                    .Group( group )
                    .Volume( volume_override )
                    .Loop( loop )
                    .Intervals( intervals )
                    .Start();
        }

        /// <summary>
        /// Plays a set of sounds placed in a queue. Used for dialogs containing separate phrases.
        /// </summary>
        /// <param name="snd_name">AudioClip names, value "||2.2" gives extra pause between phrases</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="interval">Pause before next AudioClip will be played</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sequence</param>
        /// <returns>Stack length</returns>
        public static PlayingSound PlaySound( string[] snd_name, StreamGroup group = StreamGroup.FX, float interval = 0, float volume_override = 1, bool loop = false )
        {
            var handler = Play( snd_name )
                            .Group( group )
                            .Volume( volume_override )
                            .Loop( loop );
            if (interval > 0)
            {
                float[] intervals = new float[snd_name.Length];
                for (int i = 0; i < intervals.Length; i++)
                    intervals[i] = interval;

                handler.AddIntervals( intervals );
            }
            return handler.Start();
        }

        
        #endregion
    }
}