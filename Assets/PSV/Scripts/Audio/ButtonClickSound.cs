﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace PSV
{
    [DisallowMultipleComponent]
    [AddComponentMenu( "Audio/Click Sound", 100 )]
    public class ButtonClickSound : MonoBehaviour, IPointerClickHandler
    {
        public static void Play()
        {
            AudioController.Play( PSVSettings.settings.click_btn_sound ).IgnorePause( true ).Start();
            Vibration.Vibrate( 20 );
        }

        void IPointerClickHandler.OnPointerClick( PointerEventData eventData )
        {
            Play();
        }
    }
}
