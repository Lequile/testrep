﻿using UnityEngine;

namespace PSV.Audio
{
    [AddComponentMenu( "Audio/Audio Toggle", 100 )]
    public class AudioToggle : ToggleButton
    {
        public enum Toggles
        {
            MusicDeprecated,
            Sounds,
            Vibro,
            FX,
            Music,
            Voice,
        }

        public Toggles ToggleType;

        private bool active = false;

        protected override void Awake()
        {
            if (ToggleType == Toggles.MusicDeprecated
                || ( ToggleType == Toggles.Vibro && !Vibration.IsAllowPlatform( Application.platform ) ))
                gameObject.SetActive( false );
            else
                base.Awake();
        }

        private void OnEnable()
        {
            SetValues();
        }

        private void SetValues()
        {
            active = false;
            bool param = false;
            switch (ToggleType)
            {
                case Toggles.Music:
                    param = StreamGroup.MUSIC.Container().IsMuted();
                    break;
                case Toggles.Sounds:
                    param = AudioControllerBase.IsMuted();
                    break;
                case Toggles.Vibro:
                    param = !Vibration.IsEnabled();
                    break;
                case Toggles.FX:
                    param = StreamGroup.FX.Container().IsMuted();
                    break;
                case Toggles.Voice:
                    param = StreamGroup.VOICE.Container().IsMuted();
                    break;
            }
            //this stuff is done to avoid listeners from catching init changes events
            SetToggle( param );
            active = true;
        }

        protected override void OnValueChanged( bool param )
        {
            if (!active)
                return;
            switch (ToggleType)
            {
                case Toggles.Music:
                    StreamGroup.MUSIC.Container().Mute( param );
                    break;
                case Toggles.Sounds:
                    AudioControllerBase.Mute( param, true );
                    break;
                case Toggles.Vibro:
                    Vibration.SetEnabled( !param, true );
                    break;
                case Toggles.FX:
                    StreamGroup.FX.Container().Mute( param );
                    break;
                case Toggles.Voice:
                    StreamGroup.VOICE.Container().Mute( param );
                    break;
            }
        }

    }
}