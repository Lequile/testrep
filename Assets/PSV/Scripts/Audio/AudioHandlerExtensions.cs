﻿using UnityEngine.Audio;
using PSV.Audio;

namespace PSV
{
    public static class AudioHandlerExtensions
    {
        /// <summary>
        /// Get <see cref="AudioStreamContainer"/> for this type.
        /// </summary>
        public static AudioStreamContainer Container( this StreamGroup type )
        {
            return AudioControllerBase.GetGroup( (int)type );
        }

        /// <summary>
        /// Set StreamGroup the clip is related to
        /// </summary>
        public static AudioHandler Group( this AudioHandler handler, StreamGroup group = StreamGroup.FX )
        {
            return handler.Group( ( int )group );
        }

        #region Not recommended
        /// <summary>
        /// Set StreamGroup the clip is related to
        /// </summary>
        public static AudioHandler SetGroup( this AudioHandler handler, StreamGroup group = StreamGroup.FX )
        {
            return handler.Group( ( int )group );
        }

        /// <summary>
        /// This will override volume from GameSettings for the group
        /// </summary>
        public static AudioHandler SetVolume( this AudioHandler handler, float volume = 1.0f )
        {
            return handler.Volume( volume );
        }

        /// <summary>
        /// This will loop the sound.
        ///  Warning! Loop is not supported for Sequence playing.
        /// </summary>
        public static AudioHandler SetLoop( this AudioHandler handler, bool loop = false )
        {
            return handler.Loop( loop );
        }

        /// <summary>
        /// Pitch value for sound
        /// </summary>
        public static AudioHandler SetPitch( this AudioHandler handler, float pitch = 1.0f )
        {
            return handler.Pitch( pitch );
        }

        /// <summary>
        /// Will ignore pause and play on unscaled time.
        /// </summary>
        public static AudioHandler SetIgnorePause( this AudioHandler handler, bool ignore_pause = false )
        {
            return handler.IgnorePause( ignore_pause );
        }
        
        /// <summary>
        /// Pause before next AudioClip will be played, should be of the same length as clips or null to make no pause
        /// </summary>
        public static AudioHandler SetIntervals( this AudioHandler handler, float[] intervals )
        {
            return handler.Intervals( intervals );
        }

        /// <summary>
        /// Set Unity Audio Mixer Group for <see cref="AudioMixer"/>.
        /// </summary>
        public static AudioHandler SetMixerGroup( this AudioHandler handler, AudioMixerGroup mixer_group )
        {
            return handler.MixerGroup( mixer_group );
        }
        #endregion
    }
}