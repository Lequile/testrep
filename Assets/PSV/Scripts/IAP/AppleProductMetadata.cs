﻿#if !NO_UNITY_IAP && ( UNITY_PURCHASING || UNITY_2019_4_OR_NEWER )
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

namespace PSV.IAP
{
    public class AppleProductMetadata
    {
        public string origin_json { get; internal set; }
        public string subscriptionNumberOfUnits { get; internal set; }
        public string subscriptionPeriodUnit { get; internal set; }
        public decimal localizedPrice { get; internal set; }
        public string isoCurrencyCode { get; internal set; }
        public string localizedPriceString { get; internal set; }
        public string localizedTitle { get; internal set; }
        public string localizedDescription { get; internal set; }
        public decimal introductoryPrice { get; internal set; }
        public string introductoryPriceLocale { get; internal set; }
        public string introductoryPriceNumberOfPeriods { get; internal set; }
        public string numberOfUnits { get; internal set; }
        public string unit { get; internal set; }

        public bool hasOriginJson => string.IsNullOrEmpty(origin_json);

        public AppleProductMetadata(string json)
        {
            if (string.IsNullOrEmpty(json))
            {
                Debug.LogError("Product metadata is empty");
                origin_json = null;
                return;
            }

            try
            {
                origin_json = json;

                Dictionary<string, object> metadata = (Dictionary<string, object>)MiniJson.JsonDecode(origin_json);
                
                subscriptionNumberOfUnits = metadata.TryGetString("subscriptionNumberOfUnits");
                subscriptionPeriodUnit = metadata.TryGetString("subscriptionPeriodUnit");
                localizedPrice = GetPrice(metadata.TryGetString("localizedPrice"));
                isoCurrencyCode = metadata.TryGetString("isoCurrencyCode");
                localizedPriceString = metadata.TryGetString("localizedPriceString");
                localizedTitle = metadata.TryGetString("localizedTitle");
                localizedDescription = metadata.TryGetString("localizedDescription");
                introductoryPrice = GetPrice(metadata.TryGetString("introductoryPrice"));
                introductoryPriceLocale = metadata.TryGetString("introductoryPriceLocale");
                introductoryPriceNumberOfPeriods = metadata.TryGetString("introductoryPriceNumberOfPeriods");
                numberOfUnits = metadata.TryGetString("numberOfUnits");
                unit = metadata.TryGetString("unit");
            }
            catch
            {
                origin_json = null;
            }
        }

        /// <summary>
        /// Get product period in months. Return 0 if perid is less then month. Return -1 if not available.
        /// </summary>
        /// <returns>
        /// Product period in months.
        /// </returns>
        public int GetProudctPeriodInMonth()
        {
            return GetProudctPeriodInMonth(subscriptionPeriodUnit, subscriptionNumberOfUnits);
        }

        /// <summary>
        /// Get introductory period in months. Return 0 if perid is less then month. Return -1 if not available.
        /// </summary>
        /// <returns>
        /// Introductory period in months.
        /// </returns>
        public int GetIntroductoryPeriodInMonth()
        {
            return GetProudctPeriodInMonth(unit, numberOfUnits);
        }

        private int GetProudctPeriodInMonth(string periodUnit, string numberOfUnits)
        {
            if (periodUnit == "4")
                return -1;

            if (periodUnit == "0" || periodUnit == "1")
                return 0;

            int month = 1;

            if (periodUnit == "3")
                month = 12;

            if (int.TryParse(numberOfUnits, out int unit))
                month *= unit;
            else
                month = -1;

            return month;
        }

        private decimal GetPrice(string price)
        {
            decimal num = 0.0m;

            try
            {
                num = System.Convert.ToDecimal(price);
            }
            catch
            {
                num = 0.0m;
            }

            return num;
        }
    }

    internal static class IAPExtensions
    {
        public static string TryGetString(this Dictionary<string, object> dictionary, string key)
        {
            if (dictionary.ContainsKey(key) && dictionary[key] != null)
            {
                return dictionary[key].ToString();
            }
            return null;
        }
    }
}
#endif