﻿#if !NO_UNITY_IAP
#if UNITY_PURCHASING || UNITY_2019_4_OR_NEWER
#define USE_UNITY_IAP
//#define USE_APPLE_VALIDATOR
#endif
#endif

#if USE_UNITY_IAP
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

namespace PSV.IAP
{
    public class UnityIAPProvider : IAPProviderBase, UnityEngine.Purchasing.IStoreListener
    {
        private IStoreController storeController;
        private IExtensionProvider storeExtensions;

        public override byte priority { get { return 10; } }

        [RuntimeInitializeOnLoadMethod( RuntimeInitializeLoadType.AfterSceneLoad )]
        public static void InitProvider()
        {
            AwakeStatic.Done( typeof( UnityIAPProvider ) );
            UnityIAPProvider instance = new UnityIAPProvider();
            if (TryInit( instance ))
            {
                var module = StandardPurchasingModule.Instance();
                if (Application.isEditor)
                    module.useFakeStoreUIMode = FakeStoreUIMode.DeveloperUser;
                var builder = ConfigurationBuilder.Instance( module );

                //builder.Configure<IGooglePlayConfiguration>().SetPublicKey();

                ProductProperties[] properties = PSVSettings.settings.products_SKU;

                for (int i = 0; i < properties.Length; i++)
                {
                    string sku = properties[i].sku;
                    var prodType = ( UnityEngine.Purchasing.ProductType )properties[i].type;
                    DebugLog( "Initialize product " + sku + " as " + prodType );
                    builder.AddProduct( sku, prodType );
                }

                //SubscriptionManager
                // Expect a response either in OnInitialized or OnInitializeFailed.
                UnityPurchasing.Initialize( instance, builder );
            }
        }

#region IStoreListener
        /// <summary>
        /// This will be called when Unity IAP has finished initialising.
        /// </summary>
        public void OnInitialized( IStoreController controller, IExtensionProvider extensions )
        {
            DebugLog( "On Initialize Unity Purchasing successed" );

            // Overall Purchasing system, configured with products for this application.
            storeController = controller;
            // Store specific subsystem, for accessing device-specific store features.
            storeExtensions = extensions;
        }


        public void OnInitializeFailed( InitializationFailureReason error )
        {
            switch (error)
            {
                case InitializationFailureReason.AppNotKnown:
                    DebugLog( "On Initialize Unity Purchasing Failed: Is your App correctly uploaded on the relevant publisher console?", true );
                    break;
                case InitializationFailureReason.PurchasingUnavailable:
                    // Ask the user if billing is disabled .
                    Debug.Log( "" );
                    DebugLog( "On Initialize Unity Purchasing Failed: Billing disabled in device settings", true );

                    break;
                case InitializationFailureReason.NoProductsAvailable:
                    // Developer configuration error; check product metadata.
                    Debug.Log( "No products available for purchase!" );
                    break;
            }
        }

        /// <summary>
        /// This will be called when a purchase completes.
        /// </summary>
        public PurchaseProcessingResult ProcessPurchase( PurchaseEventArgs args )
        {
            PurchaseSucceded( args.purchasedProduct.definition.id );

            // Return a flag indicating whether this product has completely been received, or if the application needs 
            // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
            // saving purchased products to the cloud, and when that save is delayed. 
            return PurchaseProcessingResult.Complete;
        }

        /// <summary>
        /// This will be called if an attempted purchase fails.
        /// </summary>
        public void OnPurchaseFailed( UnityEngine.Purchasing.Product product, PurchaseFailureReason failureReason )
        {
            // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
            // this reason with the user to guide their troubleshooting actions.
            DebugLog( "Purchase the product with Id[" + product.definition.id + "] failed: " + failureReason, true );

            // Detailed debugging information
            if (PSVSettings.settings != null && PSVSettings.settings.billing_manager_debug)
            {
                var history = storeExtensions.GetExtension<ITransactionHistoryExtensions>();
                if (history != null)
                {
                    DebugLog( "Store specific error code: " + history.GetLastStoreSpecificPurchaseErrorCode() );
                    var lastDescription = history.GetLastPurchaseFailureDescription();
                    if (lastDescription != null)
                        DebugLog( "Purchase failure description message: " + lastDescription.message );
                }
            }

            PurchaseFailed( product.definition.id );
        }
#endregion

#region PSV Purchasing

        public override bool IsProductPurchased( ProductProperties prop )
        {
            if (storeController == null)
            {
                DebugLog( "You are trying to get the product with Id[" + prop.sku +
                    "] status, but Unity Purchasing is still not initialized.", true );
                return false;
            }
            var unityProduct = storeController.products.WithID( prop.sku );
            if (unityProduct == null)
            {
                DebugLog( "Product with Id[" + prop.sku + "] not found in Unity Purchasing", true );
                return false;
            }
            if (unityProduct.hasReceipt)
            {
                PurchaseSucceded( prop.sku );
                return true;
            }
            return false;
        }

        public override void OnPreparationProductsRestore()
        {
            if (storeExtensions == null)
            {
                DebugLog( "You are trying to Restore Transactions, but Unity Purchasing is still not initialized.", true );
                ResultProductsRestore( false );
                return;
            }
            if (Application.platform != RuntimePlatform.IPhonePlayer)
            {
                if (!Application.isEditor)

                    ResultProductsRestore( false );
                return;
            }
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    DebugLog( "Restore Transactions started ..." );
                    storeExtensions
                        .GetExtension<IGooglePlayStoreExtensions>()
                        .RestoreTransactions( ResultProductsRestore );
                    break;
                case RuntimePlatform.IPhonePlayer:
                    DebugLog( "Restore Transactions started ..." );
                    storeExtensions
                        .GetExtension<IAppleExtensions>()
                        .RestoreTransactions( ResultProductsRestore );
                    break;
                default:
                    DebugLog( "You are trying to Restore Transactions on not supported platform:"
                        + Application.platform, true );
                    ResultProductsRestore( false );
                    break;
            }

        }

        public override void OnPreparationPurchase( ProductProperties prop )
        {
            if (storeController == null)
            {
                DebugLog( "You are trying to purchase the product with Id[" + prop.sku +
                    "] status, but Unity Purchasing is still not initialized.", true );
                PurchaseFailed( prop.sku );
                return;
            }

            DebugLog( "Trying to purchase " + prop.product );

            // ... look up the Product reference with the general product identifier and the Purchasing 
            // system's products collection.
            var product = storeController.products.WithID( prop.sku );

            if (product != null && product.availableToPurchase)
            {
                DebugLog( "Purchasing product with Id[" + prop.sku + "] started..." );
                // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                // asynchronously.
                storeController.InitiatePurchase( product );
                return;
            }
            // ... report the product look-up failure situation  
            if (!Application.isEditor)
                DebugLog( "Product with Id[" + prop.sku + "] not found in Unity Purchasing", true );
            PurchaseFailed( prop.sku );
        }

        public override object GetProductMetadata( string sku )
        {
            if (storeController == null)
            {
                DebugLog( "You are trying to get metadata of product with Id[" + sku +
                    "] status, but Unity Purchasing is still not initialized.", true );
                return null;
            }
            var product = storeController.products.WithID( sku );
            if (product != null)
                return product.metadata;
            DebugLog( "Product with Id[" + sku + "] not found in Unity Purchasing", true );
            return null;
        }

        public override string GetLocalizedPrice( string sku )
        {
            var metadata = (ProductMetadata)GetProductMetadata( sku );
            if (metadata == null)
                return string.Empty;
            return metadata.localizedPriceString;
        }

        public override string GetLocalizedTitle( string sku )
        {
            var metadata = ( ProductMetadata )GetProductMetadata( sku );
            if (metadata == null)
                return string.Empty;
            return metadata.localizedTitle;
        }

        public override string GetLocalizedDescription( string sku )
        {
            var metadata = ( ProductMetadata )GetProductMetadata( sku );
            if (metadata == null)
                return string.Empty;
            return metadata.localizedDescription;
        }

        public override object GetSubscriptionInfo( string sku )
        {
            if (storeController == null)
            {
                DebugLog( "You are trying to get Subscription data of product with Id[" + sku +
                    "] status, but Unity Purchasing is still not initialized.", true );
                return null;
            }

            var item = storeController.products.WithID( sku );
            if (item == null)
            {
                DebugLog( "Product with Id[" + sku + "] not found in Unity Purchasing", true );
                return null;
            }

            if (item.receipt == null)
            {
                DebugLog( "Product with Id[" + sku + "] have a no valid receipt" );
                return null;
            }

            if (item.definition.type != UnityEngine.Purchasing.ProductType.Subscription)
            {
                DebugLog( "Product with Id[" + sku + "] is not a subscription product" );
                return null;
            }

            var introductory_info_dict = storeExtensions.GetExtension<IAppleExtensions>().GetIntroductoryPriceDictionary();

            string intro_json = null;
            if (introductory_info_dict != null)
                introductory_info_dict.TryGetValue( item.definition.storeSpecificId, out intro_json );

            SubscriptionManager p = new SubscriptionManager( item, intro_json );
            return p.getSubscriptionInfo();
        }

        public object GetAppleProductInfo(string sku)
        {
            if (storeExtensions == null)
            {
                DebugLog("You are trying to get data of product with Id[" + sku +
                    "] status, but Unity Purchasing is still not initialized.", true);
                return null;
            }

            var product_info_dict = storeExtensions.GetExtension<IAppleExtensions>().GetProductDetails();

            string product_json = null;
            if (product_info_dict != null)
                product_info_dict.TryGetValue(sku, out product_json);

            if (string.IsNullOrEmpty(product_json))
                return null;

            AppleProductMetadata appleProductMetadata = new AppleProductMetadata(product_json);

            return appleProductMetadata;
        }

#if USE_APPLE_VALIDATOR
        public static AppleReceipt GetAppleReceipts()
        {
            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
            var appleConfig = builder.Configure<IAppleConfiguration>();

            if (string.IsNullOrEmpty(appleConfig.appReceipt))
            {
                DebugLog("Receipt is null or empty");
                return null;
            }

            try
            {
                var receiptData = System.Convert.FromBase64String(appleConfig.appReceipt);
                var appleReceipts = new AppleValidator(AppleTangle.Data()).Validate(receiptData);
                return appleReceipts;
            }
            catch
            {
                DebugLog("Wrong receipt data");
                return null;
            }
        }
#endif

#endregion
    }
}
#endif