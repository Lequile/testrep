﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace PSV.IAP
{
    [DisallowMultipleComponent]
    [AddComponentMenu("UI/PSV/Restore iOS Purchase Button")]
    public class RestorePurchaseButton : PointerClickBase
    {
        public Text label;

        protected new void Awake()
        {
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                base.Awake();
                Languages.OnLangChange += OnLangChange;
            }
            else
            {
                gameObject.SetActive(false);
            }
        }

        protected void OnEnable()
        {
            OnLangChange(Languages.GetLanguage());
        }

        public override void Click(PointerEventData eventData = null)
        {
            BillingManager.RestoreProducts();
        }

        private void OnLangChange(Languages.Language lang)
        {
            if (label)
                label.text = GetLocalizedLabel(lang);
        }


        public static string GetLocalizedLabel(Languages.Language lang)
        {
            switch (lang)
            {
                case Languages.Language.Chinese:
                    return "恢复购买";
                case Languages.Language.Dutch:
                    return "Genoprette indkøb";
                case Languages.Language.French:
                    return "Restaurer les achats";
                case Languages.Language.German:
                    return "Einkäufe wiederherstellen";
                case Languages.Language.Italian:
                    return "Ripristinare gli acquisti";
                case Languages.Language.Japanese:
                    return "購入を復元";
                case Languages.Language.Norwegian:
                    return "Gjenopprette kjøp";
                case Languages.Language.Polish:
                    return "Przywróć zakupy";
                case Languages.Language.Portuguese:
                    return "Restaurar compras";
                case Languages.Language.Romanian:
                    return "Restaurați achizițiile";
                case Languages.Language.Russian:
                    return "Восстановить покупки";
                case Languages.Language.Spanish:
                    return "Restaurar las compras";
                case Languages.Language.Swedish:
                    return "Återställa köp";
                case Languages.Language.Turkish:
                    return "Satın alma işlemlerini geri yükle";
                case Languages.Language.Indonesian:
                    return "Mengembalikan pembelian";
                case Languages.Language.Korean:
                    return "구매를 복원";
                case Languages.Language.Ukrainian:
                    return "Відновити покупки";
                case Languages.Language.Catalan:
                    return "Restaura les compres";
                case Languages.Language.Belarusian:
                    return "Аднавіць пакупкі";
                case Languages.Language.Vietnamese:
                    return "Khôi phục mua hàng";
                default:
                    return "Restore purchases";
            }
        }
    }
}