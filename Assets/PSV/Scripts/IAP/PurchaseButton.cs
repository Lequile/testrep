﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace PSV.IAP
{
    [DisallowMultipleComponent]
    [AddComponentMenu( "" )]
    public class PurchaseButton : InAppPurchaseButton
    {
        [UnityEngine.Serialization.FormerlySerializedAs( "product" )]
        [Header("Deprecated. Use Name Product instead.")]
        [SerializeField]
        private Products productName = Products.SKU_ADMOB;

        public Linker.LinkerWarning lw;
        
        public Products product
        {
            get
            {
                return productName;
            }
            set
            {
                base.nameProduct = value.ToString();
                productName = value;
            }
        }

        private new void Awake()
        {
            base.Awake();
            base.nameProduct = productName.ToString();
        }

    }
}