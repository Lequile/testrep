﻿using PSV.ADS;
using PSV.ADS.Core;
using UnityEngine;
using UnityEngine.EventSystems;

namespace PSV.IAP
{
    [DisallowMultipleComponent]
    [AddComponentMenu( "UI/PSV/Purchase Ads Button" )]
    public class PurchaseAdsButton : PointerClickBase
    {
        private void Start()
        {
            AdsInterop.OnEnabledAdsChanged += OnEnabledAdsChanged;
            EnableButton();
        }

        private void OnDestroy()
        {
            AdsInterop.OnEnabledAdsChanged -= OnEnabledAdsChanged;
        }

        private void OnEnabledAdsChanged( AdTypeFlags ignored)
        {
            EnableButton();
        }

        private void EnableButton()
        {
#if !NO_PSV_ADS
            gameObject.SetActive( ( Application.isEditor || Application.isMobilePlatform ) && AdsManager.IsAdsEnabled() );
#else
            gameObject.SetActive( false );
#endif
        }

        public override void Click( PointerEventData eventData = null )
        {
            if (Application.isEditor || Application.isMobilePlatform)
                BillingManagerBase.Purchase( ProductProperties.admob_product_name );
        }
    }
}