﻿#if UNITY_EDITOR || UNITY_STANDALONE || DEBUG

#if !NO_UNITY_IAP
#if UNITY_PURCHASING || UNITY_2019_4_OR_NEWER
#define USE_UNITY_IAP
#endif
#endif

#if USE_UNITY_IAP
using UnityEngine.Purchasing;
#endif

namespace PSV.IAP
{
    internal class DummyIAPProvider : IAPProviderBase
    {
        private const float delayOperations = 1.0f;
        public override byte priority { get { return 250; } }

        [UnityEngine.RuntimeInitializeOnLoadMethod( UnityEngine.RuntimeInitializeLoadType.AfterSceneLoad )]
        public static void InitProvider()
        {
            AwakeStatic.Done( typeof( DummyIAPProvider ) );
            TryInit( new DummyIAPProvider() );
        }

        public override bool IsProductPurchased( ProductProperties prop )
        {
            return false;
        }

        public override void OnPreparationProductsRestore()
        {
#if USE_UNITY_IAP
            ResultProductsRestore( true );
#else
            // Call on next frame for init Purchase progress indicator first.
            NextFrameCall.Add( NextFramePanelRestore );
#endif
        }

        public override void OnPreparationPurchase( ProductProperties prop )
        {
#if USE_UNITY_IAP
            PurchaseSucceded( prop.sku );
#else
            // Call on next frame for init Purchase progress indicator first.
            NextFrameCall.Add( () => NextFramePanelPurchase( prop ) );
#endif
        }

        private void NextFramePanelRestore()
        {
            WarningPanel.Show( "Restore products?", true )
                        .SetTitle( "For Develop only" )
                        .OnAccept( () => DelayRestoreResult( true ) )
                        .OnCancel( () => DelayRestoreResult( false ) );
        }

        private void NextFramePanelPurchase( ProductProperties prop )
        {
            WarningPanel.Show( "Purchase: " + prop.ToString(), true )
                        .SetTitle( "For Develop only" )
                        .OnAccept( () => this.Invoke( () => PurchaseSucceded( prop.sku ), delayOperations, true, true ) )
                        .OnCancel( () => this.Invoke( () => PurchaseFailed( prop.sku ), delayOperations, true, true ) );
        }

        private void DelayRestoreResult( bool succses )
        {
            this.Invoke( () => ResultProductsRestore( succses ), delayOperations, true, true );
        }

        public override string GetLocalizedPrice( string sku )
        {
#if USE_UNITY_IAP
            return (( ProductMetadata )GetProductMetadata(sku)).localizedPriceString;
#else
            return "99.99 грн.";
#endif
        }

        public override string GetLocalizedTitle( string sku )
        {
#if USE_UNITY_IAP
            return ( ( ProductMetadata )GetProductMetadata( sku ) ).localizedTitle;
#else
            return sku + " Title";
#endif
        }

        public override string GetLocalizedDescription( string sku )
        {
#if USE_UNITY_IAP
            return ( ( ProductMetadata )GetProductMetadata( sku ) ).localizedDescription;
#else
            return sku + " Description";
#endif
        }

        public override object GetSubscriptionInfo( string sku )
        {
            DebugLog( "Get Subscription Info not supported for dummy IAP provider" );
            return null;
        }

        public override object GetProductMetadata( string sku )
        {
#if USE_UNITY_IAP
            return new ProductMetadata( "99.99 грн.", sku + " Title", sku + " Description", "UAH", 99.99M );
#else
            return null;
#endif
        }
    }
}
#endif