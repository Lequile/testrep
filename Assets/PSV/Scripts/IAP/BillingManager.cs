﻿#if !NO_UNITY_IAP
#if UNITY_PURCHASING || UNITY_2019_4_OR_NEWER
#define USE_UNITY_IAP
//#define USE_APPLE_VALIDATOR
#endif
#endif

using System;
#if USE_UNITY_IAP
using UnityEngine.Purchasing;
#endif

namespace PSV.IAP
{
    public partial class BillingManager : BillingManagerBase
    {
        public static ProductProperties GetProperty( Products product )
        {
            return GetPropertyByName( product.ToString() );
        }

        public static void Purchase( Products product )
        {
            Purchase( product.ToString() );
        }

        public static bool IsProductPurchased( Products prod )
        {
            return IsProductPurchased( prod.ToString() );
        }

#if USE_UNITY_IAP
        public static ProductMetadata GetProductMetadata( Products prod )
        {
            if (provider == null)
            {
                DebugLog( "You are trying to get metadata of product[" + prod +
                       "], but BillingManager is still not initialized.", true );
                return null;
            }
            var sku = GetPropertyByName( prod.ToString() ).sku;
            if (string.IsNullOrEmpty( sku ))
                return null;
            return provider.GetProductMetadata( sku ) as ProductMetadata;
        }


        public static GoogleProductMetadata GetGoogleProductMetadata( Products prod )
        {
            try
            {
                return GetProductMetadata( prod ) as GoogleProductMetadata;
            }
            catch
            {
                return null;
            }
        }

        public static AppleProductMetadata GetAppleProductInfo(Products prod)
        {
            if (provider == null)
            {
                DebugLog("You are trying to get metadata of product[" + prod +
                       "], but BillingManager is still not initialized.", true);
                return null;
            }

            var sku = GetPropertyByName(prod.ToString()).sku;

            if (string.IsNullOrEmpty(sku))
                return null;

            return ((UnityIAPProvider)provider).GetAppleProductInfo(sku) as AppleProductMetadata;
        }

        public static SubscriptionInfo GetSubscriptionInfo( Products prod )
        {
            if (provider == null)
            {
                DebugLog( "You are trying to get Subscription Info of product[" + prod +
                       "], but BillingManager is still not initialized.", true );
                return null;
            }
            var prop = GetPropertyByName( prod.ToString() );
            if (string.IsNullOrEmpty( prop.sku ))
            {
                DebugLog( "You are trying to get Subscription Info of product[" + prod +
                       "] without SKU." );
                return null;
            }
            if(prop.type != ProductType.Subscription)
            {
                DebugLog( "You are trying to get Subscription Info of product[" + prod +
                       "], but product is not subscription." );
                return null;
            }
            try
            {
                return provider.GetSubscriptionInfo( prop.sku ) as SubscriptionInfo;
            }
            catch
            {
                return null;
            }
        }
#endif

        #region Deprecated
        [Obsolete( "Deprecated. Use GetProperty( product ).sku instead!" )]
        public static ProductType GetProductType( Products prod )
        {
            return GetProperty( prod ).type;
        }

        [Obsolete( "Deprecated. Use GetProperty( product ).sku instead!" )]
        public static string GetProductSku( Products prod )
        {
            return GetProperty( prod ).sku;
        }

        [Obsolete( "Deprecated. Use GetPropertyBySku( sku_alias ).producs instead!", true )]
        public static Products GetProduct( string sku_alias )
        {
            return Products.Empty;
        }
#endregion
    }
}
