﻿#if !UNITY_STANDALONE
using UnityEngine;

namespace PSV.IAP
{
    public static class PurchaseProgressIndicator
    {
        private static WarningPanel panel = null;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        public static void StaticAwake()
        {
            AwakeStatic.Done(typeof(PurchaseProgressIndicator));
            if (Utils.isEditor || Application.isMobilePlatform)
            {
                BillingManagerBase.OnPreparationProductsRestore += Show;
                BillingManagerBase.OnPreparationPurchase += OnPreparationPurchase;
                BillingManagerBase.OnPurchaseFailed += OnPurchaseSucceded;
                BillingManagerBase.OnPurchaseSucceded += OnPurchaseSucceded;
                BillingManagerBase.OnResultProductsRestore += OnResultProductsRestore;
            }
        }

        public static void Show()
        {
            if (panel == null)
            {
                panel = WarningPanel.ShowPleaseWait()
                                    .HideMessage(20.0f);
            }
        }

        public static void Hide()
        {
            if (panel != null)
            {
                panel.HideMessage();
                panel = null;
            }
        }

        private static void OnResultProductsRestore(bool succses)
        {
            Hide();
        }

        private static void OnPurchaseSucceded(ProductProperties prop)
        {
            Hide();
        }

        private static void OnPreparationPurchase(ProductProperties prop)
        {
            Show();
        }

        private static void OnPurchaseFailed(ProductProperties prop)
        {
            Hide();
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                panel = WarningPanel.Show(WaitAllowInternet.LangTextWaitNetworcConnection(), true)
                                    .SetEndlessProgress()
                                    .OnAccept(Hide, false)
                                    .OnFadeClick(Hide, false);
            }
        }
    }
}
#endif