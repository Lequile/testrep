﻿#if UNITY_STANDALONE
using System;
using UnityEngine;

namespace PSV
{
    [AddComponentMenu( "" )]
    public class ResolutionCheckerStandalone : AgentObject<ResolutionCheckerStandalone>
    {
        private const float targetAspect = 9.0f / 16.0f;

        public static bool
            change_size = true;

        private int
            saved_height = 0,
            saved_width = 0;

        public static void OnScreenShutter( bool closed )
        {
            change_size = !closed;
        }

        private void Start()
        {
            if (PSVSettings.settings.portrait_orientation_only)
                InvokeRepeating( "CheckResolution", 0.0f, 1.0f );
            else
                Destroy( this );
        }

        public static void Create()
        {
            CheckAgent();
        }

        /// <summary>
        /// Invoke Repeating method : <see cref="Start()"/>
        /// </summary>
        private void CheckResolution()
        {
            if (change_size && ( Screen.height != saved_height || Screen.width != saved_width ))
            {
                saved_height = Screen.height;
                saved_width = ( int )( saved_height * targetAspect );
                Debug.Log( "CheckResolution " + saved_width + "x" + saved_height );
                Screen.SetResolution( saved_width, saved_height, false );
            }
        }
    }
}
#endif