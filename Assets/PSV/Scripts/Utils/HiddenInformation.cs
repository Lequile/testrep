﻿//#define NO_PSV_AUDIO
#define PSV_CORE
//#define NO_NOTIFICATIONS
#if !NO_PSV_ADS
#define CAS_ADS
#endif

#if true
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Text.RegularExpressions;
#if CAS_ADS
using CAS;
#endif
#if PSV_CORE
using PSV.IAP;
#endif
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

using UnityEngine.UI;
#pragma warning disable 0618
namespace PSV
{
    [DisallowMultipleComponent]
    [AddComponentMenu( "Miscellaneous/Hidden Information", 1000 )]
    public class HiddenInformation : MonoBehaviour, IPointerClickHandler
    {
        private const string sceneName = "HiddenInfo";
        private const string passValue = "1606";
#if UNITY_EDITOR
        private const int required_clicks = 3;
#else
        private const int required_clicks = 5;
#endif

        [Header( "Password: " + passValue + " Information will be show after 5 pointer clicks." )]
        public bool active = true;

        private int clicksInRow = 0;

        public void OnMouseDown()
        {
            OnPointerClick( null );
        }

        public void OnPointerClick( PointerEventData data = null )
        {
            if (!active)
                return;
            if (clicksInRow == 0)
                Invoke( "ClearCountClicks", 8.0f );
            clicksInRow++;
            if (clicksInRow > required_clicks)
            {
                CancelInvoke();
                clicksInRow = 0;
                ShowAccessWindow();
            }
        }

        public static void ShowAccessWindow()
        {
            SceneManager.CreateScene( sceneName );
            ServiceFade.FadeScreen( 1.0f, 0.0f, null );
            ServiceFade.ShowLoadingUI();
            string currentScene = SceneManager.GetActiveScene().name;
            SceneManager.UnloadScene( currentScene );

            var cameraObj = new GameObject( "Camera", typeof( AudioListener ) );
            var camera = cameraObj.AddComponent<Camera>();
            camera.clearFlags = CameraClearFlags.SolidColor;
            camera.backgroundColor = new Color32( 140, 57, 57, 255 );
            if (Application.isEditor)
            {
                ShowWindow( currentScene );
            }
            else
            {
                new AccessWindow( passValue, ShowWindow, currentScene, true );
#if CAS_ADS
                var smallBanner = AdsManager.smallBannerLifecycle;
                if (smallBanner != null)
                    smallBanner.OverrideSettings( false );
#endif
                ServiceFade.HideLoadingUI();
                ServiceFade.FadeScreen( 0.0f, 0.5f, null );
            }
        }

        public static void ShowWindow( string backToSceneName )
        {
            new HiddenWindow( backToSceneName, HiddenWindow.CreateCanvas() );
#if CAS_ADS
            var smallBanner = AdsManager.smallBannerLifecycle;
            if (smallBanner != null)
                smallBanner.OverrideSettings();
#endif
            ServiceFade.FadeScreen( 0.0f, 0.5f, null );

        }

        private void ClearCountClicks()
        {
            clicksInRow = 0;
        }

        public class AccessWindow : CanvasConstructor
        {
            public event Action<string> OnSuccses;
            public readonly InputField passwordInput;
            public readonly string targetValue;
            public bool destroyWindowOnSuccses;
            public string succsesArg;

            public AccessWindow( string password, Action<string> OnSuccses, string succsesArg, bool destroyWindowOnSuccses = true )
                : base( "AccessWindow", HiddenWindow.CreateCanvas() )
            {
                this.OnSuccses = OnSuccses;
                this.succsesArg = succsesArg;
                targetValue = password;
                this.destroyWindowOnSuccses = destroyWindowOnSuccses;

                baseTran.anchorMin = new Vector2( 0.2f, 0.2f );
                baseTran.anchorMax = new Vector2( 0.8f, 0.8f );
                var layout = baseTran.gameObject.AddComponent<VerticalLayoutGroup>();
                layout.spacing = 10.0f;

                passwordInput = AddInputField( baseTran );
                passwordInput.contentType = InputField.ContentType.Pin;
                passwordInput.characterValidation = InputField.CharacterValidation.Integer;
                passwordInput.onEndEdit.AddListener( CheckValidPassword );
                AddButton( baseTran, "Enter", CheckValidPassword );
                AddButton( baseTran, "Cancel", LoadFirstScene );
            }


            private void CheckValidPassword()
            {
                CheckValidPassword( passwordInput.text );
            }

            private void CheckValidPassword( string content )
            {
                if (OnSuccses != null && content == targetValue)
                {
                    if (destroyWindowOnSuccses)
                        DestroyWindow();
                    OnSuccses( succsesArg );
                }
            }

            public void DestroyWindow()
            {
                Destroy( baseTran.parent.gameObject );
            }

            public void LoadFirstScene()
            {
#if CAS_ADS
                AdsManager.ResetLastInterstitialTime();
#endif
                SceneManager.LoadScene( succsesArg );
            }
        }

        public class HiddenWindow : CanvasConstructor
        {
            private const float rightScrollOffset = 25.0f;
            public string backToSceneName;
            private RectTransform mainContentTran;
            private ScrollRect mainScrollRect;
            private float safeAreaTopOffset;

#if CAS_ADS
            private IMediationManager adManager;
            public Toggle debugToggle;
            public Toggle bannerDownToggle;
            public Toggle enabledTypeToggle;
            private Toggle bannerVisibleToggle;
            public Text adsEnabledInfo;

            private Image bannerPlace;

            private Text[] lastAdInfo = new Text[4];
            private Text[] currentAdInfo = new Text[4];
            private AdListener[] adListeners = new AdListener[4];
            private int interstiitalInterval;
#endif

            public static RectTransform CreateCanvas()
            {
                RectTransform canvasTran;
                Canvas val = CreateCanvas( "HiddenInfo", RenderMode.ScreenSpaceOverlay, out canvasTran );
                val.sortingOrder = -500;
                val.GetComponent<CanvasScaler>().referenceResolution = new Vector2( 700.0f, 700.0f );
                return canvasTran;
            }

            public HiddenWindow( string backToSceneName, RectTransform parent ) : base( sceneName, parent )
            {
                this.backToSceneName = backToSceneName;

                var size = parent.sizeDelta;

                var heightPixels = Screen.height;
                var safeAreaRect = Screen.safeArea;
                safeAreaTopOffset = safeAreaRect.top / heightPixels * size.y;
                
                CoroutineHandler.Start( CreateGUI() );
            }

            #region Main GUI constructor
            private IEnumerator CreateGUI()
            {
                float contentHeight = 5.0f + safeAreaTopOffset;

                Image background = baseObject.AddComponent<Image>();
                background.color = new Color( 0, 0.208f, 0.208f, 255 );
                RectTransform mainContentViewRect = CreateMainContentView();
                CreateMainContentViewRect( mainContentViewRect );
                Text mainInfoText = CreateTextInfoView( contentHeight );

                var infoBuilder = new System.Text.StringBuilder();
                AddProjectInfo( infoBuilder );
                AddAdsInfo( infoBuilder );
                AddSceneOffersInfo( infoBuilder );

#if !NO_NOTIFICATIONS && !UNITY_IOS
                infoBuilder.AppendLine();
                infoBuilder.Append( NativeNotifications.GetAllNotifications() );
#endif
                mainInfoText.text = infoBuilder.ToString();
                var mainInfoHeight = mainInfoText.text.Split( '\n' ).Length * 30.0f;
                contentHeight += mainInfoHeight;
                mainInfoText.rectTransform.sizeDelta = new Vector2( 0.0f, mainInfoHeight );

                yield return null;
                var secondContentTran = AddVertical( mainContentTran );
                {
                    const float height = 450.0f;
                    secondContentTran.pivot = Vector2.up;
                    secondContentTran.sizeDelta = new Vector2( 0.0f, height ); // 500
                    secondContentTran.anchorMin = Vector2.up;
                    secondContentTran.anchorMax = Vector2.one;
                    secondContentTran.anchoredPosition = new Vector2( 0.0f, -contentHeight );
                    CreateUtilsHorizontalView( secondContentTran );
                    CreateIAPHorizontalView( secondContentTran );
                    CreateGameplayHorizontalView( secondContentTran );
                    CreateAdsControllsHorizontalView( secondContentTran );
                    contentHeight += height;
                }

                yield return null;
#if CAS_ADS
                adManager = MobileAds.BuildManager().Initialize();

                lastAdInfo[( int )AdType.Banner] = CreateInfoFiled( "Last Banner Info", ref contentHeight );
                UpdateLastInfo( AdType.Banner );

                lastAdInfo[( int )AdType.Interstitial] = CreateInfoFiled( "Last Interstitial Info", ref contentHeight );
                UpdateLastInfo( AdType.Interstitial );

                lastAdInfo[( int )AdType.Rewarded] = CreateInfoFiled( "Last Banner Info", ref contentHeight );
                OnRewardedrResult( string.Empty );

                interstiitalInterval = MobileAds.settings.interstitialInterval;
                MobileAds.settings.interstitialInterval = 0;
                for (int i = 0; i < 3; i++)
                {
                    currentAdInfo[i] = CreateInfoFiled( "Current Ad Info", ref contentHeight );
                    if (adManager.IsReadyAd( ( AdType )i ))
                        Manager_OnLoadedAd( ( AdType )i );
                    else
                        Manager_OnFailedToLoadAd( ( AdType )i, "unknown" );
                    var state = CreateInfoFiled( "Current Ad State", ref contentHeight );
                    var adListener = new AdListener( ( AdType )i, state );
                    adListeners[i] = adListener;
                }
                adManager.OnLoadedAd += Manager_OnLoadedAd;
                adManager.OnFailedToLoadAd += Manager_OnFailedToLoadAd;

                var listener = adListeners[( int )AdType.Banner];
                adManager.OnBannerAdShown += listener.OnShown;
                adManager.OnBannerAdFailedToShow += listener.OnShowFailed;
                adManager.OnBannerAdClicked += listener.OnClick;
                adManager.OnBannerAdHidden += listener.OnClosed;

                AdsManager.OnBannerShown += RefreshBannerPlace;
                AdsManager.OnBannerHidden += RefreshBannerPlace;

                listener = adListeners[( int )AdType.Interstitial];
                adManager.OnInterstitialAdShown += listener.OnShown;
                adManager.OnInterstitialAdFailedToShow += listener.OnShowFailed;
                adManager.OnInterstitialAdClicked += listener.OnClick;
                adManager.OnInterstitialAdClosed += listener.OnClosed;

                listener = adListeners[( int )AdType.Rewarded];
                adManager.OnRewardedAdShown += listener.OnShown;
                adManager.OnRewardedAdFailedToShow += listener.OnShowFailed;
                adManager.OnRewardedAdClicked += listener.OnClick;
                adManager.OnRewardedAdCompleted += listener.OnCompleted;
                adManager.OnRewardedAdClosed += listener.OnClosed;

                RectTransform bannerPlaceRect;
                var bannerPlaceObj = AddObject( "BannerPlace", baseTran, out bannerPlaceRect );
                bannerPlace = bannerPlaceObj.AddComponent<Image>();
                bannerPlace.color = new Color( 0.52f, 0.25f, 0.25f );
                bannerPlaceObj.AddComponent<Button>()
#if !NO_PSV_AUDIO
                    .onClick.AddListener( ButtonClickSound.Play );
#else
                    ;
#endif
                RefreshBannerPlace();
#endif
                mainContentTran.sizeDelta = new Vector2( -rightScrollOffset, contentHeight );
            }

            private void CreateAdsControllsHorizontalView( RectTransform secondContentTran )
            {
#if CAS_ADS
                var horizontalAdsShowFullscreen = AddHorizontal( secondContentTran );
                {
                    AddButton( horizontalAdsShowFullscreen, "Interstitial", OnShowInterstitial );
                    AddButton( horizontalAdsShowFullscreen, "Rewarded", OnShowRewarded );
                    bannerVisibleToggle = AddToggle( horizontalAdsShowFullscreen, "Small Banner",
                          AdsManager.smallBannerLifecycle != null && AdsManager.smallBannerLifecycle.IsVisible(),
                          OnToggledBanner );
                }

                var horizontalMoveBanner = AddHorizontal( secondContentTran );
                {
                    bannerDownToggle = AddToggle( horizontalMoveBanner, "Ad Bot|Top", true, MoveBannerTopBot );
                    AddButton( horizontalMoveBanner, "Ad Left", MoveBannerLeft );
                    AddButton( horizontalMoveBanner, "Ad Center", MoveBannerCenter );
                    AddButton( horizontalMoveBanner, "Ad Right", MoveBannerRight );
                }

                var horizontalNativeActions = AddHorizontal( secondContentTran );
                {
                    var maangersCount = MobileAds.BuildManager().managersCount;
                    for (int i = 0; i < maangersCount; i++)
                    {
                        int index = i;
                        AddButton( horizontalNativeActions, "Open Debugger " + i,
                            () => OpenMediationDebugger( index ) );
                    }

                    AddButton( horizontalNativeActions, "ValidateAd", MobileAds.ValidateIntegration );
                }
#endif
            }

            private void CreateGameplayHorizontalView( RectTransform secondContentTran )
            {
                var horizontalGameplay = AddHorizontal( secondContentTran );
                {
#if !NO_PSV_AUDIO
                    AddToggle( horizontalGameplay, "Enable Music", !AudioController.GetGroup( StreamGroup.MUSIC ).IsMuted(),
                        ( toggle, enabled ) => AudioController.GetGroup( StreamGroup.MUSIC ).Mute( !enabled, false ) );
#endif

#if CAS_ADS
                    debugToggle = AddToggle( horizontalGameplay, "Debug Ads", MobileAds.settings.isDebugMode,
                        ( toggle, enabled ) => MobileAds.settings.isDebugMode = enabled );
#endif

#if !NO_NOTIFICATIONS && !UNITY_IOS
                    AddButton( horizontalGameplay, "Test Push", CallNotification );
#endif
                    //AddToggle( horizontalGameplay, "Experimental mode", PSVSettings.settings.experimental_logic, OnChangeExperimentalMode );
#if ASSET_BUNDLES_STORAGE
                    AddButton( horizontalGameplay, "Update bundles", AssetBundles.BundlesManager.UpdateAndReloadAllBundles );
#endif
                }
            }

            private void CreateIAPHorizontalView( RectTransform secondContentTran )
            {
#if PSV_CORE
                var horizontalIAP = AddHorizontal( secondContentTran );
                {
                    var allProducts = PSVSettings.settings.products_SKU;
                    for (int i = 0; i < allProducts.Length; i++)
                    {
                        string product = allProducts[i].product;
                        AddToggle( horizontalIAP, product, BillingManagerBase.IsProductPurchased( product ), ToggleIAPProduct );
                    }
                }
#endif
            }

            private void CreateUtilsHorizontalView( RectTransform secondContentTran )
            {
                var horizontalUtils = AddHorizontal( secondContentTran );
                {
                    AddButton( horizontalUtils, "Rate app",
                        () => Application.OpenURL( ConstSettings.GetApplicationMarketURL() ) );
                    AddToggle( horizontalUtils, "Test Helper", FindObjectOfType<TestHelper>(), ToggleTestHelper );

#if PSV_CORE
                    AddToggle( horizontalUtils, "Unlock progress", PSVSettings.isUnlockedProgress,
                        ( toggle, enable ) => PSVSettings.isUnlockedProgress = enable );
#endif
                    AddButton( horizontalUtils, "Clear data", PlayerPrefs.DeleteAll );
                    AddButton( horizontalUtils, "Close", CloseWindow );
                }
            }

            private Text CreateInfoFiled( string objName, ref float contentHeight )
            {
                RectTransform rectTran;
                const float height = 32.0f;
                AddObject( objName, mainContentTran, out rectTran,
                            new Vector4( 0.0f, 1.0f, 1.0f, 1.0f ),
                            new Rect( 0.0f, -contentHeight, 0.0f, height ),
                            0.5f, 1.0f );
                var infoText = AddText( rectTran, new Vector2( 0.05f, 0.0f ), Vector2.one );
                infoText.color = Color.white;
                infoText.alignment = TextAnchor.MiddleLeft;
                contentHeight += height;
                return infoText;
            }

            private static void AddProjectInfo( StringBuilder infoBuilder )
            {
                infoBuilder.Append( Application.identifier )
                            .Append( ": <b>" ).Append( Application.version )
                            .Append( "</b>\nUnity: <b>" ).Append( Application.unityVersion )
                            .Append( "</b> Prototype: <b>" ).Append( ConstSettings.version )
#if CAS_ADS
                            .Append( "</b> CAS: <b>" ).Append( CAS.MobileAds.GetSDKVersion() )
#endif
                            .Append( "</b>\nCore: " ).Append( SystemInfo.processorCount ).Append( "x" )
                            .Append( SystemInfo.processorFrequency ).Append( "hz " )
                            .Append( SystemInfo.operatingSystem )
                            .Append( " RAM: " ).Append( SystemInfo.systemMemorySize )
                            .Append( " GPU: " ).Append( SystemInfo.graphicsMemorySize );
            }

            private static void AddAdsInfo( StringBuilder infoBuilder )
            {
#if CAS_ADS
                infoBuilder.Append( "\nAudience:<b>" )
                           .Append( MobileAds.settings.taggedAudience )
                           .Append( "</b>|Inter delay:<b>" )
                           .Append( MobileAds.settings.interstitialInterval )
                           .Append( "</b>|Banner refresh:<b>" )
                           .Append( MobileAds.settings.bannerRefreshInterval )
                           .Append( "</b>|LoadMode:<b>" )
                           .Append( MobileAds.settings.loadingMode )
                           .Append( "</b>|Consent:<b>" )
                           .Append( MobileAds.settings.userConsent )
                           .Append( "</b>" );
#endif
            }

            private void AddSceneOffersInfo( StringBuilder infoBuilder )
            {
#if PSV_CORE
                infoBuilder.Append( "\nRate Me " );
                try
                {
                    var instance = ( SceneManagement.SceneOfferBase )Activator.CreateInstance(
                        GetType().Assembly.GetType( "RateMePlugin.RateMeOffer" ) );

                    if (instance.IsEmpty())
                    {
                        infoBuilder.Append( "is <b>disabled!</b>" );
                    }
                    else
                    {
                        infoBuilder.Append( "after: <b>" );
                        foreach (string one in instance.ScenesList())
                            infoBuilder.Append( one ).Append( ' ' );
                        infoBuilder.Append( "</b>scenes. " );
                        float interval = instance.Interval( null );
                        if (interval == float.MaxValue)
                            infoBuilder.Append( "<b>Once per session</b>" );
                        else
                            infoBuilder.Append( "With interval: <b>" ).Append( ( int )interval ).Append( "</b> sec" );
                    }
                }
                catch (Exception e)
                {
                    e.LogException();
                    infoBuilder.Append( "is <b>not defined!</b>" );
                }
#endif
            }

            private Text CreateTextInfoView( float contentHeight )
            {
                Text mainInfoText = AddText( mainContentTran, Vector2.up, Vector2.one );
                mainInfoText.alignment = TextAnchor.UpperCenter;
                mainInfoText.color = Color.white;
                mainInfoText.resizeTextMaxSize = 30;
                mainInfoText.rectTransform.pivot = new Vector2( 0.5f, 1.0f );
                mainInfoText.rectTransform.anchoredPosition = new Vector2( 0.0f, -contentHeight );
                mainInfoText.gameObject.AddComponent<Outline>();
                return mainInfoText;
            }

            private void CreateMainContentViewRect( RectTransform mainContentViewRect )
            {
                RectTransform scrollRect;
                {
                    var scrollObj = AddObject( "Scroll", mainContentViewRect, out scrollRect,
                        new Vector4( 1, 0, 1, 1 ), new Rect( 0, 0, 25, 0 ), 1 );
                    var scrollBar = scrollObj.AddComponent<Scrollbar>();
                    scrollBar.direction = Scrollbar.Direction.BottomToTop;

                    var scrollImage = scrollObj.AddComponent<Image>();
                    scrollImage.color = new Color( 1, 1, 1, 0.7f );
                    scrollBar.targetGraphic = scrollImage;

                    RectTransform handleRect;
                    var handleObj = AddObject( "Handle", scrollRect, out handleRect, Vector4.zero, Rect.zero );
                    handleObj.AddComponent<Image>()
                             .color = new Color( 0.63f, 1.0f, 0.63f, 1.0f );
                    scrollBar.handleRect = handleRect;

                    mainScrollRect.verticalScrollbar = scrollBar;
                    mainScrollRect.verticalScrollbarVisibility = ScrollRect.ScrollbarVisibility.AutoHide;
                }
            }

            private RectTransform CreateMainContentView()
            {
                RectTransform mainContentViewRect;
                mainContentTran = AddRectView( baseTran, 1000.0f, out mainContentViewRect );
                mainScrollRect = mainContentViewRect.GetComponent<ScrollRect>();

                var widthPixels = Screen.width;
                var safeAreaRect = Screen.safeArea;

#if CAS_ADS
                mainContentViewRect.anchorMin = new Vector2( safeAreaRect.left / widthPixels, 0.17f );
#else
                mainContentViewRect.anchorMin = new Vector2( safeAreaRect.left / widthPixels, 0.0f );
#endif
                mainContentViewRect.anchorMax = new Vector2( safeAreaRect.right / widthPixels, 1.0f );
                mainContentViewRect.sizeDelta = Vector2.zero;
                mainContentTran.anchoredPosition = new Vector2( -rightScrollOffset * 0.5f, 0.0f );
                return mainContentViewRect;
            }

            private void CloseWindow()
            {
#if CAS_ADS
                MobileAds.settings.interstitialInterval = interstiitalInterval;

                AdsManager.OnBannerShown -= RefreshBannerPlace;
                AdsManager.OnBannerHidden -= RefreshBannerPlace;

                if (adManager != null)
                {
                    adManager.OnLoadedAd -= Manager_OnLoadedAd;
                    adManager.OnFailedToLoadAd -= Manager_OnFailedToLoadAd;

                    var listener = adListeners[( int )AdType.Banner];
                    adManager.OnBannerAdShown -= listener.OnShown;
                    adManager.OnBannerAdFailedToShow -= listener.OnShowFailed;
                    adManager.OnBannerAdClicked -= listener.OnClick;
                    adManager.OnBannerAdHidden -= listener.OnClosed;

                    listener = adListeners[( int )AdType.Interstitial];
                    adManager.OnInterstitialAdShown -= listener.OnShown;
                    adManager.OnInterstitialAdFailedToShow -= listener.OnShowFailed;
                    adManager.OnInterstitialAdClicked -= listener.OnClick;
                    adManager.OnInterstitialAdClosed -= listener.OnClosed;

                    listener = adListeners[( int )AdType.Rewarded];
                    adManager.OnRewardedAdShown -= listener.OnShown;
                    adManager.OnRewardedAdFailedToShow -= listener.OnShowFailed;
                    adManager.OnRewardedAdClicked -= listener.OnClick;
                    adManager.OnRewardedAdCompleted -= listener.OnCompleted;
                    adManager.OnRewardedAdClosed -= listener.OnClosed;
                }
                AdsManager.ResetLastInterstitialTime();
#endif
                SceneManager.LoadScene( backToSceneName );
#if CAS_ADS
                this.Invoke( AdsManager.RefreshSmallBannerForCurrentScene, 1.0f, false, true );
#endif
            }
            #endregion

            #region Mediation constructor
#if CAS_ADS

            private void OpenMediationDebugger( int managerIndex )
            {
                var manager = MobileAds.BuildManager().WithManagerIdAtIndex( managerIndex ).Initialize();
                if (manager != null)
                    manager.GetType().GetMethod( "TryOpenDebugger" ).Invoke( manager, null );
            }

            private void OnShowRewarded()
            {
                if (AdsManager.TryShowRewardedVideoAd( OnRewardedSuccses, OnRewardedFail, false, true ))
                {
                    AdsManager.OnRewardedClosed += OnFullScreenAdClosed;
                    baseObject.SetActive( false );
                }
            }

            private void OnRewardedSuccses()
            {
                OnRewardedrResult( "Success" );
            }

            private void OnRewardedFail()
            {
                OnRewardedrResult( "Fail" );
            }

            private void OnFullScreenAdClosed()
            {
                baseObject.SetActive( true );
                AdsManager.OnRewardedClosed -= OnFullScreenAdClosed;
                AdsManager.OnInterstitialClosed -= OnFullScreenAdClosed;
                UpdateLastInfo( AdType.Interstitial );
            }

            private void OnShowInterstitial()
            {
                if (AdsManager.ShowInterstitial( true ))
                {
                    baseObject.SetActive( false );
                    AdsManager.OnInterstitialClosed += OnFullScreenAdClosed;
                }
            }

            private void OnToggledBanner( ChangeToggle toggle, bool isEnabled )
            {
                if (AdsManager.smallBannerLifecycle.IsVisible() != isEnabled)
                {
                    if (isEnabled)
                        AdsManager.ShowSmallBanner();
                    else
                        AdsManager.HideSmallBanner();
                }
                if (AdsManager.smallBannerLifecycle != null && bannerPlace)
                    bannerPlace.gameObject.SetActive( !AdsManager.smallBannerLifecycle.IsVisible() );
            }

            private void OnRewardedrResult( string valid )
            {
                var result = new System.Text.StringBuilder( "Last Rewarded: " );
                result.Append( valid );

                string label = AdsManager.GetInfoLastActiveAd( AdType.Rewarded );

                if (!string.IsNullOrEmpty( label ))
                    result.Append( ' ' )
                          .Append( label );
                lastAdInfo[( int )AdType.Rewarded].text = result.ToString();
            }

            private void MoveBannerTopBot( ChangeToggle toggle, bool isBot )
            {
                var position = AdsManager.GetAdPos();
                bool isBotAlready;
                switch (position)
                {
                    default:
                        position = AdPosition.Bottom_Centered;
                        isBotAlready = false;
                        break;
                    case AdPosition.Top_Left:
                        position = AdPosition.Bottom_Left;
                        isBotAlready = false;
                        break;
                    case AdPosition.Top_Right:
                        position = AdPosition.Bottom_Right;
                        isBotAlready = false;
                        break;
                    case AdPosition.Bottom_Centered:
                        position = AdPosition.Top_Centered;
                        isBotAlready = true;
                        break;
                    case AdPosition.Bottom_Left:
                        position = AdPosition.Top_Left;
                        isBotAlready = true;
                        break;
                    case AdPosition.Bottom_Right:
                        position = AdPosition.Top_Right;
                        isBotAlready = true;
                        break;
                }
                if (isBotAlready == isBot)
                    return;
                AdsManager.RefreshSmallBanner( position, default( AdSize ) );
                var rectView = ( RectTransform )mainContentTran.parent;
                if (isBot)
                {
                    rectView.anchorMin = new Vector2( 0.0f, 0.17f );
                    rectView.anchorMax = Vector2.one;
                }
                else
                {
                    rectView.anchorMin = Vector2.zero;
                    rectView.anchorMax = new Vector2( 1.0f, 0.83f );
                }
                RefreshBannerPlace();
            }
            private void MoveBannerRight()
            {
                AdPosition pos = bannerDownToggle.isOn ? AdPosition.Bottom_Right : AdPosition.Top_Right;
                AdsManager.SetBannerPosition( pos );
                RefreshBannerPlace();
            }
            private void MoveBannerCenter()
            {
                AdPosition pos = bannerDownToggle.isOn ? AdPosition.Bottom_Centered : AdPosition.Top_Centered;
                AdsManager.SetBannerPosition( pos );
                RefreshBannerPlace();
            }
            private void MoveBannerLeft()
            {
                AdPosition pos = bannerDownToggle.isOn ? AdPosition.Bottom_Left : AdPosition.Top_Left;
                AdsManager.SetBannerPosition( pos );
                RefreshBannerPlace();
            }
            private void RefreshBannerPlace()
            {
                if (AdsManager.smallBannerLifecycle != null)
                {
                    var visible = AdsManager.smallBannerLifecycle.IsVisible();
                    bannerPlace.gameObject.SetActive( !visible );
                    if (bannerVisibleToggle.isOn != visible)
                        bannerVisibleToggle.isOn = visible;
                    var pos = AdsManager.smallBannerLifecycle.GetPosition();

                    float anchorX = 0.5f;
                    switch (pos)
                    {
                        case AdPosition.Bottom_Left:
                        case AdPosition.Top_Left:
                            anchorX = 0.0f;
                            break;
                        case AdPosition.Bottom_Right:
                        case AdPosition.Top_Right:
                            anchorX = 1.0f;
                            break;
                    }
                    var tran = bannerPlace.rectTransform;
                    tran.anchorMin = new Vector2( anchorX, bannerDownToggle.isOn ? 0.0f : 0.85f );
                    tran.anchorMax = new Vector2( anchorX, bannerDownToggle.isOn ? 0.15f : 1.0f );
                    tran.pivot = new Vector2( anchorX, bannerDownToggle.isOn ? 0.0f : 1.0f );
                    tran.sizeDelta = new Vector2( 350.0f, 0.0f );
                }
            }

            private void UpdateLastInfo( AdType type )
            {
                string label = AdsManager.GetInfoLastActiveAd( type );
                string result = "Last " + type.ToString();
                if (string.IsNullOrEmpty( label ))
                    lastAdInfo[( int )type].text = result + " information not found";
                else
                    lastAdInfo[( int )type].text = result + ": " + label;
            }
#endif
            #endregion

            private void ToggleTestHelper( ChangeToggle toggle, bool enabled )
            {
                if (enabled)
                    ServiceContainer.AddComponent<TestHelper>();
                else
                    Destroy( FindObjectOfType<TestHelper>() );
            }

#if !NO_NOTIFICATIONS && !UNITY_IOS
            private void CallNotification()
            {
                new NativeNotifications
                    .Builder( "Test Title", "Test message" )
                    .DelaySeconds( 10 )
                    .Build();
                Application.Quit();
            }
#endif

#if PSV_CORE
            private void ToggleIAPProduct( ChangeToggle toggle, bool isEnabled )
            {
                var product = BillingManagerBase.GetPropertyByName( toggle.label.text );
                if (isEnabled)
                {
                    typeof( BillingManagerBase )
                        .GetMethod( "PurchaseSucceded", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic )
                        .Invoke( null, new object[] { product.sku } );
                }
                else
                {
                    typeof( BillingManagerBase )
                        .GetMethod( "PurchaseCancelled", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic )
                        .Invoke( null, new object[] { product.sku } );
                }
            }
#endif

#if CAS_ADS
            private void Manager_OnFailedToLoadAd( AdType adType, string error )
            {
                currentAdInfo[( int )adType].text = adType.ToString() + " Ad laod error: " + error;
            }

            private void Manager_OnLoadedAd( AdType adType )
            {
                currentAdInfo[( int )adType].text = adType.ToString() + " Ad: Ready";
            }

            private class AdListener
            {
                public readonly AdType type;
                public Text info;
                private bool completeCalled = false;

                public AdListener( AdType type, Text info )
                {
                    this.type = type;
                    this.info = info;
                }

                public void OnShown()
                {
                    completeCalled = false;
                    info.text = type.ToString() + " Ad: Visible";
                }

                public void OnShowFailed( string error )
                {
                    info.text = type.ToString() + " Ad Show failed: " + error;
                }

                public void OnClick()
                {
                    info.text = type.ToString() + " Ad: Clicked";
                }

                public void OnCompleted()
                {
                    completeCalled = true;
                    info.text = type.ToString() + " Ad: Completed";
                }

                public void OnClosed()
                {
                    info.text = type.ToString() +
                        ( completeCalled ? " Ad: Impression and Completed" : " Ad: Successful Impression" );
                }
            }
#endif
        }

        [AddComponentMenu( "" )]
        public sealed class TestHelper : MonoBehaviour
        {
            private Rect mainBtnRect;
            private Rect systemInfoRect;
            private Rect errorContentRect;
            private EventSystem eventSystem;
            private GUIStyle btnStyle = null;
            private GUIStyle labelStyle = null;
            private int errorsCount = 0;
            private int additionalLine = 0;
            private int bannerVisibleTime = 0;
            private int interShownCount = 0;
            private int lastFPS;
            private float nextUpdateTime;
            private bool open = true;
            private bool openErrorsLog = false;
            private bool drag;
            private bool firstChange;
            private string totalSystemInfo;
            private string errorContent;
            private StringBuilder errorsList = new StringBuilder();
            private Vector2 scrollPosition;
            private Vector2 startTouch;

            private void OnEnable()
            {
                int w = Screen.width;
                int h = Screen.height;
                if (w < h)
                {
                    int temp = w;
                    w = h;
                    h = temp;
                }
                mainBtnRect = new Rect( Screen.width * 0.01f, Screen.height * 0.5f, w * 0.065f, h * 0.06f );
                systemInfoRect = new Rect( 5f, 0f, Screen.width * 0.5f, Screen.height * 0.5f );

                Application.logMessageReceived += HandleLog;
#if CAS_ADS
                AdsManager.OnInterstitialShown += AdsManager_OnInterstitialShown;
                AdsManager.OnRewardedShown += AdsManager_OnInterstitialShown;
#endif
            }

            private void OnDisable()
            {
                Application.logMessageReceived -= HandleLog;
#if CAS_ADS
                AdsManager.OnInterstitialShown -= AdsManager_OnInterstitialShown;
                AdsManager.OnRewardedShown -= AdsManager_OnInterstitialShown;
#endif
            }

            private void HandleLog( string condition, string stackTrace, LogType type )
            {
                if (type == LogType.Log || type == LogType.Warning)
                    return;

                errorsCount++;
                errorsList.Append( "-- " ).AppendLine( condition );

                additionalLine += condition.Length / 55;

                var stackSplited = stackTrace.Split( new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries );
                for (int i = 0; i < 3 && i < stackSplited.Length; i++)
                {
                    additionalLine += stackSplited[i].Length / 55;

                    errorsList.Append( " > " ).AppendLine( stackSplited[i] );
                }


                errorsList.AppendLine();
            }

            private void AdsManager_OnInterstitialShown()
            {
                interShownCount++;
            }

            private void LateUpdate()
            {
                if (Input.GetMouseButtonDown( 0 ))
                {
                    var mouse = Input.mousePosition;
                    mouse.y = Screen.height - mouse.y;
                    if (mainBtnRect.Contains( mouse ))
                    {
                        startTouch = mainBtnRect.center - ( Vector2 )mouse;
                        drag = true;
                    }
                }
                if (drag && Input.GetMouseButton( 0 ))
                {
                    var mouse = Input.mousePosition;
                    mouse.y = Screen.height - mouse.y;
                    mainBtnRect.center = ( Vector2 )mouse + startTouch;
                }
                if (Input.GetMouseButtonUp( 0 ))
                    drag = false;

                var curTime = Time.unscaledTime;
                if (nextUpdateTime < curTime)
                {
                    nextUpdateTime = curTime + 1.0f;
                    lastFPS = ( int )( 1.0f / Time.unscaledDeltaTime );
#if CAS_ADS
                    bool bannerVisible = AdsManager.GetBannerVisible();
                    if (bannerVisible)
                        bannerVisibleTime++;
#endif

                    if (open)
                    {
                        var infoBuilder = new StringBuilder();
                        infoBuilder.Append( "Scene: <b>" ).Append( SceneManager.GetActiveScene().name )
                                   .Append( "</b> : " ).Append( ( int )Time.timeSinceLevelLoad ).Append( " sec" );
#if CAS_ADS
                        infoBuilder.Append( "\nInter: " );
                        if (AdsManager.IsAdReady( AdType.Interstitial ))
                            infoBuilder.Append( "Ready" );
                        else
                            infoBuilder.Append( "Loading" );
                        infoBuilder.Append( " (Count " ).Append( interShownCount ).Append( ") " )
                                   .Append( AdsManager.GetInfoLastActiveAd( AdType.Interstitial ) );
                        infoBuilder.Append( "\nBanner: " );
                        if (bannerVisible)
                            infoBuilder.Append( bannerVisibleTime ).Append( " sec visible" );
                        else
                            infoBuilder.Append( "HIDDEN (Total " ).Append( bannerVisibleTime ).Append( " sec)" );
#endif
                        var currTime = ( int )curTime;
                        infoBuilder.Append( "\nTime: " ).AppendFormat( "{0:D2}:{1:D2}", currTime / 60, currTime % 60 );
                        if (lastFPS < 15)
                            infoBuilder.Append( "<color=red>" );
                        else if (lastFPS < 50)
                            infoBuilder.Append( "<color=orange>" );
                        infoBuilder.Append( " FPS: " ).Append( lastFPS );
                        if (lastFPS < 50)
                            infoBuilder.Append( "</color>" );
                        totalSystemInfo = infoBuilder.ToString();
                    }
                }
            }

            private void OnGUI()
            {
                if (btnStyle == null)
                {
                    btnStyle = new GUIStyle( "Button" );
                    labelStyle = new GUIStyle( "label" );
                    var fontSize = ( int )( mainBtnRect.height * 0.5f );
                    btnStyle.fontSize = fontSize;
                    labelStyle.fontSize = fontSize;
                }
                if (openErrorsLog)
                {
                    var screenW = Screen.width;
                    var screenH = Screen.height;


                    //new Rect( 0, 0, screenW - 30, screenH )
                    scrollPosition = GUI.BeginScrollView( new Rect( 5, 0, Screen.width - 15, screenH ), scrollPosition, errorContentRect );
                    var btnRect = errorContentRect;
                    btnRect.xMax -= 20;
                    if (GUI.Button( btnRect, "", btnStyle ))
                    {
                        errorContent = null;
                        eventSystem.enabled = true;
                        eventSystem = null;
                        openErrorsLog = false;
                    }
                    GUI.Label( errorContentRect, errorContent, labelStyle );
                    GUI.EndScrollView();
                    return;
                }
                if (open != GUI.Toggle( mainBtnRect, open, open ? "Hide" : lastFPS.ToString(), btnStyle ))
                {
                    open = !open;
                }
                if (open)
                {
                    GUI.Label( systemInfoRect, totalSystemInfo, labelStyle );

                    Rect sliderRect = mainBtnRect;
                    sliderRect.x += mainBtnRect.width * 1.1f;
                    sliderRect.width = mainBtnRect.width * 3.0f;

                    int newScale = ( int )GUI.HorizontalSlider( sliderRect, Time.timeScale * 2.0f, 0.0f, 6.0f, GUI.skin.horizontalScrollbar, GUI.skin.horizontalScrollbarThumb );

                    if (GUI.changed)
                    {
                        Time.timeScale = newScale * 0.5f;
                        if (!firstChange)
                        {
#if !NO_PSV_AUDIO
                            foreach (var group in PSV.Audio.AudioControllerBase.GetGroups())
                                group.HandleTimeScale = true;
#endif
                            firstChange = true;
                        }
                    }

                    sliderRect.y -= mainBtnRect.height;
                    GUI.Label( sliderRect, "<b>Scale x" + Time.timeScale.ToString() + "</b>", labelStyle );
                }
                if (errorsCount > 0)
                {
                    Rect errorsRect = mainBtnRect;
                    errorsRect.yMin = mainBtnRect.yMax + 3.0f;
                    errorsRect.yMax = errorsRect.yMin + mainBtnRect.height;
                    errorsRect.width *= 2.0f;
                    if (GUI.Button( errorsRect, "ERRORS: " + errorsCount, btnStyle ))
                    {
                        errorContent = errorsList.ToString();
                        var countLines = Regex.Matches( errorContent, "\n" ).Count;
                        Debug.Log( "Font size = " + labelStyle.fontSize );
                        Debug.Log( countLines );
                        errorContentRect = new Rect( 0, 0, Screen.width * 0.95f, ( labelStyle.lineHeight ) * ( countLines + additionalLine ) );
                        scrollPosition.y = errorContentRect.height;
                        eventSystem = EventSystem.current;
                        eventSystem.enabled = false;
                        openErrorsLog = true;
                    }
                }
            }
        }

        [AddComponentMenu( "" )]
        private class PointerClickHandler : MonoBehaviour, IPointerClickHandler
        {
            public event Action click;

            public void OnPointerClick( PointerEventData eventData = null )
            {
                if (click != null)
                    click();
            }

            public PointerClickHandler OnClick( Action click )
            {
                this.click += click;
                return this;
            }
        }
    }
}
#pragma warning restore 0618
#endif