﻿#if PSV_SPINE // Enable if you use this extension
using UnityEngine;
using System.Collections.Generic;
using Spine.Unity;

namespace PSV
{
    public static partial class UtilsSpine
	{
		static public void SetSpineAnimation( this SkeletonAnimation anim, string clip1, string clip2 = "", bool delay = false, float from = 0, float to = 0 )
		{
			if (anim != null)
			{
				if (anim.state == null)
				{
					anim.Initialize( false );
				}
				if (delay)
				{
                    DelayedCallHandler.DelayedCallScaled( () => SetSpineAnimation( anim, clip1, clip2, false ), Random.Range( from, to ), false );
				}
				else
				{
					if (clip1 != "")
					{
						anim.state.SetAnimation( 0, clip1, clip2 == "" );
						if (clip2 != "")
						{
							anim.state.AddAnimation( 0, clip2, true, 0 );
						}
					}
				}
			}
			else
			{
				Debug.Log( "SetAnimation: Animation is null" );
			}
		}
	}
}
#endif
