﻿#if UNITY_EDITOR || UNITY_STANDALONE
using PSV.Utility;
using UnityEngine;

namespace PSV.Initialize
{
    public static class ScreenCaptureInit
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        public static void StaticAwake()
        {
            AwakeStatic.Done( typeof( ScreenCaptureInit ) );
            KeyCode notResizedCapture;
#if UNITY_EDITOR
            notResizedCapture = ( KeyCode )UnityEditor.EditorPrefs.GetInt( ScreenShootTaker.editorHotkeyPref, ( int )KeyCode.F11 );
#else
            notResizedCapture = KeyCode.None;
#endif
#if UNITY_STANDALONE
            var instance = 
#endif
            ScreenShootTaker.Create( PSVSettings.settings.resized_capture_hotkey,
                                notResizedCapture,
                                PSVSettings.settings.locking_capture_aspects );

#if UNITY_STANDALONE
            if (PSVSettings.settings.portrait_orientation_only)
            {
                instance.OnScreenShutter += ResolutionCheckerStandalone.OnScreenShutter;
                ResolutionCheckerStandalone.Create();
            }
#endif
        }

    }
}
#endif