﻿#if UNITY_ANALYTICS

using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.Scripting;

[assembly: AlwaysLinkAssembly]

namespace PSV.AnalyticsLog
{
    public static class AnalyticsUnityLog
    {
        [RuntimeInitializeOnLoadMethod( RuntimeInitializeLoadType.BeforeSceneLoad )]
        public static void Subscrbe()
        {
            AwakeStatic.Done( typeof( AnalyticsUnityLog ) );
            //Analytics.enabled = false;
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                Analytics.limitUserTracking = true;
                Analytics.deviceStatsEnabled = false;
            }
            AnalyticsManager.OnLogEvent -= Log;
            AnalyticsManager.OnLogEvent += Log;

            //try
            //{
            //    Type.GetType( "PSV.AnalyticsLog.AnalyticsFirebase, PSV.AnalyticsFirebase" )
            //        .GetMethod( "Subscribe",
            //            System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public )
            //        .Invoke( null, null );
            //}
            //catch (Exception e)
            //{
            //    Debug.Log( "Failed to subscribe Firebase Analytics from Unity Analytics with error: " + e.Message );
            //}
        }

        public static void Log( string _event, Dictionary<string, object> fields )
        {
            try
            {
                // Do not change `fields` dictionary! Other analytics receivers should still use source 
                Dictionary<string, object> result;
                switch (_event)
                {
                    case "CloseApplication":
                    case "StartApplication":
                        Analytics.CustomEvent( _event );
                        return;
                    case "LogScreen":
                        if (fields == null || fields.Count == 0)
                            return;
                        result = new Dictionary<string, object>();
                        result.Add( "screen", fields[AnalyticsManager.default_field_name] );
                        break;
                    default:
                        if (fields == null || fields.Count == 0)
                        {
                            Analytics.CustomEvent( _event );
                            return;
                        }
                        result = new Dictionary<string, object>( fields );
                        break;
                }

                foreach (var key in result.Keys)
                {
                    var item = result[key];
                    if (item is string || item is float || item is int)
                        continue;
                    result[key] = item.ToString();
                }
                Analytics.CustomEvent( _event, result );
            }
            catch (System.Exception e)
            {
                e.LogException( "Unity analytics" );
            }
        }
    }
}
#endif