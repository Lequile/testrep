﻿using UnityEngine;

namespace PSV.SceneManagement
{
    [DisallowMultipleComponent]
    [AddComponentMenu( "Miscellaneous/Switch Back To Scene", 100 )]
    public class SwitchBackToScene : MonoBehaviour
    {
        [Header( "Call change scene on press Escape" )]
        [SceneName]
        public string sceneName;
        [Header( "Recomended use safe field 'sceneName'." )]
        [Header( "This field will be use only if 'sceneName' empty." )]
        public Scenes target_scene;

        private void OnEnable()
        {
            KeyListener.AddKey( KeyCode.Escape, ProcessKey );
        }

        private void OnDisable()
        {
            KeyListener.OnKeyPressed -= ProcessKey;
        }

        private void ProcessKey( KeyCode key )
        {
            if (key == KeyCode.Escape)
                Action();
        }

        public void Action()
        {
            if(string.IsNullOrEmpty( sceneName ))
            SceneLoader.SwitchToScene( target_scene );
            else
                SceneLoader.SwitchToScene( sceneName );

        }
    }
}