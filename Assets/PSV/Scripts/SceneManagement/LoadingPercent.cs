﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace PSV.SceneManagement
{
    [RequireComponent(typeof(Text))]
	public class LoadingPercent : MonoBehaviour 
	{
        private Text percent_field;

        private void Awake()
        {
            percent_field = GetComponent<Text>();
        }

        private void OnEnable()
        {
            percent_field.text = string.Empty;
            SceneLoader.OnLoadProgressChange += UpdatePercent;
        }

        private void OnDisable()
        {
            SceneLoader.OnLoadProgressChange -= UpdatePercent;
        }

        private void UpdatePercent(float percent)
        {
            percent_field.text = percent.ToString( "P1" );
        }
    }
}