﻿using UnityEngine;
using UnityEngine.UI;

namespace PSV.SceneManagement
{
    [RequireComponent( typeof( Image ) )]
    public class LoadingIndicator : MonoBehaviour
    {

        private Image
            indicator;

        void Awake()
        {
            indicator = GetComponent<Image>();
        }

        // Use this for initialization
        void UpdateIndicator( float progress )
        {
            //Debug.Log ( "UpdateIndicator " + progress );
            indicator.fillAmount = progress;
        }

        void OnEnable()
        {
            UpdateIndicator( 0f );
            SceneLoader.OnLoadProgressChange += UpdateIndicator;
        }

        void OnDisable()
        {
            SceneLoader.OnLoadProgressChange -= UpdateIndicator;
        }
    }
}