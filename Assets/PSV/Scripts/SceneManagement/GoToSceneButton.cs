﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace PSV.SceneManagement
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Miscellaneous/Go To Scene Button")]
    public class GoToSceneButton : PointerClickBase
    {
        [SceneName]
        public string safeTarget;
        public bool resetGameMode = false;

        [Header("Recomended use safe field 'SafeTarget'.")]
        [Header("This field will be use only if 'SafeTarget' empty.")]
        public Scenes target;

        public override void Click(PointerEventData eventData = null)
        {
            if (resetGameMode)
                AnalyticsManager.game_mode = 0;
            SceneLoader.SwitchToScene(string.IsNullOrEmpty(safeTarget) ? target.ToString() : safeTarget);
        }
    }
}