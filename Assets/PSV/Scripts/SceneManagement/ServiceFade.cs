﻿using UnityEngine;

namespace PSV.Linker
{
    [AddComponentMenu( "UI/PSV/Service Fade" )]
    public class ServiceFade : PSV.ServiceFade
    {
         // Can not be removed because PSV.Core.dll doesn't know about AgetnObject in PSV.Basement
    }
}