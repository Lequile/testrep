﻿using PSV.SceneManagement;
using System;
using UnityEngine.SceneManagement;

namespace PSV
{
    public partial class SceneLoader : SceneLoaderBase
    {
        public delegate void OnSceneLoadedDelegate( Scenes from, Scenes to );
        private static OnSceneLoadedDelegate onSceneLoaded;

        [Obsolete( "Better use SceneManager.activeSceneChanged event instead" )]
        public static event OnSceneLoadedDelegate OnSceneLoaded
        {
            add
            {
                if (onSceneLoaded == null)
                    SceneManager.activeSceneChanged += SceneLoadedReady;
                onSceneLoaded += value;
            }
            remove
            {
                onSceneLoaded -= value;
                if (onSceneLoaded == null)
                    SceneManager.activeSceneChanged -= SceneLoadedReady;
            }
        }

        [Obsolete( "Better use SceneManager.activeSceneChanged event instead" )]
        private static void SceneLoadedReady( Scene last, Scene next )
        {
            if (onSceneLoaded != null)
                onSceneLoaded( last.name.ToEnum<Scenes>( Scenes.None ), next.name.ToEnum<Scenes>( Scenes.None ) );
        }

        public static Scenes GetCurrentScene()
        {
            return GetCurrentSceneName().ToEnum<Scenes>( Scenes.None );
        }

        public static TransitionHandler SwitchToScene( Scenes target_scene, float fade_duration = -1 )
        {
            return SwitchToScene( target_scene.ToString(), fade_duration );
        }

        [Obsolete( "Use SwitchToScene( target_scene, fade_duration...) instead." )]
        public static TransitionHandler SwitchToScene( Scenes target_scene, float fade_duration, LoadSceneMode scene_load_mode )
        {
            return SwitchToScene( target_scene.ToString(), fade_duration, scene_load_mode );
        }

        [Obsolete( "Use SwitchToScene( target_scene, fade_duration...) instead." )]
        public static TransitionHandler SwitchToScene( Scenes target_scene, TransitionMethod transition_type, float fade_duration = -1, LoadSceneMode scene_load_mode = LoadSceneMode.Single )
        {
            return SwitchToScene( target_scene.ToString(), transition_type, fade_duration, scene_load_mode );
        }

        public static TransitionHandler SimulateTransition( Action on_screen_obscured, bool show_ads = true, float override_duration = -1 )
        {
#if !NO_PSV_ADS
            Action adsAction = show_ads ? AdsManager.ShowInterstitial : ( Action )null;
#else
            Action adsAction = null;
            if (show_ads)
                UnityEngine.Debug.LogError( "Simulate Transition with Ads not supported. Required exist PSV.Ads" );
#endif
            return SimulateTransition( on_screen_obscured, adsAction, override_duration );
        }

#if !NO_PSV_ADS
        /// <summary>
        /// Call before Loading scene or simulating transition. Will override settings only for current transition. Call without arguments to reset overrides.
        /// </summary>
        [Obsolete( "Please use final implementation: AdsManager.smallBannerLifecycle.OverrideSettings instead." )]
        public static void OverrideBannerSettings( bool? visible = null, CAS.AdPosition pos = CAS.AdPosition.Undefined )
        {
            if (AdsManager.smallBannerLifecycle != null)
                AdsManager.smallBannerLifecycle.OverrideSettings( visible, pos );
        }
#endif
    }
}