﻿using UnityEngine;
using System.Collections;

namespace PSV.SceneManagement
{
    [RequireComponent(typeof(RectTransform))]
	public class LoadingProgressRect : MonoBehaviour 
	{
        private new RectTransform transform;

        private void Awake()
        {
            transform = GetComponent<RectTransform>();
        }

        private void OnEnable()
        {
            OnLoadProgressChange( 0.0f );
            SceneLoader.OnLoadProgressChange += OnLoadProgressChange;
        }
        private void OnDisable()
        {
            SceneLoader.OnLoadProgressChange -= OnLoadProgressChange;
        }

        private void OnLoadProgressChange( float percent )
        {
            transform.anchorMax = new Vector2( percent, 1.0f );
        }
    }
}