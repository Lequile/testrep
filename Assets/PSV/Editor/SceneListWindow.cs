﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using System.Reflection;
using System;

namespace PSVEditor
{
    public class SceneListWindow : EditorWindow
    {
        [SerializeField]
        private bool timeManagerExpand = false;
        private Vector2 scrollPosition = new Vector2();
        private EditorBuildSettingsScene[] scenes;
        private byte[] sceneStatus;

        private string filterName = string.Empty;
        private MethodInfo drawSearchFieldMethod;
        private OpenSceneMode openMode = OpenSceneMode.Single;
        private bool firstChange;

        [MenuItem( "Window/Scenes List Window" )]
        public static void OpenWindow()
        {
            EditorWindow[] allWindows = Resources.FindObjectsOfTypeAll<EditorWindow>();
            for (int i = 0; i < allWindows.Length; i++)
            {
                if (allWindows[i].titleContent.text == "Hierarchy")
                {
                    var instance = GetWindow<SceneListWindow>( "Scenes List", false, allWindows[i].GetType() );
                    instance.titleContent.image = EditorGUIUtility.FindTexture( "d_UnityEditor.ConsoleWindow" ); // allWindows[i].titleContent.image;
                    return;
                }
            }
            GetWindow<SceneListWindow>( false, "Scenes" );
        }

        private void OnEnable()
        {
            drawSearchFieldMethod = typeof( EditorGUILayout ).GetMethod( "ToolbarSearchField",
                BindingFlags.Static | BindingFlags.NonPublic, null, new[] { typeof( string ), typeof( GUILayoutOption[] ) }, null );
            EditorSceneManager.sceneLoaded += EditorSceneManager_sceneLoaded;
            EditorSceneManager.activeSceneChanged += EditorSceneManager_activeSceneChanged;
            EditorSceneManager.sceneUnloaded += EditorSceneManager_sceneUnloaded;
            EditorApplication.playModeStateChanged += EditorApplication_playModeStateChanged;
        }

        private void EditorApplication_playModeStateChanged( PlayModeStateChange obj )
        {
            RefreshStatus();
        }

        private void EditorSceneManager_sceneLoaded( Scene scene, LoadSceneMode arg1 )
        {
            if (scene.buildIndex > -1 && scene.buildIndex < sceneStatus.Length)
                sceneStatus[scene.buildIndex] = ( byte )Math.Min( sceneStatus[scene.buildIndex] + 1, 2 );
        }

        private void EditorSceneManager_activeSceneChanged( Scene last, Scene next )
        {
            if (last.buildIndex > -1 && last.buildIndex < sceneStatus.Length)
                sceneStatus[last.buildIndex]--;
            if (next.buildIndex > -1 && next.buildIndex < sceneStatus.Length)
                sceneStatus[next.buildIndex] = (byte)Math.Min( sceneStatus[next.buildIndex] + 1, 2);
        }

        private void EditorSceneManager_sceneUnloaded( Scene scene )
        {
            if (scene.buildIndex > -1 && scene.buildIndex < sceneStatus.Length)
                sceneStatus[scene.buildIndex]--;
        }

        private void OnDisable()
        {
            EditorSceneManager.sceneLoaded -= EditorSceneManager_sceneLoaded;
            EditorSceneManager.activeSceneChanged -= EditorSceneManager_activeSceneChanged;
            EditorSceneManager.sceneUnloaded -= EditorSceneManager_sceneUnloaded;
            EditorApplication.playModeStateChanged -= EditorApplication_playModeStateChanged;
        }

        private void OnFocus()
        {
            scenes = EditorBuildSettings.scenes;
            sceneStatus = new byte[scenes.Length];
            RefreshStatus();
        }

        private void RefreshStatus()
        {
            for (int i = 0; i < sceneStatus.Length; i++)
                sceneStatus[i] = 0;
            if (!EditorApplication.isPlaying)
            {
                foreach (var scene in EditorSceneManager.GetSceneManagerSetup())
                {
                    var index = SceneUtility.GetBuildIndexByScenePath( scene.path );
                    if (index > -1 && index < sceneStatus.Length)
                        sceneStatus[index] = ( byte )( scene.isActive ? 2 : ( scene.isLoaded ? 1 : 0 ) );
                }
            }
        }

        private void OnGUI()
        {
            EditorGUILayout.BeginHorizontal();
            openMode = ( OpenSceneMode )EditorGUILayout.EnumPopup( openMode, EditorStyles.toolbarPopup, GUILayout.Width( 55.0f ) );
            filterName = drawSearchFieldMethod.Invoke( null, new object[] { filterName, new GUILayoutOption[0] } ) as string;
            EditorGUILayout.EndHorizontal();
            bool isPlaying = EditorApplication.isPlaying;
            scrollPosition = GUILayout.BeginScrollView( scrollPosition );
            EditorGUI.BeginDisabledGroup( isPlaying );
            for (int i = 0; i < scenes.Length; i++)
            {
                var sceneName = Path.GetFileNameWithoutExtension( scenes[i].path );
                if (sceneName.IndexOf( filterName, System.StringComparison.OrdinalIgnoreCase ) > -1)
                {
                    if (sceneStatus[i] == 2)
                    {
                        var oldColor = GUI.backgroundColor;
                        GUI.backgroundColor = Color.green;
                        GUILayout.Label( sceneName, EditorStyles.miniButton );
                        GUI.backgroundColor = oldColor;
                    }
                    else if (sceneStatus[i] == 1)
                    {
                        var oldColor = GUI.backgroundColor;
                        GUI.backgroundColor = Color.cyan;
                        GUILayout.Label( sceneName, EditorStyles.miniButton );
                        GUI.backgroundColor = oldColor;
                    }
                    else if (GUILayout.Button( sceneName, EditorStyles.miniButton )
                        && ( openMode != OpenSceneMode.Single || EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo() ))
                    {
                        EditorSceneManager.OpenScene( scenes[i].path, openMode );
                        RefreshStatus();
                    }
                }
            }
            EditorGUI.EndDisabledGroup();
            GUILayout.EndScrollView();
            GUILayout.FlexibleSpace();
            timeManagerExpand = GUILayout.Toggle( timeManagerExpand, ( timeManagerExpand ? "Hide" : "Show" ) + " Time Manager",
                EditorStyles.toolbarPopup );
            if (timeManagerExpand)
            {
                EditorGUILayout.BeginHorizontal();
                GUILayout.Label( "Time Scale", GUILayout.ExpandWidth( false ) );
                EditorGUI.BeginChangeCheck();
                Time.timeScale = EditorGUILayout.Slider( Time.timeScale, 0.0f, 3.0f );
                if (EditorGUI.EndChangeCheck())
                {
                    if (!firstChange && EditorApplication.isPlaying)
                    {
#if !NO_PSV_AUDIO
                        foreach (var group in PSV.Audio.AudioControllerBase.GetGroups())
                            group.HandleTimeScale = true;
#endif
                        firstChange = true;
                    }
                }
                EditorGUILayout.EndHorizontal();
                if (isPlaying)
                {
                    EditorGUILayout.LabelField( "Time:", Time.time.ToString() );
                    PSV.PauseManager.SetPause( EditorGUILayout.Toggle( "PSV Pause:", PSV.PauseManager.IsPaused() ) );
                }
                else
                {
                    EditorGUI.BeginDisabledGroup( true );
                    EditorGUILayout.LabelField( "Time:", "Unknown" );
                    EditorGUILayout.Toggle( "PSV Pause:", false );
                    EditorGUI.EndDisabledGroup();
                }
            }
        }

        private void OnInspectorUpdate()
        {
            // Call Repaint on OnInspectorUpdate as it repaints the windows
            // less times as if it was OnGUI/Update
            if (timeManagerExpand && EditorApplication.isPlaying)
                Repaint();
        }
    }
}