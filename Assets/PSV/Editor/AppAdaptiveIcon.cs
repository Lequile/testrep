﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace PSVEditor
{
    public class AppAdaptiveIcon
#if UNITY_2018_1_OR_NEWER
        : AssetPostprocessor
#endif
    {
#if UNITY_2018_1_OR_NEWER
        private static void OnPostprocessAllAssets( string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths )
        {
            if (importedAssets.Length > 0)
                CheckIcon();
        }

        private static void CheckIcon()
        {
            Texture2D[] u_pi = PlayerSettings.GetIconsForTargetGroup( BuildTargetGroup.Unknown );
            if (u_pi.Length == 0 || u_pi[0] == null)
                return;
            PlatformIcon[] pi = PlayerSettings.GetPlatformIcons( BuildTargetGroup.Android, UnityEditor.Android.AndroidPlatformIconKind.Adaptive );
            if (pi.Length > 0)
            {
                Texture2D[] tx = pi[0].GetTextures();
                if (tx.Length < 2)
                {
                    tx = new Texture2D[2];
                }
                if (tx[1] == null)
                {
                    //PlatformIcon[] u_pi = PlayerSettings.GetPlatformIcons( BuildTargetGroup.Standalone, UnityEditor.iOS.iOSPlatformIconKind.Application );
                    tx[1] = u_pi[0];
                    pi[0].SetTextures( tx );
                    Debug.Log( "Android AdaptiveIcons are updated" );
                    PlayerSettings.SetPlatformIcons( BuildTargetGroup.Android, UnityEditor.Android.AndroidPlatformIconKind.Adaptive, pi );
                }
            }
        }

        private static void GetUsedIcons( BuildTargetGroup group, PlatformIconKind kind, List<Texture2D> result )
        {
            PlatformIcon[] adaptiveAndroid = PlayerSettings.GetPlatformIcons( group, kind );
            for (int i = 0; i < adaptiveAndroid.Length; i++)
            {
                var icons = adaptiveAndroid[i].GetTextures();
                for (int j = 0; j < icons.Length; j++)
                {
                    if (icons[i])
                        result.Add( icons[i] );
                }
            }
        }
#endif
        public static bool IsAdaptiveIconExist( BuildTargetGroup target )
        {
#if UNITY_2018_1_OR_NEWER
            switch (target)
            {
                case BuildTargetGroup.Android:
                    var androidIcons = PlayerSettings.GetPlatformIcons( BuildTargetGroup.Android, UnityEditor.Android.AndroidPlatformIconKind.Adaptive );
                    try
                    {
                        return /*androidIcons.Length > 0 && androidIcons[0] != null && */androidIcons[0].GetTexture( 1 ) != null;
                    }
                    catch
                    {
                        return true;
                    }
                    // TODO: check on IOS
            }
#endif
            return true;
        }

        public static List<Texture2D> GetUsedIconsAndroid()
        {
            List<Texture2D> result = new List<Texture2D>();
#if UNITY_2018_1_OR_NEWER
            GetUsedIcons( BuildTargetGroup.Android, UnityEditor.Android.AndroidPlatformIconKind.Adaptive, result );
            GetUsedIcons( BuildTargetGroup.Android, UnityEditor.Android.AndroidPlatformIconKind.Round, result );
#endif
            return result;
        }
        
    }

}
#endif
