﻿#if UNITY_IOS
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Callbacks;
using UnityEditor;
using UnityEditor.iOS.Xcode;
using System.IO;
using Lang = Languages.Language;

namespace PSVEditor
{
    public class IOSPostProcessBuild
    {
        private const string CFBundleLocalizations = "CFBundleLocalizations";
        private const string TARGET_NAME = "Unity-iPhone";

        [PostProcessBuild]
        public static void OnPostProcessBuild(BuildTarget buildTarget, string path)
        {
            if (buildTarget != BuildTarget.iOS) return;

            Debug.Log("[PSV] PostProcessBuild start");

            var languagesCode = new Dictionary<Lang, string>(){
                { Lang.Arabic,        "ar" },
                { Lang.Chinese,       "zh" },
                { Lang.Czech,         "cs" },
                { Lang.Danish,        "da" },
                { Lang.Dutch,         "nl" },
                { Lang.English,       "en" },
                { Lang.Finnish,       "fi" },
                { Lang.French,        "fr" },
                { Lang.German,        "de" },
                { Lang.Greek,         "el" },
                { Lang.Italian,       "it" },
                { Lang.Japanese,      "ja" },
                { Lang.Norwegian,     "no" },
                { Lang.Polish,        "pl" },
                { Lang.Portuguese,    "pt" },
                { Lang.Romanian,      "ro" },
                { Lang.Russian,       "ru" },
                { Lang.Spanish,       "es" },
                { Lang.Swedish,       "sv" },
                { Lang.Turkish,       "tr" },
                { Lang.Hindi,         "hi" },
                { Lang.Hebrew,        "he" },
                { Lang.Indonesian,    "id" },
                { Lang.Korean,        "ko" },
                { Lang.Thai,          "th" },
                { Lang.Ukrainian,     "uk" },
                { Lang.Catalan,       "ca" },
                { Lang.Belarusian,    "be" },
                { Lang.Vietnamese,    "vi" },
            };

            var plsitPath = path + "/Info.plist";
            var plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plsitPath));
            var plistroot = plist.root;

            PlistElementArray local = plistroot.CreateArray(CFBundleLocalizations);
            foreach (var lang in Languages.available_languages)
            {
                string langCode;
                if (languagesCode.TryGetValue(lang, out langCode))
                {
                    local.AddString(langCode);
                }
                else
                {
                    Debug.LogError(lang + " code does't exist");
                }
            }

            File.WriteAllText(plsitPath, plist.WriteToString());

            //AddDynamicLink(path);
            
            Debug.Log("[PSV] PostProcessBuild complete");
        }

#if false
        public static void AddDynamicLink(string path)
        {
            try
            {
                var projPath = Path.Combine(path, "Unity-iPhone.xcodeproj/project.pbxproj");

                var identifier = Application.identifier;
                var productName = identifier.Substring(identifier.LastIndexOf(".") + 1);
                var entitlementsFile = productName + ".entitlements";

                var entitlements = new ProjectCapabilityManager(projPath, entitlementsFile, TARGET_NAME);
                var dynamicLink = "applinks:psvios" + PSV.IdentifierIOS.data.id + ".page.link";

                entitlements.AddAssociatedDomains(new string[] { dynamicLink });
                entitlements.WriteToFile();
                Debug.Log("[PSV] Added dynamic link: " + dynamicLink);
            }
            catch
            {
                Debug.LogError("Dynamic link creation fail");
            }
        }
#endif
    }
}
#endif