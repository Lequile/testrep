@echo off
setlocal
set /p directory=Path to keystore directory: 
for /f %%i in ('dir %directory%\*.keystore /b') do set keystorepath=%%i
set keystorepath=%directory%\%keystorepath%
for /f %%i in ('dir %directory%\*.txt /b') do set passwordfile=%%i
echo --- Password file content: 
more %directory%\%passwordfile%
echo ---
set /p aliasname=Alias name: 
set outputpath=%directory%\PrivatePepkKey.zip
set globalkey=eb10fe8f7c7c9df715022017b00c6471f8ba8170b13049a11e6c09ffe3056a104a3bbe4ac5a955f4ba4fe93fc8cef27558a3eb9d2a529a2092761fb833b656cd48b9de6a

java -jar pepk.jar --keystore=%keystorepath% --alias=%aliasname% --output=%outputpath% --include-cert --encryptionkey=%globalkey%
endlocal
pause
