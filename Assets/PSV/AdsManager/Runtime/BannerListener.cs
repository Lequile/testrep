﻿#if !UNITY_IOS && !NO_PSV_ADS // Deleted for IOS
using System;
using UnityEngine;
using CAS;

namespace PSV.ADS.Core
{
    public abstract class BannerListener : MonoBehaviour
    {
        [ContextMenu( "UpdateBanner" )]
        protected void UpdateBannerPanel()
        {
            if (AdsManager.GetBannerVisible())
                BannerUpdated( AdsManager.GetBannerSizeInPX(), GetAnchor( AdsManager.GetAdPos() ) );
            else
                BannerUpdated( Vector2.zero, GetAnchor( AdPosition.BottomCenter ) );
        }

        virtual protected void BannerUpdated( Vector2 sz, Vector2 anchor )
        {

        }

        virtual protected void OnEnable()
        {
            UpdateBannerPanel();
            AdsManager.OnAdsEnabled += OnAdsEnabled;
            AdsManager.OnBannerHidden += UpdateBannerPanel;
            AdsManager.OnBannerShown += UpdateBannerPanel;

        }
        virtual protected void OnDisable()
        {
            AdsManager.OnAdsEnabled -= OnAdsEnabled;
            AdsManager.OnBannerHidden -= UpdateBannerPanel;
            AdsManager.OnBannerShown -= UpdateBannerPanel;
        }

        private void OnAdsEnabled( bool enabled )
        {
            UpdateBannerPanel();
        }

        private Vector2 GetAnchor( AdPosition ad_position )
        {
            Vector2 res = Vector2.zero;
            switch (ad_position)
            {
                case AdPosition.BottomCenter:
                    res.y = 0;
                    res.x = 0.5f;
                    break;
                case AdPosition.TopCenter:
                    res.y = 1;
                    res.x = 0.5f;
                    break;
                case AdPosition.BottomLeft:
                    res.y = 0;
                    res.x = 0;
                    break;
                case AdPosition.BottomRight:
                    res.y = 0;
                    res.x = 1;
                    break;
                case AdPosition.TopLeft:
                    res.y = 1;
                    res.x = 0;
                    break;
                case AdPosition.TopRight:
                    res.y = 1;
                    res.x = 1;
                    break;
            }
            return res;
        }
    }
}
#endif