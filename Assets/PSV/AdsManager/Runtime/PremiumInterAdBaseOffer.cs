﻿using System.Collections;
using System.Collections.Generic;
using PSV.ADS.Core;
using PSV.SceneManagement;
using UnityEngine;

namespace PSV.ADS
{
    public abstract class PremiumInterAdBaseOffer : ISceneOffer
    {
        /// <summary>
        /// Priority = -290
        /// </summary>
        public int priority { get { return -290; } }

        /// <summary>
        /// List scene names after which there will be show  premium interstitial. 
        /// </summary>
        public abstract HashSet<string> ScenesList();

        ICollection<string> ISceneOffer.ScenesList()
        {
            return ScenesList();
        }

        public OfferReply TryShow( string current_scene, string target_scene )
        {
#if !NO_PSV_ADS
            if (CanBeShown( current_scene ))
            {
                if (AdsManager.promo == null)
                {
                    AdsManager.Log( "Show Promo Interstitial failed. Promo Ads Manager bridge not initialized" );
                }
                else if (AdsInterop.isFullscreenAdVisible)
                {
                    AdsManager.LogWarning( "Show Promo Interstitial failed. Fullscreen Ad already visible" );
                }
                else if (AdsManager.promo.IsReadyAd( CAS.AdType.Interstitial ))
                {
                    AdsInterop.isFullscreenAdVisible = true;
                    if (AdsManager.interstitialShowEvent != null)
                        AdsManager.interstitialShowEvent.Invoke();

                    AdsManager.Log( "Call Show Promo Interstitial" );
                    AdsManager.promo.ShowAd( CAS.AdType.Interstitial );
                    OnShow( current_scene );
                    return OfferReply.ShowWithAds;
                }
            }
#endif
            return OfferReply.Skipped;
        }

        /// <summary>
        /// Check condition for show.
        /// Base logic: check not contains current scene in <see cref="ScenesList()"/>
        /// </summary>
        public virtual bool CanBeShown( string current_scene )
        {
            if (current_scene == SceneLoaderBase.zero_scene)
                return false;
            if (AdsInterop.AllowAdditionalAd)
                return false;
            var sceneList = ScenesList();
            return sceneList != null && sceneList.Contains( current_scene );
        }

        /// <summary>
        /// Caller on show already interstitial ad.
        /// Base logic is empty.
        /// </summary>
        protected virtual void OnShow( string current_scene )
        {

        }

        string ISceneOffer.OfferScene()
        {
            return null;
        }

        bool ISceneOffer.IsEmpty()
        {
#if !NO_PSV_ADS
            return ScenesList() == null || ScenesList().Count == 0;
#else
            return true;
#endif
        }

        bool ISceneOffer.IsValid()
        {
            return true;
        }
    }
}