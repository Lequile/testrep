﻿using UnityEngine;
using UnityEngine.SceneManagement;
using CAS;
using System;

namespace PSV.ADS.Core
{
    public abstract class BaseBannerLifecycle : IFadeCallbackReceiver, IOrderedView
    {
        protected int allow_next_show = 0;
        protected AdPosition next_show_pos = AdPosition.Undefined;

        private bool visible_now;
        private bool should_visible;

        protected BaseBannerLifecycle()
        {
            if (AdsManager.small_banner_lifecycle != null)
                ServiceFade.RemoveReceiver( AdsManager.small_banner_lifecycle );
            AdsManager.small_banner_lifecycle = this;
            ServiceFade.AddReceiver( this );
            AdsManager.Log( GetType().Name + " with system linked" );


            NextFrameCall.Add( RefreshForCurrentScene );
        }

        /// <summary>
        /// Call before Loading scene or simulating transition. Will override settings only for current transition. Call without arguments to reset overrides.
        /// </summary>
        public void OverrideSettings( bool? visible = null, AdPosition pos = AdPosition.Undefined )
        {
            allow_next_show = visible == null ? 0 : visible.Value ? 1 : -1;
            next_show_pos = pos;
            if (MobileAds.settings.isDebugMode)
            {
                AdsManager.Log( GetType().Name + " Set Behaviour overrides to Visible "
                    + ( visible == null ? "undefined" : visible.ToString() )
                    + " pos: " + pos.ToString() );
            }
        }

        [Obsolete( "Override Settings with change Size no longer supported. Yout stil can use override visible and position.", true )]
        public void OverrideSettings( bool? visible, AdPosition pos, AdSize size ) { }

        private void ResetOverride()
        {
            allow_next_show = 0;
            next_show_pos = AdPosition.Undefined;
            AdsManager.Log( GetType().Name + " Reset behaviour overrides" );
        }

        public void OnScreenBecameFullyVisible()
        {
            RefreshForCurrentScene();
        }

        public void OnScreenBeganToGetDark()
        {
            if (CAS.AdObject.BannerAdObject.Instance)
            {
                AdsManager.Log( "Hide Banner OnScreenBeganToGetDark not support with BannerAdObject on scene." );
                return;
            }
            Hide();
        }

        public void RefreshForCurrentScene()
        {
            if (CAS.AdObject.BannerAdObject.Instance)
            {
                AdsManager.Log( "Refersh banner for current scene not support with BannerAdObject on scene." );
                return;
            }
            if (!IsInternalEnabled())
            {
                //AdsManager.Log( GetType().Name + " Hidden because Internal manager are Disabled!" );
                // Log message from virtual implementeation
                return;
            }
            if (Utils.isEditor || Application.isMobilePlatform)
            {
                string scene_name = SceneManager.GetActiveScene().name;
                bool visible = IsVisible( scene_name );

                if (visible)
                {
                    AdPosition target_pos = GetPosition( scene_name );
                    AdsManager.Log( GetType().Name + " Visible on Scene " + scene_name + " with position " + target_pos );

                    ResetOverride();

                    AdsManager.SetBannerPosition( target_pos );
                    Show();
                }
                else
                {
                    ResetOverride();
                    Hide();
                }
            }
            else
            {
                AdsManager.Log( GetType().Name + " Hidden because current runtime platform not supported!" );
            }
        }

        #region Visible

        public void Show()
        {
            should_visible = true;
            if (IsInternalEnabled())
                this.AddView( GetVisibleOrder() );
            else
                AdsManager.Log( GetType().Name + " Hidden because internal manager is disabled!" );
        }

        public void Hide()
        {
            AdsManager.Log( GetType().Name + " Hidden by API" );
            should_visible = false;
            visible_now = false;
            this.RemoveView();
            InternalHide();
        }

        internal abstract void InternalShow();
        internal abstract void InternalHide();
        internal abstract bool IsInternalEnabled();

        /// <summary>
        /// Is visible banner right now
        /// </summary>
        /// <returns></returns>
        public bool IsVisible()
        {
            return visible_now;
        }

        internal bool ShouldVisible()
        {
            return should_visible;
        }

        /// <summary>
        /// Is Visible banner with override settings
        /// </summary>
        public bool IsVisible( string scene_name )
        {
            if (allow_next_show == 0)
            {
                if (scene_name != "InitScene" && IsVisibleOnScene( scene_name ))
                    return true;

                AdsManager.Log( GetType().Name + " Hidden because Scene " + scene_name + " in black list!" );
                return false;
            }
            if (allow_next_show > 0)
                return true;
            AdsManager.Log( GetType().Name + " Hidden because the behavior is overridden for the current one shown!" );
            return false;
        }

        /// <summary>
        /// Is Visible banner without override settings
        /// </summary>
        protected abstract bool IsVisibleOnScene( string scene_name );

        public virtual int GetVisibleOrder()
        {
            return int.MinValue;
        }

        void IOrderedView.OnVisibilityChanged( bool visible )
        {
            if (this.should_visible && visible != visible_now)
            {
                visible_now = visible;
                if (visible)
                {
                    InternalShow();
                }
                else
                {
                    AdsManager.Log( GetType().Name + " Hidden because found view with higher order!" );
                    InternalHide();
                }
            }
        }
        #endregion

        #region Position

        /// <summary>
        /// Get Position banner with override settings
        /// </summary>
        public AdPosition GetPosition( string scene_name = null )
        {
            if (next_show_pos == AdPosition.Undefined)
            {
                if (scene_name == null && AdsManager.current != null)
                    return AdsManager.current.bannerPosition;
                return GetPositionOnScene( scene_name );
            }
            return next_show_pos;
        }

        /// <summary>
        /// Get Position banner without override settings.
        /// Default <see cref="AdPosition.BottomCenter"/>
        /// </summary>
        protected virtual AdPosition GetPositionOnScene( string scene_name )
        {
            return AdPosition.BottomCenter;
        }
        #endregion

        public sealed override bool Equals( object obj )
        {
            if (obj == null)
                return false;
            if (obj is BaseBannerLifecycle)
            {
                return GetType() == obj.GetType();
            }
            return false;
        }

        public sealed override int GetHashCode()
        {
            return GetType().GetHashCode();
        }
    }
}