﻿using System;
using CAS;

namespace PSV
{
    [Obsolete("Renamed to AdsManager")]
    public sealed class ManagerGoogle : AdsManager
    {
        private ManagerGoogle() { }

        /// <summary>
        /// Force show ad by selected <see cref="AdType"/>
        /// Ignored delay between displayed ad.
        /// </summary>
        /// <param name="type"></param>
        [Obsolete( "Select network ad no longer supported. Please use Show method without network insted." )]
        public static bool Show( AdType type, string network )
        {
            return Show( type );
        }

        [Obsolete( "Renamed to IsAdsEnabled" )]
        public static bool IsAdmobEnabled()
        {
            return IsAdsEnabled();
        }

        [Obsolete( "Deprecated arg'1" )]
        public static void HideSmallBanner( bool not_used_arg )
        {
            HideSmallBanner();
        }

        [Obsolete( "Deprecated arg'1" )]
        public static void ShowSmallBanner( bool not_used_arg, AdPosition ad_pos = AdPosition.Undefined )
        {
            ShowSmallBanner( ad_pos );
        }

        /// <summary>
        /// Show Interstitial Ads banner.
        /// Return value indicates whether show the ads succeeded.
        /// </summary>
        [Obsolete( "Deprecated arg'1" )]
        public static bool ShowInterstitial( bool not_used_arg, bool ignore_delay )
        {
            return ShowInterstitial( ignore_delay );
        }

        [Obsolete( "Renamed to ShowInterstitial" )]
        public static void ShowFullscreenBanner( bool not_used_arg = false, bool ignore_delay = false )
        {
            ShowInterstitial();
        }

        /// <summary>
        /// Show rewarded video ad.
        /// Before calling this method, subscribe to events like OnRewardedComplete.
        /// After done or close rewarded, unsubscripted from events.
        /// </summary>
        /// <param name="not_used_arg">Deprecated</param>
        /// <param name="ignore_delay">Is ignore time delay after the last shown.</param>
        [Obsolete( "Deprecated arg'1" )]
        public static bool ShowRewardedVideoAd( bool not_used_arg, bool ignore_delay )
        {
            return ShowRewardedVideoAd( ignore_delay );
        }
    }
}
