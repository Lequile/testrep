﻿//#define USE_USER_CONSENT

using System;
using PSV.ADS;
using PSV.ADS.Core;
using UnityEngine;
using CAS;
using UnityEngine.Networking;
using System.Collections;
using System.IO;
using System.Text;
using PSV.AppStore;
using PSV.SceneManagement;
using System.Collections.Generic;
#if USE_USER_CONSENT
using CAS.UserConsent;
#endif

using UnityEngine.Scripting;

[assembly: AlwaysLinkAssembly]

namespace PSV
{
    public partial class AdsManager
    {
        #region Events implementation
        internal static IMediationManager current;
        internal static IMediationManager promo;

        internal static BaseBannerLifecycle small_banner_lifecycle = new SmallBannerAdLifecycle();

        private static SafeEvent<Action<bool>> adsEnabledEvent;

        private static SafeEvent<Action> bannerShownEvent;
        private static SafeEvent<Action> bannerHiddenEvent;

        internal static SafeEvent<Action> interstitialShowEvent;
        private static SafeEvent<Action> interstitialClosedEvent;

        private static SafeEvent<Action> rewardedShowEvent;
        private static SafeEvent<Action> rewardedClosedEvent;
        private static SafeEvent<Action> rewardedCompletedEvent;

        private static SafeEvent<Action> rewardedSuccessfulOnceEvent;
        private static SafeEvent<Action> rewardedFailOnceEvent;

        private static AdPosition bannerInitPosition = AdPosition.BottomCenter;
        #endregion

        protected AdsManager() { }

        [RuntimeInitializeOnLoadMethod( RuntimeInitializeLoadType.AfterSceneLoad )]
        private static void AwakeManager()
        {
            AwakeStatic.Done( typeof( AdsManager ) );

#if USE_USER_CONSENT
            try
            {
                var request = UserConsent.BuildRequest();
                var view = request.Present();
                if (view)
                {
                    var handler = new PauseSceneLoadingToConsent();
                    SceneLoaderBase.AddLoadable( handler );
                    request.WithCallback( handler.OnConsent );
                    return;
                }
            }
            catch (Exception e)
            {
                e.LogException( log_tag + " Consent request error" );
            }
#endif
            if (Application.isEditor)
                CreateManager();
            else
                DelayedCallHandler.DelayedCallScaled( CreateManager, 5 );
        }

        private static void CreateManager()
        {
            var casSettings = MobileAds.settings;
            casSettings.RestartInterstitialInterval(); // Wait first interval after start
            casSettings.isExecuteEventsOnUnityThread = true;
            casSettings.analyticsCollectionEnabled = true;
            casSettings.allowInterstitialAdsWhenVideoCostAreLower = true;
#if !USE_USER_CONSENT
            casSettings.userConsent = ConsentStatus.Denied;
#endif
            casSettings.userCCPAStatus = CCPAStatus.OptOutSale;

            var initSettings = MobileAds.BuildManager();
            var enabledAds = ( AdFlags )( int )AdsInterop.InitAndGetAd(
                ( AdTypeFlags )( int )initSettings.allowedAdFlags, AdTypeFlags.None );

            try
            {
                current = initSettings
                    .WithEnabledAdTypes( initSettings.allowedAdFlags & enabledAds )
                    .Initialize();
            }
            catch (NotSupportedException e)
            {
                Log( e.ToString() );
                return;
            }
            catch (Exception e)
            {
                LogError( e.ToString() );
                return;
            }

            AdsInterop.OnEnabledAdsChanged += CallEnabledAdsChanged;
            AdsInterop.OnMuteSoundsChanged += CallMuteSoundsChanged;

            current.OnBannerAdShown += CallBannerAdShown;
            current.OnBannerAdFailedToShow += CallBannerAdFailedToShow;
            current.OnBannerAdHidden += CallBannerAdHidden;

            //current.OnReturnAdClosed += CallInterstitialAdClosed;
            
            current.OnInterstitialAdFailedToShow += CallInterstitialAdFailedToShow;
            current.OnInterstitialAdClosed += CallInterstitialAdClosed;

            current.OnRewardedAdFailedToShow += CallRewardedAdFailedToShow;
            current.OnRewardedAdCompleted += CallRewardedAdCompleted;
            current.OnRewardedAdClosed += CallRewardedAdClosed;

            current.SetAppReturnAdsEnabled( true );

            if (!current.isTestAdMode && enabledAds != AdFlags.None)
                LastPageAd.Init();

            if (small_banner_lifecycle != null)
            {
                SetBannerPosition( bannerInitPosition );
                if (small_banner_lifecycle.IsVisible())
                    small_banner_lifecycle.InternalShow();
            }

            if (initSettings.managersCount > 1 && enabledAds == AdFlags.None)
            {
                promo = initSettings
                    .WithEnabledAdTypes( AdFlags.Interstitial )
                    .WithManagerIdAtIndex( 1 )
                    .ClearMediationExtras()
                    .Initialize();

                promo.OnInterstitialAdFailedToShow += CallInterstitialAdFailedToShow;
                promo.OnInterstitialAdClosed += CallInterstitialAdClosed;
            }
        }

        #region PSV AdsInterop callbacks
        private static void CallEnabledAdsChanged( AdTypeFlags flags )
        {
            Log( "Call Enabled Ads Changed to " + flags.ToString() );
            current.SetEnableAd( AdType.Banner,
                ( flags & AdTypeFlags.Small ) == AdTypeFlags.Small );
            current.SetEnableAd( AdType.Interstitial,
                ( flags & AdTypeFlags.Interstitial ) == AdTypeFlags.Interstitial );
            current.SetEnableAd( AdType.Rewarded,
                ( flags & AdTypeFlags.Rewarded ) == AdTypeFlags.Rewarded );

            if (adsEnabledEvent != null)
                adsEnabledEvent.Invoke( AdsInterop.AllowAdditionalAd );
        }

        private static void CallMuteSoundsChanged( bool mute )
        {
            Log( "Call Mute Sounds Changed to " + mute );
            MobileAds.settings.isMutedAdSounds = mute;
        }
        #endregion

        #region CAS Manager Callbacks
        private static void CallBannerAdShown()
        {
            Log( "Call Banner Ad Shown" );
            if (bannerShownEvent != null)
                bannerShownEvent.Invoke();
        }

        private static void CallBannerAdFailedToShow( string error )
        {
            Log( "Call Banner Ad Failed to Show with error: " + error );
            if (bannerHiddenEvent != null)
                bannerHiddenEvent.Invoke();
        }

        private static void CallBannerAdHidden()
        {
            Log( "Call Banner Ad Hidden" );
            if (bannerHiddenEvent != null)
                bannerHiddenEvent.Invoke();
        }

        private static void CallInterstitialAdClosed()
        {
            Log( "Call Interstitial Ad Closed" );
            if (rewardedSuccessfulOnceEvent != null)
            {
                rewardedSuccessfulOnceEvent.Invoke();
                rewardedSuccessfulOnceEvent.Clear();
            }
            rewardedFailOnceEvent = null;

            if (interstitialClosedEvent != null)
                interstitialClosedEvent.Invoke();
            AdsInterop.isFullscreenAdVisible = false; // After closed
        }

        private static void CallInterstitialAdFailedToShow( string error )
        {
            LogWarning( "Interstitial Ad Failed to Show: " + error );
            AdsInterop.isFullscreenAdVisible = false; // Before closed
            if (rewardedFailOnceEvent != null)
            {
                rewardedFailOnceEvent.Invoke();
                rewardedFailOnceEvent.Clear();
            }
            rewardedSuccessfulOnceEvent = null;
            if (interstitialClosedEvent != null)
                interstitialClosedEvent.Invoke();
        }

        private static void CallRewardedAdCompleted()
        {
            Log( "Call Rewarded Ad Completed" );
            if (rewardedSuccessfulOnceEvent != null)
            {
                rewardedSuccessfulOnceEvent.Invoke();
                rewardedSuccessfulOnceEvent.Clear();
            }
            rewardedFailOnceEvent = null;

            if (rewardedCompletedEvent != null)
                rewardedCompletedEvent.Invoke();
        }

        private static void CallRewardedAdFailedToShow( string error )
        {
            LogWarning( "Rewarded Ad Failed to Show: " + error );
            AdsInterop.isFullscreenAdVisible = false; // Before closed
            if (rewardedFailOnceEvent != null)
            {
                rewardedFailOnceEvent.Invoke();
                rewardedFailOnceEvent.Clear();
            }
            rewardedSuccessfulOnceEvent = null;
            if (rewardedClosedEvent != null)
                rewardedClosedEvent.Invoke();
        }

        private static void CallRewardedAdClosed()
        {
            Log( "Call Rewarded Ad Closed" );
            MobileAds.settings.RestartInterstitialInterval();

            if (rewardedFailOnceEvent != null)
            {
                rewardedFailOnceEvent.Invoke();
                rewardedFailOnceEvent.Clear();
            }
            rewardedSuccessfulOnceEvent = null;

            if (rewardedClosedEvent != null)
                rewardedClosedEvent.Invoke();
            AdsInterop.isFullscreenAdVisible = false; // After Closed
        }

        private static void CallRewardedApply()
        {
            CallRewardedAdCompleted();
            CallRewardedAdClosed();
        }
        #endregion

        #region Log
        internal static void Log( string message )
        {
            if (MobileAds.settings.isDebugMode)
            {
                Debug.Log( log_tag + message );
            }
        }

        internal static void LogWarning( string message )
        {
            Debug.LogWarning( log_tag + message );
        }

        internal static void LogError( string message )
        {
            Debug.LogError( log_tag + message );
        }

        private static string log_tag
        {
            get
            {
                if (Utils.isEditor)
                    return "<color=#07EE00><b>CAS ► </b></color>";
                return "[CAS Unity] ";
            }
        }
        #endregion

        #region User Consent
        private class PauseSceneLoadingToConsent : ILoadable
        {
            public string identifier { get { return "UserConsentDialog"; } }

            public bool isDone { get; set; }

            public float progress { get { return 0.2f; } }

            public void Dispose() { }

            public IEnumerator GetEnumerator()
            {
                while (!isDone)
                    yield return null;
            }

            public string GetProgressBytesString()
            {
                return "";
            }

            public void OnConsent()
            {
                isDone = true;
                try
                {
                    CreateManager();
                }
                catch (Exception e)
                {
                    e.LogException( "Init Ads Manager" );
                }
            }
        }

        #endregion
    }

    internal static class LastPageAd
    {
        private const string updateTargetTimeStampPref = "LastPageAdTaregetUpdaeTime";
        private const string cacheTargetInfoFile = "LastPageAdTargetInfo.json";

        public static void Init()
        {
            if (!Utils.HasTimePassed( updateTargetTimeStampPref, 2 )
                || Application.internetReachability == NetworkReachability.NotReachable)
            {
                if (TryInitFromCache())
                    return;
            }

            string destinationURL = Utils.GetTargetPlatform() == RuntimePlatform.Android
                ? "https://play.google.com/store/apps/dev?id=4700756200397995264"
                : "https://apps.apple.com/us/developer/oculist/id1260501789";
            MobileAds.manager.lastPageAdContent = new LastPageAdContent(
                headline: "Hippo Kids Games",
                adText: "Educational and entertaining games for Kids",
                destinationURL: destinationURL,
                imageURL: "https://lh3.googleusercontent.com/UDOsD29gL-tkjSjwO4y1JyysfhQDPnG8Mivjb06oD9FcuB7l5DuiFrOk-xf5reuCyg=w2247-h1264",
                iconURL: "https://lh3.googleusercontent.com/O8S1Dp7shft1quCUIj4tP9Kz5Ad2RzjUKWf8ob0C97kLm88eEzeVBCoKvqOu6jIaedE=w144-h144-n" );

            var currentApp = ApplicationInfo.GetCurrent();
            if (currentApp == null)
            {
                AdsManager.LogWarning( "Last Page Ad init failed: AppStore info of current Application not found" );
                return;
            }
            if (string.IsNullOrEmpty( currentApp.accountId ))
            {
                AdsManager.LogWarning( "Last Page Ad init failed: Acount ID for current Application not found" );
                return;
            }

            new AccountInfo( Utils.GetTargetPlatform(), OnMostImportantAppLoaded )
                .LoadMostImportantApp( currentApp.accountId );
        }

        private static bool TryInitFromCache()
        {
            var cachePath = Path.Combine( Application.temporaryCachePath, cacheTargetInfoFile );
            if (File.Exists( cachePath ))
            {
                try
                {
                    var json = File.ReadAllText( cachePath );
                    var content = JsonUtility.FromJson<LastPageAdContent>( json );
                    if (!string.IsNullOrEmpty( content.DestinationURL ))
                    {
                        MobileAds.manager.lastPageAdContent = content;
                        AdsManager.Log( "Last Page Ad init from cache" );
                        return true;
                    }
                }
                catch (Exception e)
                {
                    e.LogException( "Read Last Page Ad Content from Cache" );
                }
            }
            return false;
        }

        private static void OnMostImportantAppLoaded( string appId )
        {
            if (string.IsNullOrEmpty( appId ))
            {
                AdsManager.LogWarning( "Last Page Ad init failed: Target Application ID not found" );
                TryInitFromCache();
                return;
            }

            var platform = Utils.GetTargetPlatform();
            var appInfo = new ApplicationInfo();
            string url;
            if (platform == RuntimePlatform.Android)
            {
                url = ApplicationInfoLoader.GetAndroidAppInfoURL( appId, Application.systemLanguage );
                appInfo.bundleId = appId;
            }
            else
            {
                url = ApplicationInfoLoader.GetiOSAppInfoURLByiTunesId( appId, Application.systemLanguage );
                appInfo.trackId = appId;
            }

            var loader = new ApplicationInfoLoader( platform, OnApplicationInfoLoaded );
            loader.info = appInfo;
            loader.LoadAppInfo( url );
        }

        private static void OnApplicationInfoLoaded( ApplicationInfo info )
        {
            if (info == null)
            {
                AdsManager.LogWarning( "Last Page Ad init failed: Target Application Info not loaded" );
                TryInitFromCache();
                return;
            }

            string destinationURL;
            if (Utils.GetTargetPlatform() == RuntimePlatform.Android)
            {
                if (string.IsNullOrEmpty( info.bundleId ))
                {
                    AdsManager.LogWarning( "Last Page Ad init failed: Target Application bundle ID is empty" );
                    TryInitFromCache();
                    return;
                }
                destinationURL = "https://play.google.com/store/apps/details?id=" + info.bundleId
                    + "&referrer=utm_source%3DPSVTargetAd%26utm_medium%3Dpsv_lastpage%26utm_campaign%3D"
                    + Application.identifier;
            }
            else
            {
                if (string.IsNullOrEmpty( info.trackId ))
                {
                    AdsManager.LogWarning( "Last Page Ad init failed: Target Application iTunes ID is empty" );
                    TryInitFromCache();
                    return;
                }

                if (string.IsNullOrEmpty( info.bundleId ))
                {
                    destinationURL = ConstSettings.appleStoreUrl + info.trackId;
                }
                else
                {
                    var link = new StringBuilder( "https://psvios" )
                        .Append( info.trackId )
                        .Append( ".page.link/?link=" )
                        .Append( ConstSettings.appleStoreUrl )
                        .Append( info.trackId )
                        .Append( "&ibi=" )
                        .Append( info.bundleId )
                        .Append( "&isi=" )
                        .Append( info.trackId )
                        .Append( "&utm_source=PSVTargetAd&utm_medium=psv_lastpage&utm_campaign=" );
                    var selfInfo = ApplicationInfo.GetCurrent();
                    if (selfInfo != null)
                        link.Append( selfInfo.trackId );

                    link.Append( "&efr=1" );
                    destinationURL = link.ToString();
                }
            }

            if (info.description.Length > 100)
                info.description = info.description.Substring( 0, 95 ) + "...";

            var content = new LastPageAdContent(
                info.name, info.description, destinationURL, info.imageURL, info.iconURL );

            Utils.SetCurrentTimeToPref( updateTargetTimeStampPref );
            MobileAds.manager.lastPageAdContent = content;

            try
            {
                var cachePath = Path.Combine( Application.temporaryCachePath, cacheTargetInfoFile );
                File.WriteAllText( cachePath, JsonUtility.ToJson( content ) );
            }
            catch (Exception e)
            {
                e.LogException( "Save Last Page Ad Content to cache" );
            }
        }
    }
}
