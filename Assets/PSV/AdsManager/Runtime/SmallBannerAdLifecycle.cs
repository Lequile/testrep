﻿namespace PSV.ADS.Core
{
    public class SmallBannerAdLifecycle : BaseBannerLifecycle
    {
        /// <summary>
        /// Base return always True.
        /// </summary>
        protected override bool IsVisibleOnScene( string scene_name )
        {
            return true;
        }

        internal sealed override void InternalShow()
        {
            if(AdsManager.current == null)
                AdsManager.Log( "Show banner failed: Internal manager not initialzied" );
            else
                AdsManager.current.ShowAd( CAS.AdType.Banner );
        }

        internal sealed override void InternalHide()
        {
            if (AdsManager.current != null)
                AdsManager.current.HideBanner();
        }

        internal override bool IsInternalEnabled()
        {
            if(AdsManager.small_banner_lifecycle == null)
            {
                AdsManager.Log( "Show banner failed: Banner Lifecycle not found" );
                return false;
            }
            if (AdsManager.small_banner_lifecycle != this)
                return false;
            if (AdsManager.IsAdsEnabled( AdTypeFlags.Small ))
                return true;
            AdsManager.Log( "Show banner failed: Banner manager is disabled" );
            return false;
        }
    }
}