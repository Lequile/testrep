﻿#if !UNITY_IOS && !NO_PSV_ADS
using UnityEngine;
using System;

namespace PSV.ADS.Core
{
    [RequireComponent( typeof( BoxCollider2D ) )]
    [AddComponentMenu( "Physics 2D/Banner Collider 2D", 100 )]
    public class BannerPannelCollider : BannerListener
    {
        public float margin = 0.2f;
        private BoxCollider2D collider2d;
        private Camera mainCamera;

        private void Awake()
        {
            collider2d = GetComponent<BoxCollider2D>();
            mainCamera = GetScreen.FindCamera();
        }

        protected override void BannerUpdated( Vector2 sz, Vector2 anchor )
        {
            if (collider2d != null)
            {
                Vector2 coll_size = Vector2.zero;
                Vector2 pos = Vector2.zero;

                if (mainCamera)
                {
                    Vector2 top_right = mainCamera.ScreenToWorldPoint( sz );
                    Vector2 bottom_left = mainCamera.ScreenToWorldPoint( Vector2.zero );
                    coll_size = top_right - bottom_left;
                    pos = new Vector2( Screen.width * anchor.x, Screen.height * anchor.y );
                    pos = mainCamera.ScreenToWorldPoint( pos );
                }
                //saving unscaled collider size
                Vector2 offset = coll_size * 0.5f;
                //applying scale to collider's size
                coll_size.x += coll_size.y * margin;
                coll_size.y += coll_size.y * margin;
                collider2d.size = coll_size;
                //normalizing anchor from (0 .. 0.5 .. 1) to (-1 .. 0 .. 1)
                anchor *= -2;
                offset.x *= anchor.x + 1;
                offset.y *= anchor.y + 1;
                collider2d.offset = offset;
                transform.position = pos;
            }
        }
    }
}
#endif