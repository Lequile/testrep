﻿using PSV.ADS.Core;
using PSV.ADS;
using System;
using UnityEngine;
using CAS;

namespace PSV
{
    public partial class AdsManager
    {
        #region Events

        public static event Action<bool> OnAdsEnabled
        {
            add { adsEnabledEvent += value; }
            remove { adsEnabledEvent -= value; }
        }
        public static event Action<AdTypeFlags> OnEnabledAdsChanged
        {
            add { AdsInterop.OnEnabledAdsChanged += value; }
            remove { AdsInterop.OnEnabledAdsChanged -= value; }
        }

        public static event Action OnBannerShown
        {
            add { bannerShownEvent += value; }
            remove { bannerShownEvent -= value; }
        }
        public static event Action OnBannerHidden
        {
            add { bannerHiddenEvent += value; }
            remove { bannerHiddenEvent -= value; }
        }

        public static event Action OnInterstitialShown
        {
            add { interstitialShowEvent += value; }
            remove { interstitialShowEvent -= value; }
        }
        public static event Action OnInterstitialClosed
        {
            add { interstitialClosedEvent += value; }
            remove { interstitialClosedEvent -= value; }
        }

        public static event Action OnRewardedShown
        {
            add { rewardedShowEvent += value; }
            remove { rewardedShowEvent -= value; }
        }
        public static event Action OnRewardedClosed
        {
            add { rewardedClosedEvent += value; }
            remove { rewardedClosedEvent -= value; }
        }
        public static event Action OnRewardedComplete
        {
            add { rewardedCompletedEvent += value; }
            remove { rewardedCompletedEvent -= value; }
        }
        #endregion

        #region General Methods
        /// <summary>
        /// Check available any ad types.
        /// Recommended use <see cref="IsAdsEnabled(AdTypeFlags)"/> instead to check the selected type.
        /// OR <see cref="IsAdsProcessing(AdType)"/> to validate currently processing and caching of ad type.
        /// </summary>
        public static bool IsAdsEnabled()
        {
            return AdsInterop.AllowAdditionalAd;
        }

        /// <summary>
        /// Check available selected ad types.
        /// To find out currently enabled processing and caching of ad type use <see cref="IsAdsProcessing(AdType)"/>
        /// </summary>
        public static bool IsAdsEnabled( AdTypeFlags types )
        {
            return ( AdsInterop.enabledAds & types ) == types;
        }

        /// <summary>
        /// Check currently processing and caching of selected ad type.
        /// </summary>
        public static bool IsAdsProcessing( AdType type )
        {
            return current != null && current.IsEnabledAd( type );
        }

        /// <summary>
        /// Force show ad by selected type and network. 
        /// Ignored delay between displayed ad.
        /// On show fail: return false and not call AdClosed event.
        /// </summary>
        /// <param name="type"></param>
        public static bool Show( AdType type )
        {
            if (type == AdType.Interstitial || type == AdType.Rewarded)
            {
                if (AdsInterop.isFullscreenAdVisible)
                {
                    LogWarning( "Show " + type.ToString() + " failed: Fullscreen Ad already visible" );
                    return false;
                }
            }

            if (type == AdType.Rewarded && !IsAdsEnabled( AdTypeFlags.Rewarded ))
            {
                Log( "Rewarded successful because No Ads purchased." );
                NextFrameCall.Add( CallRewardedApply );
                return true;
            }

            if (current == null)
            {
                LogWarning( "Show " + type.ToString() + " failed: Ads Manager bridge not found" );
                return false;
            }

            if (current.IsReadyAd( type )) // Call super method to avoid double validation
            {
                if (type == AdType.Interstitial)
                {
                    AdsInterop.isFullscreenAdVisible = true;
                    if (interstitialShowEvent != null)
                        interstitialShowEvent.Invoke();
                }
                else if (type == AdType.Rewarded)
                {
                    AdsInterop.isFullscreenAdVisible = true;
                    if (rewardedShowEvent != null)
                        rewardedShowEvent.Invoke();
                }
                Log( "Call Show " + type.ToString() );
                current.ShowAd( type );
                return true;
            }
            Log( "Show " + type.ToString() + " failed. Ad not ready." );
            return false;
        }

        /// <summary>
        /// Check ready <see cref="AdType"/>
        /// </summary>
        public static bool IsAdReady( AdType type )
        {
            if (type == AdType.Rewarded && !IsAdsEnabled( AdTypeFlags.Rewarded ))
                return true;
            return current != null && current.IsReadyAd( type );
        }

        /// <summary>
        /// Get last active mediation ad name of selected type.
        /// Can return <see cref="string.Empty"/>.
        /// </summary>
        public static string GetInfoLastActiveAd( AdType type )
        {
            if (current == null)
                return string.Empty;
            return current.GetLastActiveMediation( type );
        }
        #endregion

        #region Banner Ad
        public static BaseBannerLifecycle smallBannerLifecycle
        {
            get { return small_banner_lifecycle; }
        }

        public static bool GetBannerVisible()
        {
            return small_banner_lifecycle != null && small_banner_lifecycle.IsVisible();
        }

        public static AdSize GetAdSize()
        {
            if (current == null)
                return AdSize.Banner;
            return current.bannerSize;
        }

        public static AdPosition GetAdPos()
        {
            if (current == null)
                return AdPosition.BottomCenter;
            return current.bannerPosition;
        }

        public static Vector2 GetBannerSizeInPX()
        {
            if (current == null)
                return Vector2.zero;
            return new Vector2(
                current.GetBannerWidthInPixels(),
                current.GetBannerHeightInPixels() );
        }

        /// <summary>
        /// The API of <see cref="HideSmallBanner"/> and <see cref="ShowSmallBanner(AdPosition)"/> are undesirable.
        /// 
        /// There are several more preferred API. Here is a list in order of priority.
        /// 
        /// 1. If you don't want to show the banner all scene long 
        /// then override <see cref="SmallBannerAdLifecycle.IsVisibleOnScene(string)"/>
        /// in Game/Scripts/CustomSettings/SmallBannerLifecycle.cs.
        /// 
        /// 2. When you need not to show a banner while it can be covered by some time window.
        /// Implement <see cref="IOrderedView"/> to window
        /// and call <see cref="OrderedViewsManager.AddView(IOrderedView, int)"/> to hide banner
        /// and call <see cref="OrderedViewsManager.RemoveView(IOrderedView)"/> to allow banner shows.
        /// 
        /// 3. Simple hide banner for current scene by <see cref="HideSmallBanner"/>.
        /// Then to restore the banner on the scene with the rules specified in <see cref="SmallBannerAdLifecycle"/>,
        /// you must use <see cref="RefreshSmallBannerForCurrentScene"/>.
        ///
        /// 4. Insecure and unconditional show of the banner for current scene using <see cref="ShowSmallBanner(AdPosition)"/>.
        /// </summary>
        public static void HideSmallBanner()
        {
            if (small_banner_lifecycle == null)
                Log( "Small Banner Lifecycle not found" );
            else
                small_banner_lifecycle.Hide();
        }

        /// <summary>
        /// The API of <see cref="HideSmallBanner"/> and <see cref="ShowSmallBanner(AdPosition)"/> are undesirable.
        /// 
        /// There are several more preferred API. Here is a list in order of priority.
        /// 
        /// 1. If you don't want to show the banner all scene long 
        /// then override <see cref="SmallBannerAdLifecycle.IsVisibleOnScene(string)"/>
        /// in Game/Scripts/CustomSettings/SmallBannerLifecycle.cs.
        /// 
        /// 2. When you need not to show a banner while it can be covered by some time window.
        /// Implement <see cref="IOrderedView"/> to window
        /// and call <see cref="OrderedViewsManager.AddView(IOrderedView, int)"/> to hide banner
        /// and call <see cref="OrderedViewsManager.RemoveView(IOrderedView)"/> to allow banner shows.
        /// 
        /// 3. Simple hide banner for current scene by <see cref="HideSmallBanner"/>.
        /// Then to restore the banner on the scene with the rules specified in <see cref="SmallBannerAdLifecycle"/>,
        /// you must use <see cref="RefreshSmallBannerForCurrentScene"/>.
        ///
        /// 4. Insecure and unconditional show of the banner for current scene using <see cref="ShowSmallBanner(AdPosition)"/>.
        /// </summary>
        public static void ShowSmallBanner( AdPosition ad_pos = AdPosition.Undefined )
        {
            if (small_banner_lifecycle == null)
            {
                Log( "Small Banner Lifecycle not found" );
                return;
            }
            SetBannerPosition( ad_pos );
            small_banner_lifecycle.Show();
        }

        /// <summary>
        /// Restore the banner on the scene with the rules specified in <see cref="SmallBannerAdLifecycle"/>.
        /// </summary>
        public static void RefreshSmallBannerForCurrentScene()
        {
            if (small_banner_lifecycle == null)
                Log( "Small Banner Lifecycle not found" );
            else
                small_banner_lifecycle.RefreshForCurrentScene();
        }

        /// <summary>
        /// Change banner position on screen.
        /// You also can set banner position for all scene long 
        /// with override <see cref="BaseBannerLifecycle.GetPositionOnScene(string)"/>
        /// in Game/Scripts/CustomSettings/SmallBannerLifecycle.cs.
        /// </summary>
        public static void SetBannerPosition( AdPosition position )
        {
            if (current == null)
            {
                bannerInitPosition = position;
                LogWarning( "SetBannerPosition " + position.ToString() + " failed: Ads Manager Bridge not intialized" );
            }
            else
                current.bannerPosition = position;
        }
        #endregion

        #region Interstitial Ad
        /// <summary>
        /// Full-screen banner cached and ready to show.
        /// </summary>
        public static bool IsInterstitialReady()
        {
            return IsAdReady( CAS.AdType.Interstitial );
        }

        /// <summary>
        /// Is displayed right now full screen banner interstitial.
        /// </summary>
        public static bool IsDisplayedInterstitial()
        {
            return AdsInterop.isFullscreenAdVisible;
        }

        /// <summary>
        /// Start delay between shown interstitial with the current time
        /// </summary>
        public static void ResetLastInterstitialTime()
        {
            Log( "Reset delay for next show Interstitial" );
            MobileAds.settings.RestartInterstitialInterval();
        }

        /// <summary>
        /// Show Interstitial Ads.
        /// On show filed: call <see cref="OnInterstitialClosed"/> event in any case.
        /// </summary>
        public static void ShowInterstitial()
        {
            if (AdsInterop.isFullscreenAdVisible)
                LogWarning( "Show error: Fullscreen Ad already visible" );
            else if (!Show( AdType.Interstitial ))
                NextFrameCall.Add( CallInterstitialAdClosed );
        }
        #endregion

        #region Rewareded Ad
        /// <summary>
        /// Rewarded video ad cached and ready to show.
        /// </summary>
        public static bool IsRewardedReady()
        {
            return IsAdReady( AdType.Rewarded );
        }

        /// <summary>
        /// Is displayed right now rewarded ad video.
        /// </summary>
        public static bool IsDisplayedRewardedVideo()
        {
            return AdsInterop.isFullscreenAdVisible;
        }

        /// <summary>
        /// Show rewarded video ad.
        /// Before calling this method, subscribe to events like <see cref="OnRewardedComplete"/>.
        /// After done or close rewarded please unsubscripted from events.
        /// On show failed: called <see cref="OnRewardedClosed"/> event in any case.
        /// </summary>
        public static bool ShowRewardedVideoAd()
        {
            if (AdsInterop.isFullscreenAdVisible)
            {
                LogWarning( "Show error: Fullscreen Ad already visible" );
                return false;
            }
            if (Show( AdType.Rewarded ))
                return true;
            NextFrameCall.Add( CallRewardedAdClosed );
            return false;
        }

        /// <summary>
        /// Show rewarded video ad. 
        /// One-time use actions without needed to unsubscripted.
        /// On show failed: called <paramref name="fail"/> events.
        /// </summary>
        /// <param name="successful">Call on complete for reward.</param>
        /// <param name="fail">Call on closed and refused the award.</param>
        public static void ShowRewardedAd( Action success, Action fail )
        {
            TryShowRewardedVideoAd( success, fail, true );
        }

        /// <summary>
        /// Show rewarded video ad. 
        /// One-time use actions without needed to unsubscripted.
        /// Return value indicates whether show the video succeeded.
        /// On show failed: return false and called <see cref="OnRewardedClosed"/>, <paramref name="fail"/> events in any case.
        /// </summary>
        /// <param name="successful">Call on complete for reward.</param>
        /// <param name="fail">Call on closed and refused the award.</param>
        /// <param name="successOnDisabled">Force call success callback if NO ADS purchased.</param>
        /// <param name="not_used">Deprecated. Rewarded not supported delay between shows</param>
        public static bool TryShowRewardedVideoAd( Action successful, Action fail, bool successOnDisabled = false, bool not_used = false )
        {
            if (IsDisplayedRewardedVideo())
            {
                LogError( "Rewarded video ad already displayed, subscribed to current shown." );
                rewardedSuccessfulOnceEvent += successful;
                rewardedFailOnceEvent += fail;
                return true;
            }
            if (successOnDisabled && !IsAdsEnabled( AdTypeFlags.Rewarded ))
            {
                Log( "Rewarded successful because No Ads purchased." );
                rewardedSuccessfulOnceEvent += successful;
                NextFrameCall.Add( CallRewardedApply );
                return true;
            }

            if (Show( AdType.Rewarded ))
            {
                rewardedSuccessfulOnceEvent += successful;
                rewardedFailOnceEvent += fail;
                return true;
            }
            rewardedFailOnceEvent += fail;
            NextFrameCall.Add( CallRewardedAdClosed );
            return false;
        }
        #endregion

        #region Deprecated
        /// <summary>
        /// Show Rewarded. If Rewarded Video Ad can't be shown then show Interstitial Ad.
        /// Method is ignored delay between shown Rewarded and Interstitial ad.
        /// </summary>
        /// <param name="successful">Successful callback. On Rewarded complete or Interstitial closed.</param>
        /// <param name="fail">Fail show callback. On Rewarded closed or Rewarded and Interstitial no fill.</param>
        /// <param name="successOnDisabled">Force call success callback if NO ADS purchased.</param>
        [Obsolete( "All rewarded impressions can use Interstitial Ad. So you can use default rewarded show methods instead." )]
        public static bool ShowRewardedElseInterAd( Action successful, Action fail, bool successOnDisabled = true )
        {
            return TryShowRewardedVideoAd( successful, fail, successOnDisabled );
        }

        [Obsolete( "Use new separated API SetBannerPosition instead!" )]
        public static void RefreshSmallBanner( AdPosition ad_pos, AdSize ad_size )
        {
            SetBannerPosition( ad_pos );
        }

        /// <summary>
        /// The time delay after the last interstitial was passed.
        /// </summary>
        [Obsolete( "Not supported for current version." )]
        public static bool IsInterDelayPassed() { return true; }

        /// <summary>
        /// Waiting time after the last show until the next interstitial show
        /// </summary>
        [Obsolete( "Not supported for current version." )]
        public static float GetInterDelayLeft() { return 0.0f; }

        /// <summary>
        /// Set delay between shown interstitial done. <see cref="IsInterDelayPassed"/> return true.
        /// </summary>
        [Obsolete( "Not supported for current version." )]
        public static void SetInterDelayDone() { }

        /// <summary>
        /// Show Interstitial Ads.
        /// On show filed: call AdClosed event in any case.
        /// </summary>
        [Obsolete( "Ignore delay between Interstitial Ads are not supported for current version. Please use ShowInterstitial without ignore_delay instead" )]
        public static bool ShowInterstitial( bool ignore_delay )
        {
            if (AdsInterop.isFullscreenAdVisible)
            {
                LogWarning( "Show error: Fullscreen Ad already visible" );
                return false;
            }
            if (Show( AdType.Interstitial ))
                return true;
            NextFrameCall.Add( CallInterstitialAdClosed );
            return false;
        }

        /// <summary>
        /// Show rewarded video ad.
        /// Before calling this method, subscribe to events like <see cref="OnRewardedComplete"/>.
        /// After done or close rewarded please unsubscripted from events.
        /// On show failed: called <see cref="OnRewardedClosed"/> event in any case.
        /// </summary>
        [Obsolete( "Ignore delay between Rewarded Ads are not supported for current version. Please use ShowRewarded without ignore_delay instead" )]
        public static bool ShowRewardedVideoAd( bool ignore_delay )
        {
            return ShowRewardedVideoAd();
        }
        #endregion
    }
}
