﻿using System;
using System.Collections.Generic;
using PSV.SceneManagement;

namespace PSV.ADS.Core
{
    public abstract class InterstitialAdsBaseOffer : ISceneOffer
    {
        /// <summary>
        /// Priority = -300
        /// </summary>
        public int priority { get { return -300; } }

        /// <summary>
        /// List scene names after which there will be no show interstitial. 
        /// Can be null or empty for skip this check logic.
        /// </summary>
        public abstract HashSet<string> ScenesList();

        ICollection<string> ISceneOffer.ScenesList()
        {
            return ScenesList();
        }

        public OfferReply TryShow( string current_scene, string target_scene )
        {
#if !NO_PSV_ADS
            if (CanBeShown( current_scene ) && AdsManager.Show( CAS.AdType.Interstitial ))
            {
                OnShow( current_scene );
                return OfferReply.ShowWithAds;
            }
#endif
            return OfferReply.Skipped;
        }

        /// <summary>
        /// Check condition for show.
        /// Base logic: check not contains current scene in <see cref="ScenesList()"/>
        /// </summary>
        public virtual bool CanBeShown( string current_scene )
        {
            if (current_scene == SceneLoaderBase.zero_scene)
                return false;
            var sceneList = ScenesList();
            return sceneList == null || !sceneList.Contains( current_scene );
        }

        /// <summary>
        /// Called before each Show interstitial.
        /// Return True for skip check delay after last show interstitial.
        /// Base logic always return false;
        /// </summary>
        [Obsolete( "Not supported for current version" )]
        public virtual bool IsForceShow( string current_scene )
        {
            return false;
        }

        /// <summary>
        /// Caller on show already interstitial ad.
        /// Base logic is empty.
        /// </summary>
        protected virtual void OnShow( string current_scene )
        {

        }

        string ISceneOffer.OfferScene()
        {
            return null;
        }

        bool ISceneOffer.IsEmpty()
        {
#if !NO_PSV_ADS
            return false;
#else
            return true;
#endif
        }

        bool ISceneOffer.IsValid()
        {
            return true;
        }
    }
}