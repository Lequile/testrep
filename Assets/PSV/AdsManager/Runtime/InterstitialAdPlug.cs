﻿#if false && UNITY_ANDROID
using System.Collections;
using System.Collections.Generic;
using PSV.ADS.Core;
using UnityEngine;
using UnityEngine.UI;

namespace PSV.ADS
{
    public class InterstitialAdPlug : MonoBehaviour
    {
        public Text message;
        public Text timer;
        public Button skipBtn;
        public PointerClickHandler info;
        private int timerSec = 30;

        [RuntimeInitializeOnLoadMethod]
        public static void Init()
        {
#if !UNITY_EDITOR
            AdsInterop.OnInterstitialShown += OnAdsShown;
            AdsInterop.OnRewardedShown += OnAdsShown;
#endif
        }

        private static void OnAdsShown()
        {
            GameObject val = Resources.Load<GameObject>( "InterstitialAdPlug" );
            if (!val)
            {
                AdsInterop.OnInterstitialShown -= OnAdsShown;
                AdsInterop.OnRewardedShown -= OnAdsShown;
                Debug.LogWarning( "InterstitialAdPlug: Prefab not found in resources" );
                return;
            }
            ServiceContainer.InstantiateUI( val, false );
        }

        // Use this for initialization
        private void Start()
        {
            AdsInterop.OnInterstitialClosed += AdsClosed;
            AdsInterop.OnRewardedClosed += AdsClosed;
            SetMessage();
            StartCoroutine( TimerAction() );
            info.click += OnClickInfo;
        }

        private void SetMessage()
        {
            switch (Languages.GetLanguage())
            {
                case Languages.Language.Russian:
                    message.text = "Идет загрузка. Пожалуйста, подождите.";
                    break;
                case Languages.Language.Spanish:
                    message.text = "Cargando en curso. Por favor espera.";
                    break;
                case Languages.Language.Polish:
                    message.text = "Trwa ładowanie. Proszę czekać.";
                    break;
                case Languages.Language.French:
                    message.text = "Chargement en cours. S'il vous plaît, attendez.";
                    break;
                case Languages.Language.German:
                    message.text = "Wird gerade geladen. Warten Sie mal.";
                    break;
                case Languages.Language.Portuguese:
                    message.text = "Carregamento em andamento. Por favor, espere.";
                    break;
                case Languages.Language.Italian:
                    message.text = "Caricamento in corso. Attendere prego.";
                    break;
                default:
                    message.text = "Loading in progress. Please wait.";
                    break;
            }
        }

        private void OnClickInfo( UnityEngine.EventSystems.PointerEventData obj )
        {
            AdType type;
            if (AdsInterop.interstitial_shown)
                type = AdType.Interstitial;
            else if (AdsInterop.rewarded_shown)
                type = AdType.Rewarded;
            else
                type = AdType.None;

            message.text = "Waiting " + type.ToString() + " " + AdsManager.GetInfoLastActiveAd( type );
            this.Invoke( SetMessage, 3.0f, true );
        }

        private void OnApplicationPause( bool pause )
        {
            if (pause)
            {
                AdType type;
                if (AdsInterop.interstitial_shown)
                    type = AdType.Interstitial;
                else if (AdsInterop.rewarded_shown)
                    type = AdType.Rewarded;
                else
                    type = AdType.None;
                message.text = "In progress " + type.ToString() + " " + AdsManager.GetInfoLastActiveAd( type );
                info.gameObject.SetActive( false );
                StopAllCoroutines();
            }
            else
            {
                SetMessage();
                info.gameObject.SetActive( true );
                StartCoroutine( TimerAction() );
            }
        }

        private void OnDestroy()
        {
            AdsInterop.OnInterstitialClosed -= AdsClosed;
            AdsInterop.OnRewardedClosed -= AdsClosed;
        }

        private void AdsClosed()
        {
            Destroy( gameObject );
        }

        private IEnumerator TimerAction()
        {
            while (timerSec > 0)
            {
                yield return new WaitForSecondsRealtime( 1.0f );
                timerSec--;
                timer.text = timerSec.ToString();
            }
            message.gameObject.SetActive( false );
            skipBtn.gameObject.SetActive( true );
            timer.gameObject.SetActive( false );
            skipBtn.onClick.AddListener( OnSkipClick );
        }

        private void OnSkipClick()
        {
            AdType type;
            if (AdsInterop.interstitial_shown)
            {
                AdsInterop.InterstitialClosed();
                type = AdType.Interstitial;
            }
            else if (AdsInterop.rewarded_shown)
            {
                AdsInterop.RewardedClosed();
                type = AdType.Rewarded;
            }
            else
            {
                AdsClosed();
                return;
            }
            string adapter = AdsManager.GetInfoLastActiveAd( type );
            Debug.LogError( "CAS Skip impression by " + adapter );
            var log = new Dictionary<string, object>();
            log["ad"] = type.ToString();
            log["action"] = "NotClosed";
            log["adapter"] = adapter;
            AnalyticsManager.Log( "PSV_AdEvent", log );
        }
    }
}
#endif