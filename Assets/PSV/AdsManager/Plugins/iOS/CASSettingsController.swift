//
//  SettingsScreenController.swift
//  TestApp
//
//  Copyright © 2020 Clever Ads Solutions. All rights reserved.
//

import CleverAdsSolutions
import Foundation
import UIKit

class CASSettingsController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet var bannerAdSwitch: UISwitch!
    @IBOutlet var interstitialAdSwitch: UISwitch!
    @IBOutlet var rewardedAdSwitch: UISwitch!
    @IBOutlet var debugModeSwitch: UISwitch!
    @IBOutlet var muteAdsSwitch: UISwitch!
    @IBOutlet var bannerRefreshSlider: UISlider!
    @IBOutlet var interIntervalSlider: UISlider!
    @IBOutlet var loadingModePicker: UIPickerView!
    @IBOutlet var trackLocationSwitch: UISwitch!
    @IBOutlet var allowInterForRewardedSwitch: UISwitch!
    @IBOutlet var callbacksInUIThreadSwitch: UISwitch!
    @IBOutlet var userConsent: UILabel!
    @IBOutlet var ccpaStatus: UILabel!
    @IBOutlet var audience: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        if let manager = CASTestSuitController.manager {
            bannerAdSwitch.isOn = manager.isEnabled(type: .banner)
            interstitialAdSwitch.isOn = manager.isEnabled(type: .interstitial)
            rewardedAdSwitch.isOn = manager.isEnabled(type: .rewarded)
        }
        debugModeSwitch.isOn = CAS.settings.isDebugMode()
        muteAdsSwitch.isOn = CAS.settings.isMutedAdSounds()
        bannerRefreshSlider.value = Float(CAS.settings.getBannerRefreshInterval())
        interIntervalSlider.value = Float(CAS.settings.getInterstitialInterval())

        loadingModePicker.dataSource = self
        loadingModePicker.delegate = self
        loadingModePicker.selectRow(CAS.settings.getLoadingMode().rawValue, inComponent: 0, animated: true)

        trackLocationSwitch.isOn = CAS.settings.isTrackLocationEnabled()
        allowInterForRewardedSwitch.isOn = CAS.settings.isInterstitialAdsWhenVideoCostAreLowerAllowed()
        callbacksInUIThreadSwitch.isOn = CAS.settings.isExecuteCallbacksInUIThread()

        switch CAS.settings.getUserConsent() {
        case .accepted:
            userConsent.text = "GDPR: Accepted"
        case .denied:
            userConsent.text = "GDPR: Denied"
        default:
            userConsent.text = "GDPR: Undefined"
        }

        switch CAS.settings.getCCPAStatus() {
        case .optInSale:
            ccpaStatus.text = "CCPA: In sale"
        case .optOutSale:
            ccpaStatus.text = "CCPA: Out sale"
        default:
            ccpaStatus.text = "CCPA: Undefined"
        }

        switch CAS.settings.getTaggedAudience() {
        case .children:
            audience.text = "Audience: Children"
        case .notChildren:
            audience.text = "Audience: Not children"
        default:
            audience.text = "Audience: Undefined"
        }
    }

    @IBAction func closeDebugger(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func onBannerAdEnabled(_ sender: UISwitch) {
        CASTestSuitController.manager?.setEnabled(sender.isOn, type: .banner)
    }

    @IBAction func onInterstitialAdEnabled(_ sender: UISwitch) {
        CASTestSuitController.manager?.setEnabled(sender.isOn, type: .interstitial)
    }

    @IBAction func onRewardedAdEnabled(_ sender: UISwitch) {
        CASTestSuitController.manager?.setEnabled(sender.isOn, type: .rewarded)
    }

    @IBAction func onDebugModeChanged(_ sender: UISwitch) {
        CAS.settings.setDebugMode(sender.isOn)
    }

    @IBAction func onMuteAdsChanged(_ sender: UISwitch) {
        CAS.settings.setMuteAdSounds(to: sender.isOn)
    }

    @IBAction func onBannerRefreshRateChanged(_ sender: UISlider) {
        CAS.settings.setBannerRefresh(interval: Int(sender.value))
    }

    @IBAction func onInterIntervalChanged(_ sender: UISlider) {
        CAS.settings.setInterstitial(interval: Int(sender.value))
    }

    @IBAction func onTrackLocationChanged(_ sender: UISwitch) {
        CAS.settings.setTrackLocation(enabled: sender.isOn)
    }

    @IBAction func onAllowInterForRewardedChanged(_ sender: UISwitch) {
        CAS.settings.setInterstitialAdsWhenVideoCostAreLower(allow: sender.isOn)
    }

    @IBAction func onCallbacksInUIThreadEnabled(_ sender: Any) {
        CAS.settings.setExecuteCallbacks(inUIThread: callbacksInUIThreadSwitch.isOn)
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return CASLoadingManagerMode.manual.rawValue + 1
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch CASLoadingManagerMode(rawValue: row) {
        case .fastestRequests:
            return "Fastest Requests"
        case .fastRequests:
            return "Fast Requests"
        case .optimal:
            return "Optimal"
        case .highePerformance:
            return "Highe Performance"
        case .highestPerformance:
            return "Highest Performance"
        case .manual:
            return "Manual"
        default:
            return nil
        }
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if let mode = CASLoadingManagerMode(rawValue: row) {
            CAS.settings.setLoading(mode: mode)
        }
    }

    @IBAction func openNewScreen(_ sender: UIButton) {
        let bundle = Bundle(for: CASTestSuitController.self)
        if bundle.path(forResource: "TestNewScreen", ofType: "storyboardc") != nil {
            let storyboard = UIStoryboard(name: "TestNewScreen", bundle: bundle)
            let secondVC = storyboard.instantiateViewController(withIdentifier: "NewScreen")
            secondVC.modalPresentationStyle = .fullScreen
            // secondVC.modalTransitionStyle = .crossDissolve
            present(secondVC, animated: true, completion: nil)
            // dismiss(animated: false, completion: nil)
        } else {
            sender.isHidden = true
        }
    }
}
