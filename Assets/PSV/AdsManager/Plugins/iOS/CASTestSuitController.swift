//
//  MainController.swift
//  TestApp
//
//  Copyright © 2020 Clever Ads Solutions. All rights reserved.
//

import CleverAdsSolutions
import Foundation
import UIKit

class CASTestSuitController: UIViewController {
    weak static var manager: CASMediationManager?
    weak static var shared: CASTestSuitController?

    @IBOutlet var bannerView: CASBannerView!

    override func loadView() {
        super.loadView()
        CASTestSuitController.manager = CAS.manager!
        CASTestSuitController.shared = self
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        bannerView.rootViewController = self
        // bannerView.adPostion = CASPosition.center
    }

    
    @objc(setTargetManager:)
    func setTargetManager(_ manager: CASMediationManager) {
        CASTestSuitController.manager = manager
    }
}
