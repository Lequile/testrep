//
//  MainTabControllerViewController.swift
//  TestApp
//
//  Copyright © 2020 Clever Ads Solutions. All rights reserved.
//

import CleverAdsSolutions
import UIKit

class CASTabBarController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()

        let bundle = Bundle(for: CASTabBarController.self)
        
        var views = viewControllers ?? []
        let storyboard = UIStoryboard(name: "CASMediation", bundle: bundle)
        if let view = createTab(storyboard, .rewarded) {
            views.insert(view, at: 0)
        }
        if let view = createTab(storyboard, .interstitial) {
            views.insert(view, at: 0)
        }
        if let view = createTab(storyboard, .banner) {
            views.insert(view, at: 0)
        }

        let settingsStoryboard = UIStoryboard(name: "CASSettings", bundle: bundle)
        let settingsController = settingsStoryboard.instantiateViewController(withIdentifier: "Settings")
        settingsController.title = "Settings"
        views.insert(settingsController, at: 0)

        viewControllers = views
    }

    func createTab(_ storyboard: UIStoryboard, _ type: CASType) -> CASMediationController? {
        if let tableController = storyboard.instantiateViewController(withIdentifier: "BaseTable") as? CASMediationController {
            tableController.initData(type: type)
            tableController.title = type.description
            return tableController
        }
        return nil
    }

    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         // Get the new view controller using segue.destination.
         // Pass the selected object to the new view controller.
     }
     */
}
