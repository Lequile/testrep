//
//  SecondViewController.swift
//  TestApp
//
//  Copyright © 2020 Clever Ads Solutions. All rights reserved.
//

import CleverAdsSolutions
import UIKit

class CASMediationController: UITableViewController {
    var adType: CASType = .none
    var statuses: [CASStatusHandler?] = []
    var infos: [CASMediationInfo] = []
    var lastAdInfo: UILabel?
    var isCompleted = false
    var listener = self

    func initData(type: CASType) {
        adType = type
        if let manager = CASTestSuitController.manager {
            statuses = manager.getMediationStatus(type: adType)
            infos = manager.getMediationOrder(type: adType)
        } else {
            print("CAS > ERROR > manager still empty on Init data")
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let manager = CASTestSuitController.manager {
            manager.adStatusDelegate = self
            statuses = manager.getMediationStatus(type: adType)
            infos = manager.getMediationOrder(type: adType)
            tableView.reloadData()
        } else {
            print("CAS > ERROR > manager still empty on viewDidAppear")
        }
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let stack = UIStackView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        stack.axis = .horizontal
        stack.alignment = .fill
        stack.distribution = .fill
        stack.spacing = 5

        let showBtn = UIButton()
        showBtn.setTitle("Show", for: .normal)
        showBtn.backgroundColor = UIColor.systemGreen
        showBtn.addTarget(self, action: #selector(onShowPressed), for: .touchUpInside)
        showBtn.heightAnchor.constraint(equalToConstant: 45).isActive = true
        showBtn.widthAnchor.constraint(equalToConstant: 70).isActive = true
        stack.addArrangedSubview(showBtn)

        if adType == .banner {
            let hideBtn = UIButton()
            hideBtn.setTitle("Hide", for: .normal)
            hideBtn.backgroundColor = UIColor.systemGreen
            hideBtn.addTarget(self, action: #selector(onHidePressed), for: .touchUpInside)
            hideBtn.heightAnchor.constraint(equalToConstant: 45).isActive = true
            hideBtn.widthAnchor.constraint(equalToConstant: 70).isActive = true
            stack.addArrangedSubview(hideBtn)
        }

        let loadBtn = UIButton()
        loadBtn.setTitle("Load", for: .normal)
        loadBtn.backgroundColor = UIColor.systemGreen
        loadBtn.addTarget(self, action: #selector(onLoadPressed), for: .touchUpInside)
        loadBtn.heightAnchor.constraint(equalToConstant: 45).isActive = true
        loadBtn.widthAnchor.constraint(equalToConstant: 70).isActive = true
        stack.addArrangedSubview(loadBtn)

        let lastAdInfo = UILabel()
        lastAdInfo.textAlignment = .center
        self.lastAdInfo = lastAdInfo
        updateLastInfo(CASTestSuitController.manager?.getLastActiveMediation(type: adType) ?? "")
        stack.addArrangedSubview(lastAdInfo)

        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return infos.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        // print("CAS > TEST > create row \(index)")
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "MediationItem", for: indexPath)
        if let cell = cell as? CASMediationCell {
            cell.initData(statuses[index], infos[index])
        }
        return cell
    }

//    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        print("CAS > TEST > willDisplay row \(indexPath.row)")
//        super.tableView(tableView, willDisplay: cell, forRowAt: indexPath)
//    }

    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        statuses[indexPath.row]?.toggleIgnoreMode()
        return nil
    }

    @objc func onShowPressed(sender: UIButton!) {
        guard let manager = CASTestSuitController.manager else { return }
        switch adType {
        case .banner:
            CASTestSuitController.shared?.bannerView?.isHidden = false
        case .interstitial:
            if manager.isAdReady(type: .interstitial) == true {
                manager.presentInterstitial(fromRootViewController: self, callback: self)
            }
        case .rewarded:
            if manager.isAdReady(type: .rewarded) == true {
                manager.presentRewardedAd(fromRootViewController: self, callback: self)
            }
        default: break
        }
    }

    @objc func onHidePressed(sender: UIButton!) {
        if adType == .banner {
            CASTestSuitController.shared?.bannerView?.isHidden = true
        }
    }

    @objc func onLoadPressed(sender: UIButton!) {
        guard let manager = CASTestSuitController.manager else { return }
        switch adType {
        case .banner:
            CASTestSuitController.shared?.bannerView?.loadNextAd()
        case .interstitial:
            manager.loadInterstitial()
        case .rewarded:
            manager.loadRewardedAd()
        default: break
        }
    }

    func updateLastInfo(_ net: String) {
        guard let view = lastAdInfo else { return }
        if net.count > 0 {
            view.text = net
        } else {
            view.text = "no info"
        }
    }
}

extension CASMediationController: CASStatusDelegate {
    func onAdStatusChanged(_ status: CASStatusHandler) {
        guard status.adType == adType, let manager = CASTestSuitController.manager else { return }
        let newOrder = manager.getMediationOrder(type: adType)
        if newOrder.count == statuses.count {
            let statusIdentifier = status.identifier
            for (index, item) in newOrder.enumerated() {
                if item.identifier == statusIdentifier {
                    if infos[index].identifier == statusIdentifier {
                        statuses[index] = status
                        CASHandler.main {
                            if let visibleIndexes = self.tableView.indexPathsForVisibleRows {
                                for path in visibleIndexes {
                                    if path.row == index {
                                        self.tableView.reloadRows(at: [path], with: .automatic)
                                    }
                                }
                            }
                        }
                        return
                    }
                    break
                }
            }
        }
        infos = newOrder
        statuses = manager.getMediationStatus(type: adType)
        CASHandler.main {
            self.tableView.reloadData()
        }
    }
}

extension CASMediationController: CASCallback {
    func onShown(ad adStatus: CASStatusHandler) {
        print("CAS > \(adType.description) Menu Callback shown")
        DispatchQueue.main.async {
            self.updateLastInfo(adStatus.network)
        }
        isCompleted = false
    }

    func onShowFailed(message: String) {
        print("CAS > \(adType.description) Menu Callback show failed: \(message)")
    }

    func onClicked() {
        print("CAS > \(adType.description) Menu Callback clicked")
    }

    func onComplete() {
        isCompleted = true
    }

    func onClosed() {
        print("CAS > \(adType.description) Menu Callback closed")
        if isCompleted {
            CASHandler.main(1) {
                let alert = UIAlertController(title: "Video Rewarded", message: "You have been rewarded!", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}
