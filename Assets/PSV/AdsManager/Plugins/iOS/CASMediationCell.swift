//
//  MediationCellTableViewCell.swift
//  TestApp
//
//  Copyright © 2020 Clever Ads Solutions. All rights reserved.
//

import CleverAdsSolutions
import UIKit

class CASMediationCell: UITableViewCell {
    @IBOutlet var identifierLabel: UILabel!
    @IBOutlet var stateLabel: UILabel!
    @IBOutlet var settingsLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func initData(_ status: CASStatusHandler?, _ info: CASMediationInfo) {
        if info.lvl == 0 {
            identifierLabel.text = info.identifier
        } else {
            identifierLabel.text = info.identifier + " lvl\(info.lvl)"
        }

        updateInfo(status)
    }

    func updateInfo(_ status: CASStatusHandler?) {
        guard let status = status else {
            stateLabel.text = ": Pending"
            identifierLabel.textColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1.0)
            settingsLabel.text = ""
            return
        }

        if status.status == "Pending" || status.status == "InitFailed"
            || status.status == "Not supported" || status.status == "Awake" {
            identifierLabel.textColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1.0)
        } else if status.isAdCached() {
            identifierLabel.textColor = UIColor(red: 0.0, green: 0.5, blue: 0.0, alpha: 1.0)
        } else {
            identifierLabel.textColor = UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 1.0)
        }

        var result: String = ""
        if !status.status.isEmpty {
            result += ": " + status.status
        }
        result += "(" + status.versionInfo + ")"

        stateLabel.text = result

        let accuracy: String
        switch status.priceAccuracy {
        case .floor: accuracy = "Floor cpm"
        case .bid: accuracy = "Bid cpm"
        default: accuracy = "Undisclosed cpm"
        }
        let demand = status.identifier.starts(with: status.network) ? "" : status.network
        settingsLabel.text = demand + " " + accuracy +
            String(format: ": %.4f. ", status.cpm) +
            status.error.filter { !"\n\r".contains($0) }
    }
}
