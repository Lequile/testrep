﻿using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEngine;
using System.IO;
using CAS;
using CAS.UEditor;
using System;

namespace PSVEditor
{

    /* Reflection used
     */

    public class AdsMediationPattern
    {
        private const byte adNotUsed = 0;
        private const byte adOptional = 1;
        private const byte adRequired = 2;

        private const string androidTeenDep = "com.cleveradssolutions:cas:";
        private const string androidGeneralDep = "com.cleveradssolutions:cas-families:";
        private const string androidPromoDep = "com.cleveradssolutions:cas-promo:";

        private const string iosPrefixDep = "CASiOS";
        private const string sufixDep = "Dependencies";

        private const string iosMainDep = iosPrefixDep + "Main" + sufixDep;
        private const string iosRecomendedDep = iosPrefixDep + "Recomended" + sufixDep;
        private const string iosPromoDep = iosPrefixDep + CASEditorUtils.promoDependency + sufixDep;

#pragma warning disable 0649
        [Serializable]
        private class AdsSettings
        {
            public int[] Banner = new int[0];
            public int[] Interstitial = new int[0];
            public int[] Rewarded = new int[0];
            public Provider[] providers = new Provider[0];
        }

        [Serializable]
        private class Provider
        {
            public string net;
        }
#pragma warning restore 0649

        private class Info
        {
            public readonly string name;
            public readonly string network;
            public readonly AdNetwork net;
            public readonly string[] iOSFile;
            public readonly string[] gradle;
            public readonly byte banner;
            public readonly byte inter;
            public readonly byte rewarded;

            public Info( string name, string network, AdNetwork net, string[] iOSFile = null, string[] gradle = null,
                byte banner = adRequired, byte inter = adRequired, byte rewarded = adRequired )
            {
                this.name = name;
                this.network = network;
                this.net = net;
                this.iOSFile = iOSFile;
                this.gradle = gradle;
                this.banner = banner;
                this.inter = inter;
                this.rewarded = rewarded;
            }

            public bool ExistiOSAsset()
            {
                if (iOSFile != null)
                {
                    for (int i = 0; i < iOSFile.Length; i++)
                    {
                        if (AssetDatabase.FindAssets( iOSFile[i] ).Length > 0)
                            return true;
                    }
                }
                return false;
            }

            public bool ExistInGradle( string gradleFile )
            {
                if (gradle != null && gradleFile != null)
                {
                    for (int i = 0; i < gradle.Length; i++)
                    {
                        if (gradleFile.Contains( gradle[i] ))
                            return true;
                    }
                }
                return false;
            }

            public void ToString( StringBuilder result )
            {
                result.Append( name );
#if false
                if (banner == adNotUsed || inter == adNotUsed || rewarded == adNotUsed)
                {
                    result.Append( "(-" );
                    if (banner == adNotUsed)
                        result.Append( 'b' );
                    if (inter == adNotUsed)
                        result.Append( 'i' );
                    if (rewarded == adNotUsed)
                        result.Append( 'r' );
                    result.Append( ')' );
                }
#endif
            }
        }

        private readonly List<Info> mediation = new List<Info>()
        {
            new Info( "A", "AdMob", AdNetwork.GoogleAds,
                new[] { iosRecomendedDep, iosMainDep, iosPrefixDep + "GoogleAds" + sufixDep },
                new[] { androidTeenDep, androidGeneralDep, "com.google.android.gms:play-services-ads:" } ),
            new Info( "V", "Vungle", AdNetwork.Vungle,
                new[] { iosRecomendedDep, iosMainDep, iosPrefixDep + "Vungle" + sufixDep },
                new[] { androidTeenDep, androidGeneralDep, "com.vungle:publisher-sdk-android:" } ),
            new Info( "K", "Kidoz", AdNetwork.Kidoz,
                new[] { iosRecomendedDep, iosPrefixDep + "Kidoz" + sufixDep },
                new[] { androidTeenDep, androidGeneralDep, "com.kidoz.sdk:KidozSDK:" } ),
            new Info( "CB", "Chartboost", AdNetwork.Chartboost,
                new[] { iosPrefixDep + "Chartboost" + sufixDep },
                new[] { "com.cleveradssolutions:chartboost:" } ),
            new Info( "U", "Unity", AdNetwork.UnityAds,
                new[] { iosRecomendedDep, iosMainDep, iosPrefixDep + "UnityAds" + sufixDep },
                new[] { androidTeenDep, androidGeneralDep, "com.unity3d.ads:unity-ads:" } ),
            new Info( "AL", "AppLovin", AdNetwork.AppLovin,
                new[] { iosRecomendedDep, iosMainDep, iosPrefixDep + "AppLovin" + sufixDep },
                new[] { androidTeenDep, androidGeneralDep, "com.applovin:applovin-sdk:" } ),
            new Info( "SuA", "SuperAwesome", AdNetwork.SuperAwesome,
                new[] { iosRecomendedDep, iosPrefixDep + "SuperAwesome" + sufixDep },
                new[] { androidGeneralDep, "tv.superawesome.sdk.publisher:superawesome:" } ),
            new Info( "StA", "StartApp", AdNetwork.StartApp,
                new[] { iosRecomendedDep, iosPrefixDep + "StartApp" + sufixDep },
                new[] { "com.cleveradssolutions:startio:" } ),
            new Info( "AC", "AdColony", AdNetwork.AdColony,
                new[] { iosRecomendedDep, iosMainDep, iosPrefixDep + "AdColony" + sufixDep },
                new[] { androidTeenDep, androidGeneralDep, "com.adcolony:sdk:" } ),
            new Info( "FB", "Facebook", AdNetwork.FacebookAN,
                new[] { iosRecomendedDep, iosPrefixDep + "FacebookAN" + sufixDep },
                new[] { androidTeenDep, "com.facebook.android:audience-network-sdk:" },
                adOptional, adOptional, adOptional),
            new Info( "IM", "InMobi", AdNetwork.InMobi,
                new[] { iosRecomendedDep, iosMainDep, iosPrefixDep + "InMobi" + sufixDep },
                new[] { androidTeenDep, androidGeneralDep, "com.inmobi.monetization:inmobi-ads:" } ),
            new Info( "MT", "myTarget", AdNetwork.MyTarget,
                new[] { iosPrefixDep + "MyTarget" + sufixDep },
                new[] { "com.my.target:mytarget-sdk:" } ),
            new Info( "MF", "MobFox", AdNetwork.MobFox,
                new[] { iosPrefixDep + "MobFox" + sufixDep },
                new[] { "com.cleveradssolutions:mobfox:" } ),
            new Info( "PSV", "PSVTarget", AdNetwork.CrossPromotion,
                new[] { iosPromoDep },
                new[] { androidPromoDep } ),
            new Info( "IS", "IronSource", AdNetwork.IronSource,
                new[] { iosRecomendedDep, iosMainDep, iosPrefixDep + "IronSource" + sufixDep },
                new[] { androidTeenDep, androidGeneralDep, "com.ironsource.sdk:mediationsdk:" }, banner: adOptional ),
            new Info( "Ya", "Yandex", AdNetwork.YandexAds,
                new[] { iosPrefixDep + "YandexAds" + sufixDep },
                new[]{ "com.yandex.android:mobileads:" } , inter: adOptional, rewarded: adOptional ),
            new Info( "VAST", "VAST", AdNetwork.OwnVAST, null,
                new[] { "com.cleveradssolutions:cas-vast:" }, banner: adNotUsed ),
            new Info( "MP", "MoPub", AdNetwork.MoPub,
                new[] { iosPrefixDep + "MoPub" + sufixDep },
                new[] { "com.mopub:mopub-sdk-banner:", "com.mopub:mopub-sdk-fullscreen:" } ),
            new Info( "TJ", "Tapjoy", AdNetwork.Tapjoy,
                new[] { iosPrefixDep + "Tapjoy" + sufixDep },
                new[] { "com.cleveradssolutions:tapjoy:" }, banner: adNotUsed ),
            new Info( "FyB", "FairBid", AdNetwork.FairBid,
                new[] { iosRecomendedDep, iosPrefixDep + "FairBid" + sufixDep },
                new[] { androidTeenDep, "com.fyber:fairbid-sdk:" } ),
            new Info( "M", "Mintegral", AdNetwork.Mintegral,
                new[] { iosPrefixDep + "Mintegral" + sufixDep },
                new[] { "com.cleveradssolutions:mintegral:" } ),

            new Info( "Fy(Wrong)", "Fyber", AdNetwork.Fyber,
                new[] { iosPrefixDep + "Fyber" + sufixDep },
                new[] { "com.cleveradssolutions:fyber:" } ),
            new Info( "PSV(Wrong)", "PSVTargetWrong", Dependency.noNetwork, null,
                new[] { "com.cleversolutions.ads:cas-promo:" } ),
            new Info( "VAST(Wrong)", "VASTWrong", Dependency.noNetwork, null,
                new[] { "com.cleversolutions.ads:cas-vast:" }, banner: adNotUsed ),
            new Info( "CB(Wrong)", "ChartboostWrong", Dependency.noNetwork, null,
                new[] { "com.chartboost:chartboost-sdk:" } ),
            new Info( "StA(Wrong)", "StartAppWrong", Dependency.noNetwork, null,
                new[] { "com.startapp:inapp-sdk:" } ),
            new Info( "MF(Wrong)", "MobFoxWrong", Dependency.noNetwork, null,
                new[] { "com.github.mobfox:mfx-android-sdk:" } ),
            new Info( "VZ(Wrong)", "VerizonWrong", AdNetwork.Verizon,
                new[] { iosPrefixDep + "Verizon" + sufixDep },
                new[] { "com.cleveradssolutions:verizonmedia:", "com.verizon.ads:android-vas-standard-edition:" } ),
            new Info( "AZ(Wrong)", "AmazonWrong", AdNetwork.AmazonAds,
                new[] { iosPrefixDep + "AmazonAds" + sufixDep },
                new[] { "com.cleveradssolutions:amazonad:", "com.amazon.android:mobile-ads:" }, inter: adNotUsed, rewarded: adNotUsed ),
        };

        public static string GetReleaseInfo( BuildTarget platform )
        {
            if (platform != BuildTarget.Android && platform != BuildTarget.iOS)
                return "";

            var initSettings = CASEditorUtils.GetSettingsAsset( platform );

            if (initSettings.testAdMode)
                return "- DEMO Ads Only. Not for release!";

            string settingsJson;
            try
            {
                settingsJson = File.ReadAllText(
                    CASEditorUtils.GetNativeSettingsPath( platform, initSettings.GetManagerId( 0 ) ) );
            }
            catch (Exception e)
            {
                Debug.LogException( e );
                return "";
            }
            AdsSettings settings = JsonUtility.FromJson<AdsSettings>( settingsJson );

            DependencyManager dependencyManager = DependencyManager.Create( platform, Audience.Mixed, true );

            //string gradleFile = null;
            //if (File.Exists( CASEditorUtils.mainGradlePath ))
            //    gradleFile = File.ReadAllText( CASEditorUtils.mainGradlePath );
            var handler = new AdsMediationPattern();

            bool allowBannerAd = ( initSettings.allowedAdFlags & AdFlags.Banner ) == AdFlags.Banner;
            bool allowInterAd = ( initSettings.allowedAdFlags & AdFlags.Interstitial ) == AdFlags.Interstitial;
            bool allowRewardAd = ( initSettings.allowedAdFlags & AdFlags.Rewarded ) == AdFlags.Rewarded;

            var networkInfo = new StringBuilder();
            StringBuilder notFoundKeys = null;
            foreach (var item in handler.mediation)
            {
                if (item.net == Dependency.noNetwork)
                    continue;
                var dep = dependencyManager.Find( item.net );
                if (dep == null || !dep.IsInstalled())
                    continue;
                //if (platform == BuildTarget.Android && !item.ExistInGradle( gradleFile ))
                //    continue;
                //if (platform == BuildTarget.iOS && !item.ExistiOSAsset())
                //    continue;

                item.ToString( networkInfo );
                networkInfo.Append( "_" );

                bool bannerValid = item.banner < adRequired || !allowBannerAd;
                bool interValid = item.inter < adRequired || !allowInterAd;
                bool rewardedValid = item.rewarded < adRequired || !allowRewardAd;
                bool notFound = !bannerValid || !interValid || !rewardedValid;
                for (int i = 0; i < settings.providers.Length; i++)
                {
                    if (item.network.Equals( settings.providers[i].net, StringComparison.OrdinalIgnoreCase ))
                    {
                        if (!bannerValid)
                            bannerValid = Array.IndexOf( settings.Banner, i ) > -1;
                        if (!interValid)
                            interValid = Array.IndexOf( settings.Interstitial, i ) > -1;
                        if (!rewardedValid)
                            rewardedValid = Array.IndexOf( settings.Rewarded, i ) > -1;

                        if (bannerValid && interValid && rewardedValid)
                        {
                            notFound = false;
                            break;
                        }
                    }
                }
                if (notFound)
                {
                    if (notFoundKeys == null)
                        notFoundKeys = new StringBuilder( "- Need to add on server: " );

                    notFoundKeys.Append( item.network );
                    if (!bannerValid) notFoundKeys.Append( "[Banner]" );
                    if (!interValid) notFoundKeys.Append( "[Inter]" );
                    if (!rewardedValid) notFoundKeys.Append( "[Reward]" );
                    notFoundKeys.Append( ' ' );
                }
            }

            if (networkInfo.Length == 0)
                return "- Ads does not exist";

            networkInfo.Remove( networkInfo.Length - 1, 1 );

            if (notFoundKeys != null)
            {
                networkInfo.AppendLine();
                networkInfo.Append( notFoundKeys );

                //#if NET_4_6
                //                var root = Environment.GetFolderPath( Environment.SpecialFolder.UserProfile );
                //                var path = root + ".gradle/caches/modules-2/files-2.1/com.cleversolutions.ads/"

                //#endif
            }

            var result = new StringBuilder();
            result.Append( "- CAS: " );
            result.Append( MobileAds.wrapperVersion );

            var depManager = DependencyManager.Create( platform, initSettings.defaultAudienceTagged, false );
            var nativeVer = GetNativeCASVersion( depManager, platform );
            if (nativeVer != null && nativeVer != MobileAds.wrapperVersion)
                result.Append( '(' ).Append( nativeVer ).Append( ')' );

            if (!allowBannerAd)
                result.Append( " NO Banner," );
            else
            {
                switch (initSettings.bannerSize)
                {
                    case AdSize.Banner:
                        result.Append( " Banner," );
                        break;
                    case AdSize.AdaptiveBanner:
                        result.Append( " Adaptive," );
                        break;
                    case AdSize.SmartBanner:
                        result.Append( " Smart," );
                        break;
                    case AdSize.Leaderboard:
                        result.Append( " Leader," );
                        break;
                    case AdSize.MediumRectangle:
                        result.Append( " MREC," );
                        break;
                }
            }

            if (allowInterAd)
                result.Append( " Inter," );
            else
                result.Append( " NO Inter," );

            if (allowRewardAd)
                result.Append( " Reward" );
            else
                result.Append( " NO Reward" );

            result.AppendLine().Append( networkInfo );
            return result.ToString();
        }

        private static string GetNativeCASVersion( DependencyManager manager, BuildTarget platform )
        {
            for (int i = 0; i < manager.solutions.Length; i++)
            {
                var version = manager.solutions[i].installedVersion;
                if (version != null && version.Length > 0)
                    return version;
            }
            if (platform == BuildTarget.Android)
            {
                var version = manager.Find( "Base" ).installedVersion;
                if (version != null && version.Length > 0)
                    return version;
            }
            else
            {
                // Skip Base
                for (int i = 1; i < manager.networks.Length; i++)
                {
                    var version = manager.networks[i].installedVersion;
                    if (version != null && version.Length > 0)
                        return version;
                }
            }
            return null;
        }
    }
}