﻿#if UNITY_IOS
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Callbacks;
using UnityEditor;
using UnityEditor.iOS.Xcode;
using System.IO;
using System;

namespace PSVEditor
{
    public class IOSMoveStoryboards
    {

        [PostProcessBuild]
        public static void OnPostProcessBuild( BuildTarget buildTarget, string path )
        {
            if (buildTarget != BuildTarget.iOS) return;

            var project = new PBXProject();
            var projectPath = Path.Combine( path, "Unity-iPhone.xcodeproj/project.pbxproj" );
            project.ReadFromString( File.ReadAllText( projectPath ) );
#if UNITY_2019_3_OR_NEWER
            var target = project.GetUnityFrameworkTargetGuid();
#else
            var target = project.TargetGuidByName( PBXProject.GetUnityTargetName() );
#endif
            var resourcesBuildPhase = project.GetResourcesBuildPhaseByTarget( target );

            var folders = AssetDatabase.FindAssets( "Storyboards" );
            for (int i = 0; i < folders.Length; i++)
            {
                var pathInAssets = AssetDatabase.GUIDToAssetPath( folders[i] );
                var sourceDir = new DirectoryInfo( pathInAssets );
                if (!sourceDir.Exists)
                    continue;

                var pathInProject = Path.Combine( "Libraries", pathInAssets.Substring( 7 ) );
                var fullPath = Path.Combine( path, pathInProject );
                Directory.CreateDirectory( fullPath );

                FileInfo[] files = sourceDir.GetFiles();
                foreach (FileInfo file in files)
                {
                    try
                    {
                        if (file.Extension == ".storyboard")
                        {
                            string tempPath = Path.Combine( fullPath, file.Name );
                            string tempPathInProject = Path.Combine( pathInProject, file.Name );
                            file.CopyTo( tempPath, false );
                            var fileGuid = project.AddFile( tempPathInProject, tempPathInProject, PBXSourceTree.Source );

                            project.AddFileToBuildSection( target, resourcesBuildPhase, fileGuid );
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogWarning( "IOSMoveTestSuit: " + e.ToString() );
                    }
                }
            }

            File.WriteAllText( projectPath, project.WriteToString() );
        }
    }
}
#endif