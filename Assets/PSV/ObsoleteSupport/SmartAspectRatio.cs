﻿using UnityEngine;

namespace PSV.Linker
{
    [AddComponentMenu( "" )]
    [System.Obsolete( "Deprecated" )]
    public class SmartAspectRatio : PSV.SmartAspectRatio
    {

    }
}