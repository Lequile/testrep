﻿using UnityEngine;

namespace PSV.Audio
{
    [System.Obsolete( "This logic move to LocalizationManager." )]
    public static class LanguageAudio
    {
        public static AudioClip GetSoundByName( string name, bool multilanguage = true )
        {
            return LocalizationManager.GetSound( name );
        }
    }
}