﻿
namespace PSV.Linker
{
    [UnityEngine.AddComponentMenu("")]
    [System.Obsolete( "Deprecated" )]
    public class PausePanel : PSV.PausePanel
    {
        public LinkerWarning lm;
    }
}