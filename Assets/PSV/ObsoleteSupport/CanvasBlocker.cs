﻿namespace PSV.Linker
{
    [UnityEngine.AddComponentMenu( "" )]
    [System.Obsolete( "Deprecated" )]
    public class CanvasBlocker : PSV.CanvasBlocker
    {
        public LinkerWarning lm;
    }
}