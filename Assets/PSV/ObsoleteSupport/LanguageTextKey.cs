﻿namespace PSV.Linker
{
    [UnityEngine.AddComponentMenu( "" )]
    [System.Obsolete( "Deprecated" )]
    public class LanguageTextKey : Localization.LanguageTextKey
    {
        public LinkerWarning lm;
    }
}