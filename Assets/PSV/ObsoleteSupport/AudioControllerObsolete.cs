﻿using UnityEngine;
using System;

namespace PSV
{
    using Audio;

    public partial class AudioController : AudioControllerBase
    {
        /* 
         * Obsolete methods can be removed.
         */

        #region Base
        /// <summary>
        /// Will change volume override for streams of the group. It will immediately apply to all streams from that group.
        /// Redirect to <see cref="SetStreamsVolume(StreamGroup, float)"/>
        /// </summary>
        /// <param name="volume"></param>
        /// <param name="group">A group of streams to change volume</param>
        [Obsolete]
        public static void SetStreamsVolume( float volume, StreamGroup group )
        {
            SetStreamsVolume( group, volume );
        }
        
        /// <summary>
        /// Will unmute/mute sources, listed in music_like_streams list.
        /// Muting background sounds (music or ambient) without muting other sounds is not allowed.
        /// </summary>
        /// <param name="param">true - unmute, false - mute</param>
        [Obsolete( "Is deprecated. Use MuteStreams for specific group." )]
        public static void EnableMusic( bool param )
        {
            GetGroup( StreamGroup.MUSIC ).Mute( !param, true );
        }

        /// <summary>
        /// Returns volume for the group multiplied by volume_override.
        /// </summary>
        /// <param name="group">StreamGroup to select</param>
        /// <returns>Volume level</returns>
        [Obsolete( "Renamed to GetGroupVolume" )]
        public static float GetStreamVolume( StreamGroup group )
        {
            return GetGroupVolume( group );
        }

        /// <summary>
        /// Will change own and user sources volume (individual group values are counted. Set of groups will depend on is_music param (music_like_streams list defines whether group will be managed as music or as sound)
        /// </summary>
        /// <param name="volume">This will override main volume</param>
        /// <param name="is_music">tells what groups will be changed (See Common.music_like_streams)</param>
        [Obsolete( "You can use SetSoundVolume or SetMusicVolume methods instead." )]
        protected static void SetStreamsVolume( float volume, bool is_music )
        {
            if (is_music)
                SetMusicVolume( volume );
            else
                SetSoundsVolume( volume );
        }
        #endregion

        #region Methods to play sound
        
        /// <summary>
        /// Plays sound on custom AudioSource. Use PlaySound with custom AudioStream to be able to reuse it in future. Having stream itself will let you manage it manually.
        /// </summary>
        /// <param name="clip">AudioClip to play</param>
        /// <param name="custom_src">Custom AudioSource to play sound</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sound</param>
        /// <param name="pitch">Pitch value for sound</param>
        /// <param name="on_complete">Will be called when Stream will stop</param>
        /// <param name="locker">Set here a tween that will use this stream</param>
        /// <returns>Length of sound</returns>
        [Obsolete( "Better use Play() method with AudioHandler" )]
        public static float PlaySound( AudioClip clip, AudioSource custom_src, StreamGroup group, float volume_override = 1, bool loop = false, float pitch = 1, Action on_complete = null )
        {
            return Play( clip )
                        .Group( group )
                        .Volume( volume_override )
                        .Loop( loop )
                        .Pitch( pitch )
                        .OnComplete( on_complete )
                        .Start(custom_src);
        }
        
        /// <summary>
        /// Plays sound on custom AudioSource. Use PlaySound with custom AudioStream to be able to reuse it in future. Having stream itself will let you manage it manually.
        /// </summary>
        /// <param name="snd_name">AudioClip name</param>
        /// <param name="custom_src">Custom AudioSource to play sound</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sound</param>
        /// <param name="pitch">Pitch value for sound</param>
        /// <param name="on_complete">Will be called when Stream will stop</param>
        /// <param name="locker">Set here a tween that will use this stream</param>
        /// <returns>Length of sound</returns>
        [Obsolete( "Better use Play() method with AudioHandler" )]
        public static float PlaySound( string snd_name, AudioSource custom_src, StreamGroup group = StreamGroup.FX, float volume_override = 1, bool loop = false, float pitch = 1, Action on_complete = null )
        {
            return Play( snd_name )
                        .Group( group )
                        .Volume( volume_override )
                        .Loop( loop )
                        .Pitch( pitch )
                        .OnComplete( on_complete )
                        .Start(custom_src);
        }
        
        /// <summary>
        /// Plays a set of sounds placed in a queue. Used for dialogs containing separate phrases.
        /// </summary>
        /// <param name="clips">sounds to play in a row</param>
        /// <param name="custom_src">Custom AudioSource to play sound</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="intervals">Pause before next AudioClip will be played, should be of the same length as clips or null to make no pause</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sequence</param>
        /// <param name="pitch">Pitch value for sound</param>
        /// <param name="on_complete">Will be called when Stream will stop</param>
        /// <returns>Stack length</returns>
        [Obsolete( "Better use Play() method with AudioHandler" )]
        public static float PlaySound( AudioClip[] clips, AudioSource custom_src, StreamGroup group = StreamGroup.FX, float[] intervals = null, float volume_override = 1, bool loop = false, float pitch = 1, Action on_complete = null )
        {
            return Play( clips )
                        .Group( group )
                        .Volume( volume_override )
                        .Loop( loop )
                        .Intervals( intervals )
                        .Pitch( pitch )
                        .OnComplete( on_complete )
                        .Start(custom_src);
        }
        
        /// <summary>
        /// Plays a set of sounds placed in a queue. Used for dialogs containing separate phrases.
        /// </summary>
        /// <param name="snd_name">AudioClip names, value "||2.2" gives extra pause between phrases</param>
        /// <param name="custom_src">Custom AudioSource to play sound</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="interval">Pause before next AudioClip will be played</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sequence</param>
        /// <param name="pitch">Pitch value for sound</param>
        /// <param name="on_complete">Will be called when Stream will stop</param>
        /// <returns>Stack length</returns>
        [Obsolete( "Better use Play() method with AudioHandler" )]
        public static float PlaySound( string[] snd_name, AudioSource custom_src, StreamGroup group = StreamGroup.FX, float interval = 0, float volume_override = 1, bool loop = false, float pitch = 1, Action on_complete = null )
        {
            var handler = Play( snd_name )
                        .Group( group )
                        .Volume( volume_override )
                        .Loop( loop )
                        .Pitch( pitch )
                        .OnComplete( on_complete );
            if (interval > 0)
            {
                float[] intervals = new float[snd_name.Length];
                for (int i = 0; i < intervals.Length; i++)
                    intervals[i] = interval;

                handler.AddIntervals( intervals );
            }
            return handler.Start(custom_src);
        }
        #endregion
        
    }
    
}
