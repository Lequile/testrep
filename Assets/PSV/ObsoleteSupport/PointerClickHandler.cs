﻿using UnityEngine;

namespace PSV.Linker
{
    [AddComponentMenu( "" )]
    [System.Obsolete( "Deprecated" )]
    public class PointerClickHandler : PSV.PointerClickHandler
    {
        public LinkerWarning lm;
    }
}