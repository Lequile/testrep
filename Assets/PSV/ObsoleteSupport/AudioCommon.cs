﻿using System;

namespace PSV.Audio
{
    /// <summary>
    /// Contains general settings for StreamGroups. These values are set manually.
    /// </summary>
    [System.Obsolete( "Deprecated" )]
    public static class Common
    {
        /// <summary>
        /// List of audio stream groups that use GameSettings values for music
        /// </summary>
        [Obsolete( "Use AudioController.GetGroup().IsMusicLike() instead" )]
        public static bool IsMusicLikeGroup( StreamGroup group )
        {
            return AudioController.GetGroup( group ).IsMusicLike();
        }

        /// <summary>
        /// Tells if group is not enabled in GameSettings.
        /// </summary>
        /// <param name="group">StreamGroup to select</param>
        /// <returns>Mute value for selected group</returns>
        [Obsolete( "Better use AudioController.GetGroup(group).isMuted() instead" )]
        public static bool IsGroupSettingsMuted( StreamGroup group )
        {
            if (IsMusicLikeGroup( group ))
            {
                return AudioController.GetGroup( StreamGroup.MUSIC ).IsMuted();
            }
            else
            {
                return AudioController.GetGroup( StreamGroup.FX ).IsMuted();
            }
        }

        /// <summary>
        /// Get default stream group volume. (Is set manually)
        /// </summary>
        /// <param name="group">StreamGroup to select</param>
        [Obsolete( "Use AudioController.GetGroup().Volume instead" )]
        public static float GetGroupVolume( StreamGroup group )
        {
            return AudioController.GetGroup( group ).Volume;
        }

        /// <summary>
        /// Get default solo mode value for group. (Is set manually)
        /// Will keep only one stream playing for the group.
        /// </summary>
        /// /// <param name="group">StreamGroup to select</param>
        [Obsolete( "Use AudioController.GetGroup().Solo instead" )]
        public static bool IsSoloGroup( StreamGroup group )
        {
            return AudioController.GetGroup( group ).Solo;
        }
    }

}
