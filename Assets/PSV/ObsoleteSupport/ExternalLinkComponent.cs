﻿using UnityEngine;
using UnityEngine.Events;

namespace PSV
{
    [AddComponentMenu( "Event/External Link Component" )]
    [System.Obsolete("Better use Ordered Game Object instead.")]
    public class ExternalLinkComponent : MonoBehaviour, IExternalLink
    {

        public bool is_static = false;

        public UnityEvent
            OnExternalLinkHide,
            OnExternalLinkShow;


        private void OnEnable()
        {
            CreateLink();
        }


        private void OnDisable()
        {
            DestroyLink();
        }

        #region ExternalLink implementation

        public bool IsStatic
        {
            get
            {
                return is_static;
            }
        }


        public void CreateLink()
        {
            ExternalLinksManager.AddLink( this );
        }

        public void DestroyLink()
        {
            ExternalLinksManager.DeleteLink( this );
        }

        public void Show( bool param )
        {
            if (param)
            {
                if (OnExternalLinkShow != null)
                {
                    OnExternalLinkShow.Invoke();
                }
            }
            else
            {
                if (OnExternalLinkHide != null)
                {
                    OnExternalLinkHide.Invoke();
                }
            }
        }
    }
    #endregion

}
