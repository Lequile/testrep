﻿using UnityEngine;

namespace PSV.Linker
{
    [AddComponentMenu( "" )]
    [System.Obsolete( "Deprecated" )]
    public class QuitMenu : PSV.QuitMenu
    {
        public LinkerWarning lm;
    }
}