﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace PSV.Localization
{
    [RequireComponent( typeof( Text ) )]
    [AddComponentMenu( "UI/PSV/Localized Text Enum Key" )]
    [System.Obsolete( "Deprecated. Use Localized Text" )]
    public class LanguageText : MonoBehaviour
    {
        public enum AliasNames
        {
            play_btn,
        }

        public AliasNames alias;
        
        private void OnEnable()
        {
            Refresh();
            LocalizationManager.OnLanguageLoaded += Refresh;
        }

        private void OnDisable()
        {
            LocalizationManager.OnLanguageLoaded -= Refresh;
        }

        public void Refresh( Languages.Language any_lang = 0 )
        {
            GetComponent<Text>().text = LocalizationManager.GetString( alias.ToString() );
        }
    }
}
