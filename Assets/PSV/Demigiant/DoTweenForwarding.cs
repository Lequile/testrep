﻿#if TweenForwarding //|| true
using PSV;
using PSV.Audio;
using System;
using UnityEngine;

public static class DOVirtual
{
    public static AudioHandler DelayedCall( AudioHandler audio, DG.Tweening.TweenCallback callback, bool ignoreTimeScale = true )
    {
        audio.OnComplete( new Action( callback ) );
        return audio;
    }

    public static Coroutine DelayedCall( float delay, DG.Tweening.TweenCallback callback, bool ignoreTimeScale = true )
    {
        return DelayedCallHandler.Invoke( null, new Action( callback ), delay, ignoreTimeScale );
    }

    public static Coroutine Float( float from, float to, float duration, DG.Tweening.TweenCallback<float> onVirtualUpdate )
    {
        return CoroutineHandler.Start( new DoSmoothInstruction( from, to, duration, new Action<float>( onVirtualUpdate ) ), false );
    }

    public static void Kill( this Coroutine t, bool not_used = false )
    {
        CoroutineHandler.Stop( t );
    }
}

public class DOTween : DG.Tweening.DOTween
{
    public static new int KillAll( bool complete = false )
    {
        CoroutineHandler.StopAllOnScene();
        return DG.Tweening.DOTween.KillAll( complete );
    }

    public static int Kill( Coroutine coroutine, bool complete = false )
    {
        CoroutineHandler.Stop( coroutine );
        return 0;
    }
}
#endif