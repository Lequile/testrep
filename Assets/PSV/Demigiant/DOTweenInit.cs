﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace PSV
{
    public static class DOTweenInit
    {
        private static List<Tween> paused_tweens;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        public static void InitDefault()
        {
            AwakeStatic.Done( typeof( DOTweenInit ) );
            DOTween.Init( false, Application.isEditor ? ( bool? )null : true, LogBehaviour.Verbose )
                .SetCapacity( 200, 10 );
            PauseManager.OnPause += OnPause;
        }

        private static void OnPause( bool paused )
        {
            if (paused)
            {
                DOTween.PauseAll();
                paused_tweens = DOTween.PausedTweens();
            }
            else if (paused_tweens != null)
            {
                for (int i = 0; i < paused_tweens.Count; i++)
                {
                    paused_tweens[i].Play();
                }
                paused_tweens = null;
            }
        }
    }
}