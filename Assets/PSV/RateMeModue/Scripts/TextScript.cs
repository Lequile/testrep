﻿using UnityEngine;
using UnityEngine.UI;


namespace RateMePlugin
{
    [RequireComponent ( typeof ( Text ) )]
    public class TextScript : MonoBehaviour
    {
        private Text rateMeText;

        private void Awake ()
        {
            rateMeText = GetComponent<Text> ( );
        }

        private void OnEnable ()
        {
            rateMeText.text = RateMeModule.GetLocalisedText ( );
        }
    }
}
