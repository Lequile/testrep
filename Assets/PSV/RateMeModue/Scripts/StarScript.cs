﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


namespace RateMePlugin
{
    [RequireComponent ( typeof ( Image ) )]
    public partial class StarScript : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField]
        protected bool positive = false;

        protected Image starImage;

        public delegate void Callback (bool positive, float x);
        public static event Callback OnStarClicked;

        protected void Awake ()
        {
            starImage = GetComponent<Image> ( );
        }

        protected void OnDisable ()
        {
            OnStarClicked -= CheckIfActive;
        }

        protected void OnEnable ()
        {
            SetActive ( false );
            OnStarClicked += CheckIfActive;
        }

        private void CheckIfActive (bool positive, float x)
        {
            SetActive ( x >= this.transform.position.x );
        }

        protected virtual void SetActive (bool active)
        {
            Color col = starImage.color;
            col.a = active ? 1 : 100 / 255f;
            starImage.color = col;
        }

        public virtual void OnPointerDown (PointerEventData eventData)
        {
            OnStarClicked?.Invoke(positive, this.transform.position.x);
        }

    }
}