﻿#define BRAND_NEW_PROTOTYPE // Prototype 3 or newer
#define Prototype_4_2_OR_NEWER

using PSV;
using System.Collections;
//using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RateMePlugin
{

    public static class RateMeModule
    {
        public enum DialogState
        {
            Hided, ShowOnScene, ShowPrefab
        }

        private const string
            closed_var = "RateMeModule:closed",
            negative_var = "RateMeModule:negative",
            positive_var = "RateMeModule:positive";

        private const int
            negative_limit = 2,
            closed_limit = 3;
        private static int
            closed_val,
            negative_val,
            positive_val;

#if BRAND_NEW_PROTOTYPE && !Prototype_4_2_OR_NEWER
        public static Scenes
            rate_scene = Scenes.RateMeScene;
        private static Scenes 
            target_scene;           // TODO: Can be replaced to OffersManager.suspendedScene
#endif
        private static float
            time_interval = 60; //60

        private static float
            last_time = 0.0f; //to show dialogue after first call - set to zero to avoid early activation

        private static DialogState
            is_shown;

        public static bool
            positive = false; // Unlock Rate Application

        /// <summary>
        /// Called on close dialog. Boolean param is positive. 
        /// </summary>
        public static event System.Action<bool> OnCloseDialogue;


#if !NO_PSV_AUDIO
        private static PSV.Audio.PlayingSound playing_sound = PSV.Audio.PlayingSound.Empty;
#else
        private static AudioSource src;
#endif

        private const string clip_prefix = "rateme";


        static RateMeModule()
        {
            InitializeAudio();
            LoadResources();
            GetVariables();
            Subscribe();
        }


        public struct LocalisedData
        {
            public AudioClip audio;
            public string txt;
            public LocalisedData( AudioClip a, string t )
            {
                audio = a;
                txt = t;
            }

            public override string ToString()
            {
                return "[audio = " + ( audio != null ? audio.name : "null" ) + "], [txt = " + txt + "]";
            }
        }

        private static Dictionary<Languages.Language, LocalisedData> localised_data;


        public static GameObject
            rate_me_panel;

        private static void InitializeAudio()
        {
#if NO_PSV_AUDIO
            GameObject go = new GameObject( "RateMeAudio" );
            src = go.AddComponent<AudioSource>();
            GameObject.DontDestroyOnLoad( go );
#endif
        }

        private static void LoadResources()
        {
            localised_data = new Dictionary<Languages.Language, LocalisedData>();
            LoadTexts();
            LoadSounds();
        }

        private static void LoadSounds()
        {
            AudioClip[] res = Resources.LoadAll<AudioClip>( "RateMeModuleRes/" );
            if (res != null)
            {
                for (int i = 0; i < res.Length; i++)
                {
                    AudioClip clip = res[i];
                    string[] name = clip.name.Split( '_' );
                    if (name.Length > 1 && name[0].Equals( clip_prefix ))
                    {
                        Languages.Language ln = ( Languages.Language )System.Enum.Parse( typeof( Languages.Language ), name[1] );
                        if (System.Enum.IsDefined( typeof( Languages.Language ), ln ))
                        {
                            if (localised_data.ContainsKey( ln ))
                            {
                                LocalisedData d = localised_data[ln];
                                d.audio = clip;
                                localised_data[ln] = d;
                            }
                            else
                            {
                                localised_data.Add( ln, new LocalisedData( clip, "" ) );
                            }
                        }
                    }
                }
            }
        }

        private static void LoadTexts()
        {
            TextAsset txt = ( TextAsset )Resources.Load( "RateMeModuleRes/RateMeText", typeof( TextAsset ) );
            if (txt != null)
            {
                string[] items = System.Text.RegularExpressions.Regex.Split( txt.text, "\r\n|\r|\n" );
                for (int i = 0; i < items.Length; i++)
                {
                    int splitter = items[i].IndexOf( ":" );
                    if (splitter > 0)
                    {
                        string key = items[i].Substring( 0, splitter );
                        string val = items[i].Substring( splitter + 1 );

                        Languages.Language ln = ( Languages.Language )System.Enum.Parse( typeof( Languages.Language ), key );
                        if (System.Enum.IsDefined( typeof( Languages.Language ), ln ))
                        {
                            if (localised_data.ContainsKey( ln ))
                            {
                                LocalisedData d = localised_data[ln];
                                d.txt = val;
                                localised_data[ln] = d;
                            }
                            else
                            {
                                localised_data.Add( ln, new LocalisedData( null, val ) );
                            }
                        }
                    }
                }
            }
        }

        private static void LoadPrefab()
        {
            if (rate_me_panel == null)
            {
                GameObject go = Resources.Load<GameObject>( "RateMeModuleRes/RateMeCanvas" );
                if (!go)
                {
                    Debug.LogError( "RateMeModuleRes contains no RateMeCanvas prefab" );
                    return;
                }
                go.SetActive( false );
                rate_me_panel = GameObject.Instantiate( go );
                if (Application.isPlaying)
                    GameObject.DontDestroyOnLoad( rate_me_panel );
            }
        }

        private static bool IsLangAvailable( Languages.Language lang )
        {
            return localised_data != null && localised_data.ContainsKey( lang );
        }

        private static Languages.Language GetLang()
        {
            Languages.Language lang = Languages.GetLanguage();
            //set overrides
            if (!IsLangAvailable( lang ))
            {
                switch (lang)
                {
                    case Languages.Language.Ukrainian:
                    case Languages.Language.Belarusian:
                        lang = Languages.Language.Russian;
                        break;
                }
            }
            if (!IsLangAvailable( lang ))
            {
                lang = Languages.Language.English;
            }
            return lang;
        }

        public static string GetLocalisedText()
        {
            return localised_data[GetLang()].txt;
        }

        public static AudioClip GetLocalisedAudio()
        {
            var audio = localised_data[GetLang()].audio;
            if (!audio)
                audio = localised_data[Languages.Language.English].audio;
            return audio;
        }

        private static void GetVariables()
        {
            closed_val = PlayerPrefs.GetInt( closed_var, 0 );
            negative_val = PlayerPrefs.GetInt( negative_var, 0 );
            positive_val = PlayerPrefs.GetInt( positive_var, 0 );
        }

        private static void SetVal( string var, int val )
        {
            PlayerPrefs.SetInt( var, val );
            NextFrameCall.SavePrefs();
        }

        private static void Unsubscribe()
        {
            RateButtonScript.OnRateClicked -= OnRateClicked;
            CloseButton.OnClose -= OnClosed;
            StarScript.OnStarClicked -= OnStarClicked;
        }

        private static void Subscribe()
        {
            RateButtonScript.OnRateClicked += OnRateClicked;
            CloseButton.OnClose += OnClosed;
            StarScript.OnStarClicked += OnStarClicked;
        }

        private static IEnumerator DelayedRateClicked()
        {
            yield return new WaitForSeconds( 1 );
            OnRateClicked();
        }

        private static Coroutine d_rate = null;

        private static void OnStarClicked( bool _positive, float x )
        {
            positive = _positive;
            if (positive)
            {
                CoroutineHandler.Stop( d_rate );
                d_rate = CoroutineHandler.Start( DelayedRateClicked() );
            }
        }

        private static void OnClosed()
        {
            if (positive)
            {
                OnRateClicked();
            }
            else
            {
                closed_val++;
                SetVal( closed_var, closed_val );
                ShowDialogue( false, false, true );
            }
            if (OnCloseDialogue != null)
                OnCloseDialogue( positive );
        }

        private static void OnRateClicked()
        {
            CoroutineHandler.Stop( d_rate );
            d_rate = null;
            if (positive)
            {
                positive_val++;
                SetVal( positive_var, positive_val );
                RateApplication();
            }
            else
            {
                negative_val++;
                SetVal( negative_var, negative_val );
            }
            ShowDialogue( false, false, true );
            if (OnCloseDialogue != null)
                OnCloseDialogue( positive );
        }

        [System.Obsolete( "Deprecated! For set Offer show interval use override Interval() in offer. Or SetIntervalNotScene() for not offer logic." )]
        public static void SetInterval( float delay )
        {
            time_interval = delay; // Work only for prefab mode.
        }

        public static void SetIntervalNotScene( float delay )
        {
            time_interval = delay;
        }

        /// <summary>
        /// Show/Hide Rate Me Dialog without Switch to rate me scene.
        /// For call show dialog on switch to scene then use <see cref="RateMeOffer"/>
        /// </summary>
        public static bool ShowDialogue( bool show, bool forse, bool load_scene )
        {
            if (!Application.isMobilePlatform && !Application.isEditor)
                return false;

            bool res = false;

            if (forse || !show || CanShow( Time.time ))
            {
                if (show)
                {
                    positive = false;
                }

                if (IsVisible())
                {
                    last_time = Time.time + time_interval;
                }

                if (show)
                {
                    is_shown = load_scene ? DialogState.ShowOnScene : DialogState.ShowPrefab;
                }
                else
                {
#if Prototype_4_2_OR_NEWER
                    if (is_shown == DialogState.ShowOnScene)
                    {
                        load_scene = true;
                    }
                    else if (is_shown == DialogState.ShowPrefab)
                    {
                        load_scene = false;
                    }
#endif
                    is_shown = DialogState.Hided;
                }

#if BRAND_NEW_PROTOTYPE
                if (load_scene)
                {
#if Prototype_4_2_OR_NEWER
                    res = true;
#else
                    if (show)
                    {
                        if (rate_scene == Scenes.None)
                        {
                            Debug.LogError( "RateMeModule: no scene defined." );
                        }
                        else
                        {
                            Scenes _target_scene = SceneLoader.GetTargetScene();
                            if (_target_scene != rate_scene)
                            {
                                //if rate_me can be shown we tell scene loader to change destination and return false
                                target_scene = _target_scene;
                                SceneLoader.SetTargetScene( rate_scene );
                                res = true;
                            }
                            else
                            {
                                Debug.LogError( "RateMeModule: Do not call transition to RateMeScene directly! Use RateMeModule.ShowDialogue() before normal transition to the scene you need. RateMeModule will override transition order itself" );
                            }
                        }
                    }
                    else
                    {
                        if (SceneLoader.GetCurrentScene() == rate_scene)
                        {
                            SceneLoader.SwitchToScene( target_scene );
                            res = true;
                        }
                    }
#endif
                }
                else
#endif
                {
                    ActivatePanel( show );
                    res = true;
                }

                if (!show)
                {
                    PlaySound( false );
                }
            }
            return res;
        }

        public static void PlaySound( bool play )
        {
            if (play)
            {
#if !NO_PSV_AUDIO
                playing_sound = AudioController.Play( GetLocalisedAudio() ).Group( StreamGroup.VOICE ).Start();
#else
                bool mute =
#if BRAND_NEW_PROTOTYPE
                    !GameSettings.IsSoundsEnabled();
#else
					!AudioController.IsSoundsEnabled ( );
#endif
                src.clip = GetLocalisedAudio();
                src.mute = mute;
                src.Play();
#endif
            }
            else
            {
#if !NO_PSV_AUDIO
                playing_sound.Stop();
#else
                src.Stop();
#endif
            }
        }


        private static void ActivatePanel( bool show )
        {
#if !NO_PSV_ADS
            if (show)
                AdsManager.ResetLastInterstitialTime();
#endif
            LoadPrefab();
            rate_me_panel.SetActive( show );
        }

        private static bool CanShow( float t )
        {
            return IsOnline() && last_time < t && IsValidEvaluation();
        }

        public static bool IsValidEvaluation()
        {
            return positive_val < 1 && negative_val < negative_limit && closed_val < closed_limit;
        }

        private static bool IsOnline()
        {
            return Application.internetReachability != NetworkReachability.NotReachable;
        }

        public static bool IsVisible()
        {
            return is_shown != DialogState.Hided;
        }

        public static void RateApplication()
        {
            string url = ConstSettings.GetApplicationMarketURL();
            if (url.Length > 1)
            {
                Debug.Log( "RateMeDialogue: open URL=" + url );
                Application.OpenURL( url );
            }
            else
            {
                Debug.LogError( "RateMeDialogue: package_name is empty" );
            }
        }
    }
}