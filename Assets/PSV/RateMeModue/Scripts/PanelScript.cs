﻿//#define BRAND_NEW_PROTOTYPE

using PSV;
using UnityEngine;
using UnityEngine.UI;

namespace RateMePlugin
{
    [RequireComponent( typeof( Image ) )]
    public class PanelScript : MonoBehaviour, IOrderedView
    {
        private bool
            //dialogue_shown = false,
            animate = false;

        private Image
            img;
        public int order;

        private const float
            target_alpha = 100 / 255f;

        public void OnEnable()
        {
            ShowBackground();
            this.AddView( order );
            RateMeModule.PlaySound( true );
        }


        public void OnDisable()
        {
            this.RemoveView();
        }

        public void Awake()
        {
            img = GetComponent<Image>();
        }

        private void ShowBackground()
        {
            SetAlpha( -img.color.a );
            animate = true;
        }

        private void ActivatePanel( bool param )
        {
            this.transform.localScale = param ? Vector3.one : Vector3.zero;
        }

        private void Update()
        {
            if (animate)
            {
                float alpha = SetAlpha( Time.unscaledDeltaTime );
                if (alpha == target_alpha /*|| res == 0*/)
                {
                    animate = false;
                    //if (!dialogue_shown)
                    //{
                    //    this.gameObject.SetActive ( false );
                    //}
                }
            }
        }

        private float SetAlpha( float step )
        {
            Color col = img.color;
            col.a = Mathf.MoveTowards( col.a, target_alpha, step );
            img.color = col;
            return col.a;
        }


        void IOrderedView.OnVisibilityChanged( bool visible )
        {
            ActivatePanel( visible );
        }
    }
}
