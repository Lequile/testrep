﻿using PSV;
using PSV.SceneManagement;
using UnityEngine;

namespace RateMePlugin
{
    public abstract class RataMeBaseOffer : SceneOfferBase
    {
        public override int priority { get { return 50; } }

        public override bool IsEmpty()
        {
            return ( !Application.isMobilePlatform && !Application.isEditor )
                || base.IsEmpty();
        }

        public override bool CanBeShown( string current_scene )
        {
            return base.CanBeShown( current_scene )
#pragma warning disable 0618 // Тип или член устарел
                && ExtraConditions( current_scene, target_scene )
                && IsCanBeShown( current_scene )
#pragma warning restore 0618 // Тип или член устарел
                && RateMeModule.IsValidEvaluation();
        }

        public override OfferReply OnShow( string current_scene )
        {
            RateMeModule.OnCloseDialogue += OnCloseDialog;
            RateMeModule.ShowDialogue( true, true, true ); // Skip result and call force.
#pragma warning disable 0618 // Тип или член устарел
            OnShowDialog( current_scene, target_scene );
#pragma warning restore 0618 // Тип или член устарел
            return base.OnShow( current_scene ); // Reset timer and return OfferReply.ShownNoAds
        }

        private void OnCloseDialog(bool positive)
        {
            RateMeModule.OnCloseDialogue -= OnCloseDialog;
            OnComplete();
        }

        [System.Obsolete( "Deprecated! Use override CanBeShown() instead." )]
        protected virtual bool ExtraConditions( string current_scene, string target_scene )
        {
            return true;
        }

        [System.Obsolete( "Deprecated! Use override OnShow() instead." )]
        protected virtual void OnShowDialog( string current_scene, string target_scene )
        {

        }

        [System.Obsolete( "Deprecated! Use override CanBeShown() instead. Will be deleted in the future." )]
        public virtual bool IsCanBeShown( string current_scene )
        {
            return true;
        }
    }
}