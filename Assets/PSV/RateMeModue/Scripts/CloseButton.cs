﻿using UnityEngine.EventSystems;
using UnityEngine;
using PSV;

namespace RateMePlugin
{
    public class CloseButton : MonoBehaviour, IPointerClickHandler
    {
        public delegate void Callback();
        public static event Callback OnClose;

        public void OnPointerClick( PointerEventData eventData )
        {
            if (OnClose == null)
                SceneLoader.SwitchToScene( SceneLoader.first_scene );
            else
                OnClose();
        }

    }
}
