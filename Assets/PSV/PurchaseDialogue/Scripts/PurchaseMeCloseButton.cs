﻿using UnityEngine;
using System;

namespace PSV.PurchaseDialogue
{
    
    public class PurchaseMeCloseButton : ButtonClickHandler
    {

        public static event Action OnPurchaseClosed;

        protected override void OnButtonClick()
        {
            Close();
        }

        public void Close()
        {
            if (OnPurchaseClosed == null)
                SceneLoader.SwitchToScene( SceneLoader.first_scene );
            else
                OnPurchaseClosed();
        }
    }
}