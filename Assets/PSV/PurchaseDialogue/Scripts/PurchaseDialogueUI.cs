﻿using PSV.IAP;

namespace PSV.PurchaseDialogue
{
    public class PurchaseDialogueUI : DialoguePanel
    {
        public Products product = Products.SKU_ADMOB;


        public void OpenDialogue()
        {
            if (!BillingManager.IsProductPurchased( product ))
            {
                ShowPannel( true );
            }
        }

        public void CloseDialogue()
        {
            ShowPannel( false );
        }

        public void Purchase()
        {
            BillingManagerBase.OnPurchaseSucceded += OnPurchaseSucceded;
            BillingManager.Purchase( product );
        }

        private void OnPurchaseSucceded( ProductProperties prop )
        {
            if (prop.product == product.ToString())
            {
                CloseDialogue();
            }
        }

        protected override void ShowPannel( bool param, bool use_tween = true )
        {
            if (param)
            {
                PurchaseMeCloseButton.OnPurchaseClosed += CloseDialogue;
                PurchaseMeBuyButton.OnPurchaseProceed += Purchase;
            }
            else
            {
                PurchaseMeCloseButton.OnPurchaseClosed -= CloseDialogue;
                PurchaseMeBuyButton.OnPurchaseProceed -= Purchase;
                BillingManagerBase.OnPurchaseSucceded -= OnPurchaseSucceded;
            }
            base.ShowPannel( param, use_tween );
        }

    }
}
