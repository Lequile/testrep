﻿using PSV.IAP;
using PSV.SceneManagement;
using UnityEngine;

namespace PSV.PurchaseDialogue
{
    public abstract class PurchaseBaseOffer : SceneOfferBase
    {
        public string purchase_product = ProductProperties.admob_product_name;

        [System.Obsolete( "Deprecated! Use override Interval() instead. Will be deleted in the future." )]
        protected float show_interval;

        public override int priority { get { return 51; } }

        public override OfferReply OnShow( string current_scene )
        {
            PurchaseMeCloseButton.OnPurchaseClosed += OnComplete;
            PurchaseMeBuyButton.OnPurchaseProceed += ProceedWithPurchase;
#pragma warning disable 0618 // Тип или член устарел
            OnShowDialog( current_scene, target_scene );
#pragma warning restore 0618 // Тип или член устарел
            return base.OnShow( current_scene );
        }
        
        public override bool CanBeShown( string current_scene )
        {
            return AdsManager.IsAdsEnabled()
#pragma warning disable 0618 // Тип или член устарел
                && ExtraConditions( current_scene, target_scene )
                && IsCanBeShown( current_scene )
#pragma warning restore 0618 // Тип или член устарел
                && base.CanBeShown( current_scene );
        }


        private void ProceedWithPurchase()
        {
            BillingManagerBase.OnPurchaseSucceded += OnPurchaseSucceded;
            BillingManagerBase.Purchase( purchase_product );
        }


        private void OnPurchaseSucceded( ProductProperties prop )
        {
            if (prop.product == purchase_product && prop.type == ProductType.NonConsumable)
            {
                BillingManagerBase.OnPurchaseSucceded -= OnPurchaseSucceded;
                OnComplete();
            }
        }

        public override void OnComplete()
        {
            PurchaseMeCloseButton.OnPurchaseClosed -= OnComplete;
            PurchaseMeBuyButton.OnPurchaseProceed -= ProceedWithPurchase;
            base.OnComplete();
        }

        public override float Interval( string current_scene )
        {
#pragma warning disable 0618 // Тип или член устарел
            if (show_interval != default( float )) // Support obsolete interface. Will be removed in future
                return show_interval;
#pragma warning restore 0618 // Тип или член устарел
            return base.Interval( current_scene );
        }

        [System.Obsolete( "Deprecated! Use override CanBeShown() instead. Will be deleted in the future." )]
        protected virtual bool ExtraConditions( string current_scene, string target_scene )
        {
            return true;
        }

        [System.Obsolete( "Deprecated! Use override OnShow() instead. Will be deleted in the future." )]
        protected virtual void OnShowDialog( string current_scene, string target_scene )
        {

        }

        [System.Obsolete( "Deprecated! Use override CanBeShown() instead. Will be deleted in the future." )]
        public virtual bool IsCanBeShown( string current_scene )
        {
            return true;
        }
    }
}
