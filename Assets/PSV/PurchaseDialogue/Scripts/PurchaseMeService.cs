﻿#if false // Logic moved to PurchaseOfferBase
using UnityEngine;
using System.Collections;

namespace PSV.PurchaseDialogue
{
    using IAP;


    public class PurchaseMeService : MonoBehaviour
    {
        public Products
            product = Products.SKU_ADMOB;
        
        private float current_delay;
        private float last_show_time = 0.0f;

        private const string SECONDARY_INTERVAL_PREF = "PurchaseMeService:use_secondary";

        #region Singleton

        private static PurchaseMeService instance;

        public static PurchaseMeService Instance
        {
            get
            {
                if (instance == null)
                {
                    GameObject e_man = new GameObject( "PurchaseMeService", typeof( PurchaseMeService ) );
                }
                return instance;
            }
        }
        public static bool InstanceExist()
        {
            return instance;
        }

        void Awake()
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad( instance.gameObject );
                current_delay = PlayerPrefs.GetFloat( SECONDARY_INTERVAL_PREF, 60.0f );
            }
            else
            {
                Debug.LogError( "PurchaseMeService: not allowed multiple instances" );
            }
        }


        #endregion

        public static void SetInterval( float delay )
        {
            if (instance)
                instance.current_delay = delay;
            PlayerPrefs.SetFloat( "SECONDARY_INTERVAL_PREF", delay );
            NextFrameCall.SavePrefs();
        }
        /// <summary>
        /// Call this at the end of Game cycle (all levels complete, passed 2-3 game loops, 2-3 days since installation)
        /// </summary>
        [System.Obsolete( "Use SetInterval instead" )]
        public void SetSecondaryInterval( float delay = 120.0f )
        {
            SetInterval( delay );
        }

        public static void SetPurchasedProduct(string product_name )
        {

        }

        public bool ShowPurchaseDialogue()
        {
            bool res = false;
            if (Time.time > last_show_time + current_delay && ManagerGoogle.IsAdmobEnabled())
            {
                PurchaseMeCloseButton.OnPurchaseClosed += DialogueClosed;
                PurchaseMeBuyButton.OnPurchaseProceed += ProceedWithPurchase;
                last_show_time = Time.time;
                res = true;
            }
            return res;
        }

        private void ProceedWithPurchase()
        {
            BillingManagerBase.OnPurchaseSucceded += OnPurchaseSucceded;
            BillingManagerBase.Purchase( product );
        }


        private void OnPurchaseSucceded( ProductProperties prop )
        {
            if (prop.product == product && prop.type == ProductType.NonConsumable)
            {
                BillingManagerBase.OnPurchaseSucceded -= OnPurchaseSucceded;
                DialogueClosed();
            }
        }

        private void DialogueClosed()
        {
            PurchaseMeCloseButton.OnPurchaseClosed -= DialogueClosed;
            PurchaseMeBuyButton.OnPurchaseProceed -= ProceedWithPurchase;

            var offer = SceneLoader.FindOffer<PurchaseBaseOffer>();
            if (offer != null && offer.OfferScene() == SceneLoader.GetCurrentSceneName())
            {
                SceneLoader.SwitchToScene( SceneLoader.GetSuspendedSceneName() );
            }
        }
        
    }
}
#endif