﻿using UnityEngine;
using System;


namespace PSV.PurchaseDialogue
{

    public class PurchaseMeBuyButton : ButtonClickHandler
    {
        public static event Action OnPurchaseProceed;

        protected override void OnButtonClick()
        {
            Proceed();
        }


        public void Proceed()
        {
            if (OnPurchaseProceed == null)
                SceneLoader.SwitchToScene( SceneLoader.first_scene );
            else
                OnPurchaseProceed();
        }
    }
}
