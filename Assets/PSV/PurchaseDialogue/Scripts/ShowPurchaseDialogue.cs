﻿using UnityEngine;
using System.Collections;

namespace PSV.PurchaseDialogue
{

	public class ShowPurchaseDialogue :ButtonClickHandler
	{

		public PurchaseDialogueUI dialogue;

		protected override void Awake ()
		{
			base.Awake ( );
			if (dialogue == null)
			{
				dialogue = FindObjectOfType<PurchaseDialogueUI> ( );
			}
		}


		protected override void OnButtonClick ()
		{			
			if (dialogue != null)
			{
				dialogue.OpenDialogue ( );
			}
		}
	}
}
