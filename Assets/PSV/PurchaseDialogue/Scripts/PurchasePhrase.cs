﻿using UnityEngine;

namespace PSV.PurchaseDialogue
{
    public class PurchasePhrase : MonoBehaviour
    {
        public DataManager data_manager;
        private PSV.Audio.PlayingSound playing = Audio.PlayingSound.Empty;

        private void OnEnable()
        {
            if (data_manager != null)
            {
                AudioClip phrase = data_manager.GetPhrase();
                if (phrase != null)
                {
                    playing = AudioController.Play( phrase ).Group( StreamGroup.VOICE ).Start();
                    Debug.Log( "Play phrase " + phrase.name );
                }
            }
        }

        private void OnDisable()
        {
            playing.Stop();
        }
    }
}
