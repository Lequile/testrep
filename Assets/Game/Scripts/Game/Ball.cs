﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SnowPunk
{
	public partial class Ball : IPoolable
	{
		public bool isUsing { get; set; }

		public void OnBecameUnUsed()
		{
			PoolController.instance.InvokeOnBecameUnusedEvent(this);
		}
	}


	public partial class Ball : MonoBehaviour
	{
		[SerializeField] private Vector2 BallVelocity = new Vector2(24, 12);
		[SerializeField] private Vector2 startVelocity = new Vector2(24, 12);

		[SerializeField] private int damage;

		private Collider2D ballColider;
		private int countOfBallHeightToDissapear;

		[SerializeField] private float gravity = 24;
		
		public Vector2 shootPos;
		public float throwMultiplier;
		Rigidbody2D ballRB;

		private void Start()
		{
			ballRB = GetComponent<Rigidbody2D>();
			ballColider = GetComponent<Collider2D>();
		}

		private void OnBecameInvisible()
		{
			PoolController.instance.InvokeOnBecameUnusedEvent(this);
		}

		private void OnBecameVisible()
		{
			ResetSettings();
		}

		public void ResetSettings()
		{
			BallVelocity = startVelocity;
			BallVelocity *= throwMultiplier;
		}

		private void FixedUpdate()
		{
			if (transform.position.y < shootPos.y - ballColider.bounds.size.y * countOfBallHeightToDissapear) OnBecameUnUsed();
			BallVelocity.y -= gravity * Time.fixedDeltaTime;
			ballRB.velocity = BallVelocity;
		}

		private void OnTriggerEnter2D(Collider2D other)
		{
			if (other.gameObject.layer == LayerMask.NameToLayer("Enemies"))
			{
				other.GetComponent<Enemy>().TakeDamage(damage);
				OnBecameUnUsed();
			}
		}
	}
}