﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnowPunk
{
	public partial class EnemyBall : IPoolable
	{
		public bool isUsing { get; set; }

		public void OnBecameUnUsed()
		{
			PoolController.instance.InvokeOnBecameUnusedEvent(this);
		}
	}

	[RequireComponent(typeof(Rigidbody2D))]
	public partial class EnemyBall : MonoBehaviour
	{
		public Vector2 velocity;

		public float speed = 3;

		private void OnTriggerEnter2D(Collider2D other)
		{
			if (other.gameObject.layer == LayerMask.NameToLayer("Hippo"))
			{
				//other.GetComponent<Player>().TakeDamage();
				OnBecameUnUsed();
			}
		}
	}
}