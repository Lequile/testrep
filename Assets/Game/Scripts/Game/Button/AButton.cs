﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AButton : MonoBehaviour
{
    public bool isPressed;

    public void OnPointDown()
	{
        isPressed = true;
	}
    public void OnPointUp()
	{
		isPressed = false;
	}
}
