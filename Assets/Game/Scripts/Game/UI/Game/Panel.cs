﻿using UnityEngine;
using DG.Tweening;

namespace SnowPunk
{
	public class Panel : MonoBehaviour
	{
		RectTransform rectTransform;

		public virtual void Show()
		{
			rectTransform.localScale = Vector3.zero;
			rectTransform.DOScale(new Vector2(1, 1), 0.4f);
		}

		public virtual void Hide()
		{
			rectTransform.DOScale(new Vector2(0, 0), 0.4f);
		}
		private void Awake()
		{
			InitPanel();
		}

		protected virtual void InitPanel()
		{
			rectTransform = GetComponent<RectTransform>();
		}
	}
}