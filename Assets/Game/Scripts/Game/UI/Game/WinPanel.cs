﻿using UnityEngine;

namespace SnowPunk
{
	public class WinPanel : MonoBehaviour
	{
		[SerializeField] private Panel panel;
		[SerializeField] private Panel[] Stars;


		private void Start()
		{
			GamePlayEvents.OnPlayerWin.AddListener(ShowPanel);
		}

		private void ShowPanel(int playerHP)
		{
			panel.Show();
			for (int i = 0; i < playerHP; i++)
			{
				Stars[i].Show();
			}
		}
	}
}