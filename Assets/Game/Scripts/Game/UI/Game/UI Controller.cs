﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace SnowPunk
{
	public class Score : MonoBehaviour
	{

		private Text ScoreText;

		private void Start()
		{
			GamePlayEvents.OnEnemyKill.AddListener(UpdateScore);
		}

		private void UpdateScore(int score)
		{
			ScoreText.text = score.ToString();
		}

	}
}