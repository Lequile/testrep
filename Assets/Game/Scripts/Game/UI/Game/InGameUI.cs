﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnowPunk
{
	public class InGameUI : MonoBehaviour
	{
		private GameObject GameUI;


		private void Start()
		{
			GamePlayEvents.OnPlayerDie.AddListener(HideUI);
		}

		private void HideUI()
		{
			GameUI.SetActive(false);
		}
	}
}