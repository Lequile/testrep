﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnowPunk
{
	public class LosePanel : MonoBehaviour
	{
		[SerializeField] private Panel panel;

		private void Start()
		{
			GamePlayEvents.OnPlayerDie.AddListener(HidePanel);
		}

		private void HidePanel()
		{
			panel.Hide();
		}
	}
}