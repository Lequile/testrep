﻿using System.Collections.Generic;
using UnityEngine;

namespace SnowPunk
{
	public class UIHeartPanels : MonoBehaviour
	{
		[SerializeField] private List<Panel> Hearts;

		private void Awake()
		{
			GamePlayEvents.OnPlayerDamage.AddListener(OnPlayerDamage);
		}

		private void OnPlayerDamage(int playerHP)
		{
			print("heee");
			for (int i = 0; i < Hearts.Count; i++)
			{
				if (i < playerHP)
					Hearts[i].Show();
				else
				{
					Hearts[i].Hide();
				}
			}
		}

		private void HidePanels()
		{
			Hearts.ForEach(heart => heart.Hide());
		}

		private void UpdateHearts(int playerHP)
		{
			
		}
	}
}