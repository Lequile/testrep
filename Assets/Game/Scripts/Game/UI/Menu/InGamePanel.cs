﻿using System;
using UnityEngine;
using DG.Tweening;

namespace SnowPunk
{
	public class InGamePanel : PSV.DialoguePanel
	{
		protected override void TweenAnimation(bool show, Action complete)
		{
			if (show)
			{
				Sequence sequence = DOTween.Sequence();
				pannel.anchoredPosition = new Vector2(0, -pannel.rect.height);
				pannel.localScale = Vector3.zero;
				sequence.Append(pannel.DOAnchorPosY(-20, anim_duration));
				sequence.Insert(0, pannel.DOScale(1, 1));
			}
		}
	}
}