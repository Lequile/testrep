﻿using System;
using UnityEngine;
using DG.Tweening;

namespace SnowPunk
{
	public class ShopPanel : PSV.DialoguePanel
	{
		protected override void TweenAnimation(bool show, Action complete)
		{
			if (show)
			{
				pannel.localScale = Vector3.one;
				pannel.anchoredPosition = new Vector2(0, pannel.rect.height);
				Sequence sequence = DOTween.Sequence();
				sequence.Append(pannel.DOAnchorPosY(-50, anim_duration));
				sequence.Append(pannel.DOAnchorPosY(0, anim_duration));
			}
			else
			{
				Sequence sequence = DOTween.Sequence();
				sequence.Append(pannel.DOScale(Vector3.one * 1.07f, anim_duration));
				sequence.Append(pannel.DOScale(new Vector3(0, 0, 1), anim_duration)).OnComplete(() => complete());
			}
		}

	}
}