﻿using UnityEngine;
using UnityEngine.Events;

namespace SnowPunk
{
    public class GamePlayEvents : MonoBehaviour
    {
        public static IntEvent OnPlayerDamage = new IntEvent();
        public static IntEvent OnPlayerWin = new IntEvent();
        public static IntEvent OnEnemyKill = new IntEvent();
        public static UnityEvent OnPlayerDie = new UnityEvent();
        public static FloatEvent OnShootButtonReleased = new FloatEvent ();
    }

    public class IntEvent : UnityEvent<int>
	{
	}
    public class FloatEvent : UnityEvent<float>
    {
    }
}