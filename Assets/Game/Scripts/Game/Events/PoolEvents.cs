﻿using UnityEngine.Events;
using UnityEngine;

namespace SnowPunk
{
	public class PoolEvents<T> where T : MonoBehaviour, IPoolable
	{
		public UnityEvent<T> OnItemBecomeUnUsed;

	}

}