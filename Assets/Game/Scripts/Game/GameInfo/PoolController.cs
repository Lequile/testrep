﻿using System.Collections.Generic;
using UnityEngine;

namespace SnowPunk
{
    public class PoolController : MonoBehaviour
    {
        public static PoolController instance;

        public List<GetTypeBase> PoolList;
        private void Awake()
        {
            instance = this;
        }

        public T FindItem<T>() where T : MonoBehaviour, IPoolable
        {
            T poolItem = default(T);
            foreach (var item in PoolList)
            {
                if (typeof(T) == item.Type)
                {
                    poolItem = ((PoolObject<T>)item).GetFreeItem();
                }
            }
            return poolItem;
        }

        public void InvokeOnBecameUnusedEvent<T>(T unUsedItem) where T : MonoBehaviour, IPoolable
        {
            foreach (var item in PoolList)
            {
                if (item.Type == unUsedItem.GetType())
                {
                    PoolObject<T> pool = (PoolObject<T>)item;
                    pool.OnBecameUnUse.Invoke(unUsedItem);
                }
            }
        }


    }
}