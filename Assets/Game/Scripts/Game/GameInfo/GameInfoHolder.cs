﻿using UnityEngine;

namespace SnowPunk
{
    public class GameInfoHolder : MonoBehaviour
    {
        public static GameInfoHolder instance;
        public Transform[] SpawnPoints;

        public string[] AnimalName = { "Cat", "Dog", "Fox", "Hippo", "Leo", "Pig", "Puma", "Raccoon" };
        public string[] AnimalStatusInFamily = { "Grandpa", "Grandma", "Mama", "Papa" };

        public string GetRandomSkinName()
        {
            string rndName = AnimalName[Random.Range(0, AnimalName.Length)];
            string rndStatus = AnimalStatusInFamily[Random.Range(0, AnimalStatusInFamily.Length)];
            return rndName + rndStatus;
        }
    }
}
