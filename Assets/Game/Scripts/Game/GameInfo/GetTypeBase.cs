﻿using UnityEngine;
using UnityEngine.Events;

namespace SnowPunk
{
    public abstract class GetTypeBase : MonoBehaviour
    {
		

		protected System.Type type;
        public System.Type Type
		{
			get
			{
				return type;
			}
			protected set
			{
				type = value;
			}
		}
    }
}