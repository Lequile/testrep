﻿namespace SnowPunk
{
    public interface IPoolable
    {
        bool isUsing { get; set; }
        void OnBecameUnUsed();
    }
}