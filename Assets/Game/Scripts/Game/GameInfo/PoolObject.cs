﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SnowPunk
{

	public class OnBecameUnUseEvent<T> : UnityEngine.Events.UnityEvent<T>
	{

	}

	[Serializable]
	public class PoolObject<T> :  GetTypeBase where T : MonoBehaviour, IPoolable
	{
		public OnBecameUnUseEvent<T> OnBecameUnUse { get; } = new OnBecameUnUseEvent<T>();

		[SerializeField] GameObject ObjectToCreate;
		[SerializeField] protected List<T> PoolList = new List<T>();

		[SerializeField] int poolCount;



		public void SetUnUsedItem(T item)
		{
			item.gameObject.transform.parent = this.gameObject.transform;
			item.gameObject.SetActive(false);
			item.isUsing = false;
		}

		private void Start()
		{
			Type = typeof(T);
			if (ObjectToCreate.GetComponent<IPoolable>() == null)
			{
				Debug.LogError(nameof(ObjectToCreate) + " cannot be a pool item");
				return;
			}
			OnBecameUnUse.AddListener(SetUnUsedItem);
			for (int i = 0; i < poolCount; i++)
			{
				GameObject item = Instantiate(ObjectToCreate, transform.position, Quaternion.identity, transform);
				item.GetComponent<GameObject>();
				if (item.GetComponent<T>().Equals(null))
				{
					Debug.LogError("The created object" + item.name + "doesn't have a component \"" + typeof(T) + "\".");
				}
				CreatePoolItem(item);
				item.SetActive(false);
			}
		}

		protected virtual void CreatePoolItem(GameObject item)
		{
			PoolList.Add(item.GetComponent<T>());
		}

		public virtual T GetFreeItem()
		{
			print(typeof(T));
			T freeItem = null;
			foreach (var item in PoolList)
			{
				if (!item.isUsing)
				{
					freeItem = item;
				}
			}
			freeItem.gameObject.SetActive(true);
			return freeItem;
		}
	}
}