﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SnowPunk
{
    public abstract class ShootBase : MonoBehaviour
    {
		protected float reloadTime;
		public float ReloadTime
		{
			get
			{
				return reloadTime;
			}
			set
			{
				if (value > 0)
				{
					reloadTime = value;
				}
			}
		}

		public virtual void Shoot() {
			throw new System.NullReferenceException();
		}
	}
}
