﻿using UnityEngine;
using UnityEngine.Events;

namespace SnowPunk
{
	[System.Serializable]
	public class PlayerSnowBallShoot : EntityShootComponent
	{

		[SerializeField] Transform shootPos;
		[SerializeField] ShootBar shootBar;
		private float minThrowMultiplier = 0.3f;
		private float maxThrowMultiplier = 1f;
		private float throwMultiplier;

		private bool canShoot = true;

		public void Start()
		{
			GamePlayEvents.OnShootButtonReleased .AddListener(OnShootButtonReleased);
		}

		public void OnShootButtonReleased(float _throwMultiplier)
		{
			
			throwMultiplier = Mathf.Clamp(_throwMultiplier, minThrowMultiplier, maxThrowMultiplier);
			Shoot();
		}
		public override void Shoot()
		{
			
			if (canShoot)
			{
				canShoot = false;
				Ball ball = PoolController.instance.FindItem<Ball>();
				ball.gameObject.transform.parent = shootPos;
				ball.gameObject.transform.localPosition = Vector2.zero;
				ball.throwMultiplier = throwMultiplier;
				shootBar.DoReloadAnimation(ReloadTime, 
					delegate {
						PoolController.instance.InvokeOnBecameUnusedEvent(ball);
						canShoot = true;
				});
			}
		}

	}
}