﻿using UnityEngine;

namespace SnowPunk
{
	public abstract class EntityShootComponent : MonoBehaviour
	{

		private float projectileSpeedMultiplier = 0;
		public float ProjectileSpeedMultiplier
		{
			get
			{
				return projectileSpeedMultiplier;
			}
			set
			{
				if (value >= 0)
				{
					projectileSpeedMultiplier = value;
				}
			}
		}
		private float reloadTime;
		public float ReloadTime
		{
			get
			{
				return reloadTime;
			}
			set
			{
				reloadTime = value;
			}
		}
		public abstract void Shoot();
	}

	

}