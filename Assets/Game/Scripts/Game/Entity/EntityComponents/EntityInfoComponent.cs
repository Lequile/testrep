﻿using UnityEngine;

namespace SnowPunk
{
	public class EntityInfoComponent : MonoBehaviour
	{
		protected int startHP = 0;
		public int StartHP
		{
			get => startHP;
			set
			{
				if(value > 0)
				{
					startHP = value;
				}
			}
		}

		protected int currentHP;
		public virtual int CurrentHP
		{
			get => currentHP;
			set
			{
				if(value >= 0)
				{
					currentHP = value;
				}
				else
				{
					currentHP = 0;
				}
			}
		}

		protected virtual void Init()
		{
			CurrentHP = StartHP;
		}
	}
}