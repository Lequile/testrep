﻿using System.Collections;
using UnityEngine;

namespace SnowPunk
{
	public abstract class EntityMoveComponent : MonoBehaviour
	{
		protected Rigidbody2D entity2D;

		public Vector2 Velocity => entity2D.velocity;

		protected float speed = 1;

		public float Speed
		{
			get
			{
				return speed;
			}
			set
			{
				if (value > 0)
				{
					speed = value;
				}
			}
		}

		public void Init()
		{
			entity2D = GetComponent<Rigidbody2D>();
		}

		public virtual void ClampMove() { }

		public void SetVelocity(Vector2 velocity) => entity2D.velocity = velocity * speed;

		public void SetYVelocity(float yVelocity) => entity2D.velocity = new Vector2(entity2D.velocity.x, yVelocity *speed) ;

		public void SetXVelocity(float xVelocity) => entity2D.velocity = new Vector2(xVelocity * speed, entity2D.velocity.y );

		public void OnGameEnd() => SetVelocity(Vector2.zero);
	}
}