﻿using Spine.Unity;
using UnityEngine;

namespace SnowPunk
{
	public class EntityAnimationComponent : MonoBehaviour
	{
		[SerializeField] protected SkeletonAnimation entityAnimation;

		public string Skin
		{
			get
			{
				return entityAnimation.initialSkinName;
			}
			set
			{
				if (value != "")
					entityAnimation.skeleton.SetSkin(value);
			}
		}

		public void Init()
		{
			entityAnimation = GetComponent<SkeletonAnimation>();
		}

		public void SetAnimation(Vector2 velocity)
		{
			string nameOfAnimation = "";
			if (velocity != Vector2.zero)
				nameOfAnimation = "run";
			else
				nameOfAnimation = "Idle";
			entityAnimation.AnimationName = nameOfAnimation;
		}
	}
}