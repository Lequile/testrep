﻿namespace SnowPunk
{
	public class PlayerInfo : EntityInfoComponent
	{
		public int PointsToWin;
		public int currentPoints
		{
			get
			{
				return currentPoints;
			}
			set
			{
				if (currentPoints > PointsToWin)
				{
					GamePlayEvents.OnPlayerWin.Invoke(CurrentHP);
				}
			}
		}

		public override int CurrentHP
		{
			get => base.CurrentHP;
			set
			{
				if (value > 0)
				{
					currentHP = value;
					GamePlayEvents.OnPlayerDamage.Invoke(CurrentHP);
				}
				else
				{
					currentHP = 0;
					GamePlayEvents.OnPlayerDie.Invoke();
				}
			}
		}

	}
}