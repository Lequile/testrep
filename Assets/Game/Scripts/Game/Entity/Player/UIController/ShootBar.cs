﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace SnowPunk
{
	public class ShootBar : MonoBehaviour
	{
		[SerializeField] private GameObject shootBarObj;
		[SerializeField] private Image shootBarFill;
		[SerializeField] private Image shootBarBtn;

		[SerializeField] private float fillAmountFillSpeedMultiplier;

		private float minFillAmount = 0;
		private float maxFillAmount = 1;

		private float targetFillAmount;

		[SerializeField] private bool isReloading;


		private void Awake()
		{
			SetDefaultSettings();
		}
			
		private void Update()
		{
			if (shootBarObj.activeInHierarchy)
			{
				shootBarFill.fillAmount = Mathf.MoveTowards(shootBarFill.fillAmount, targetFillAmount, 0.1f * fillAmountFillSpeedMultiplier);
				if (shootBarFill.fillAmount >= maxFillAmount)
					targetFillAmount = minFillAmount;
				else if (shootBarFill.fillAmount <= minFillAmount)
					targetFillAmount = maxFillAmount;
			}
		}

		public void ShowShootBar()
		{
			if (isReloading) return;
			shootBarObj.SetActive(true);
			SetDefaultSettings();
		}

		private void SetDefaultSettings()
		{
			targetFillAmount = 1;
			shootBarFill.fillAmount = 0;
		}

		public void SendFillAmoundAsThrowMultiplier()
		{
			if (!isReloading && shootBarObj.activeInHierarchy)
			{
				isReloading = true;
				shootBarObj.SetActive(false);
				GamePlayEvents.OnShootButtonReleased.Invoke(shootBarFill.fillAmount);
			}
		}

		public void DoReloadAnimation(float reloadTime,System.Action onComplete)
		{
			shootBarBtn.fillAmount = 0;
			shootBarBtn.DOFillAmount(1, reloadTime)
				.OnComplete(
				delegate
				{
					isReloading = false;
					onComplete();
				});
		}
	}
}
