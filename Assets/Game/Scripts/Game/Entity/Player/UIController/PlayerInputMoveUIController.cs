﻿using UnityEngine;
using UnityEngine.Events;

namespace SnowPunk
{
	public enum InputStates
	{
		Pressed, Released
	}
	public class PlayerInputMoveUIController : EntityMoveComponent
	{
		public UnityEvent<Vector2> OnMove;

		private bool isUpKey;
		private bool isDownKey;



		public void UpKeyDown()
		{
			isUpKey = true; 
			RecalculateInput();
		}
		public void UpKeyUp()
		{
			isUpKey = false;
			RecalculateInput();
		}

		public void DownKeyDown()
		{
			isDownKey = true;
			RecalculateInput();
		}

		public void DownKeyUp()
		{
			isDownKey = false;
			RecalculateInput();
		}

		private void RecalculateInput()
		{
			Vector2 inputVector = new Vector2(0, BoolToInt(isUpKey) - BoolToInt(isDownKey));

			SetVelocity(inputVector);
		}

		private int BoolToInt(bool value)
		{
			return value ? 1 : 0;
		}
	}
}