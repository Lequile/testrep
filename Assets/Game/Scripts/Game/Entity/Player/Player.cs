﻿using UnityEngine;

namespace SnowPunk
{
	[RequireComponent(typeof(PlayerInfo))]
	[RequireComponent(typeof(PlayerInputMoveUIController))]
	public class Player : Entity
	{
		[SerializeField] PlayerProperty playerProperties;

		[SerializeField] new protected PlayerInfo InfoComponent;

		private void Start()
		{
			InitEntity();
		}

		protected override void InitEntity()
		{
			base.InitEntity();
			ShootComponent.ReloadTime = playerProperties.ReloadTime;
			MoveComponent.Speed = playerProperties.Speed;
			InfoComponent.StartHP = playerProperties.StartHP;
			InfoComponent.PointsToWin = playerProperties.PointToWin;
			AnimationComponent.Skin = playerProperties.skin;
		}

		private void Update()
		{
			AnimationComponent.SetAnimation(MoveComponent.Velocity);
		}
	}
}