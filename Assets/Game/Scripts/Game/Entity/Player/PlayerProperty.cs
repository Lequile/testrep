﻿using UnityEngine;

namespace SnowPunk
{
    [CreateAssetMenu(fileName = "NewPlayerProperty", menuName = "Create new Player Property Set")]
    public class PlayerProperty : PlayerPropertiesBase
    {
        public string skin = "Hippo";
    }
}
