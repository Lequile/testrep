﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class Player : Entity
//{
//	//Only 2 elements!
//	//Defines Max and Min, that entity can walk
//	public Transform[] VerticalBorders;
//	private float minYPos, maxYPos;


//	public static Player Instance;
//	[SerializeField] int currentPoints;
//	[SerializeField] private int startPoints = 0;
//	[SerializeField] int needPoints;
//	[SerializeField] Transform ShootPosition;
//	[SerializeField] Ball[] balls;


//	[SerializeField] float reloadTime;
//	[SerializeField] bool canShoot = true;
//	public float ThrowMultiplier;

//	[SerializeField] BaseUIButton UpButton;
//	[SerializeField] BaseUIButton DownButton;

//	float yAxis;


//	private void Start()
//	{
//		minYPos = VerticalBorders[0].position.y;
//		maxYPos = VerticalBorders[1].position.y;
//		if (maxYPos < minYPos)
//		{
//			float temp = minYPos;
//			minYPos = maxYPos;
//			maxYPos = temp;
//		}
//	}

//	public void ClampEntity()
//	{
//		float ClampedYPos = Mathf.Clamp(transform.position.y, minYPos, maxYPos);
//		entity2D.MovePosition(new Vector2(transform.position.x, ClampedYPos));
//	}





//	protected override void InitEntity()
//	{
//		base.InitEntity();
//		startHealth = 3;
//		speed = GameInfo.Instance.PlayerSpeed;
//		reloadTime = GameInfo.Instance.PlayerReloadTime;
//		currentPoints = startPoints;
//		currentHealth = startHealth;
//	}

//	public void Update()
//	{
//		yAxis = BoolToInt(UpButton.isPressed) - BoolToInt(DownButton.isPressed);
//		SetYVelocity(yAxis);
//		SetAnimationByVelocity();
//		ClampHippoMove();
//	}

//	int BoolToInt(bool value) => value == true ? 1 : 0;

//	private void ClampHippoMove()
//	{
//		transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, minYPosition, maxYPosition));
//	}

//	public void Shoot()
//	{
//		if (!canShoot) return;
//		Ball ball = PoolController.instance.GetItem<Ball>();
//		canShoot = false;
//		if (ThrowMultiplier < 0.3f) ThrowMultiplier = 0.3f;
//		ball.gameObject.transform.position = ShootPosition.position;
//		ball.throwMultiplier = ThrowMultiplier;
//	}
//	public void SetCanShoot()
//	{
//		canShoot = true;
//	}

//	public  void SetYVelocity(float yDirection)
//	{
//		entityRB.velocity = new Vector3(0, yDirection * speed, 0);
//	}
//	public void ResetVelocity()
//	{
//			entityRB.velocity = Vector3.zero;
//	}
//	public void AddPoints(int value)
//	{
//		currentPoints += value;
//		if(currentPoints >= needPoints)
//		{
//			GameInfo.Instance.ShowWinPanel();
//		}
//	}

//	public override void TakeDamage()
//	{
//		currentHealth--;
//		GameInfo.Instance.ShowHealth(currentHealth);
//		if (currentHealth <= 0) Die();
//	}

//	private void Die()
//	{
//		isAlive = false;
//		GameInfo.Instance.ShowLosePanel();
//	}

//	public bool CanShoot()
//	{
//		return canShoot;
//	}
	
//}
