﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public partial class Enemy : IPoolable
//{
//	public bool isUsing { get; set; }

//	public void OnBecameUnUsed()
//	{
//		PoolController.instance.EnemyEvent.OnItemBecomeUnUsed.Invoke(this);
//	}
//}


//public partial class Enemy : Entity
//{


//	//ONLY 0 or 1!!!
//	[SerializeField] int spawnPoint;
//	Vector3 target;
//	bool canShoot;

//	[SerializeField] Transform shootPos;
//	[SerializeField] ParticleSystem particles;
//	private float ballSpeedMultiplier;

//	int pointsTakenAfterKilling;


//	private void Start()
//	{

//		InitEntity();

//	}

//	private void OnBecameInvisible()
//	{
//		canShoot = false;
//	}

//	private void OnBecameVisible()
//	{
//		canShoot = true;
//	}

//	void SetRandomSkin()
//	{
//		int rndNum = Random.Range(0, AnimalName.Length);
//		string _randomName = AnimalName[rndNum];
//		rndNum = Random.Range(0, AnimalStatusInFamily.Length);
//		string _randomStatus = AnimalStatusInFamily[rndNum];

//		entitySkeletonAnimation.skeleton.SetSkin(_randomName + _randomStatus);
//	}

//	public void Respawn()
//	{
//		SetRandomSkin();
//		gameObject.SetActive(true);
//		transform.position = GameInfo.Instance.SpawnPoints[spawnPoint].transform.position;
//		StartCoroutine(GoToField());
//	}

//	IEnumerator GoToField()
//	{
//		InitEntity();
//		entityRB.velocity = Vector3.up * speed * -Mathf.Sign(transform.position.y);
//		yield return new WaitForSeconds(1f);
//		EnableEnemy();
//	}

//	protected override void InitEntity()
//	{
//		StopAllCoroutines();
//		startHealth = 1;
//		EnemyProperty[] enemyProperties = InLevelEnemyProterties.instance.enemyProperties;
//		int rnd = Random.Range(0, enemyProperties.Length);
//		EnemyProperty property = enemyProperties[rnd];
//		speed = property.Speed;
//		speed *= property.ProjectileSpeedMultiplier;
//		ballSpeedMultiplier = property.ProjectileSpeedMultiplier;
//		pointsTakenAfterKilling = rnd + 1;
//	}

//	private void EnableEnemy()
//	{
//		StartCoroutine(RandomMove());
//		StartCoroutine(TimedShoot());
//	}

//	protected void MoveToPosition()
//	{
//		Vector3 newPosition = Vector3.zero;
//		newPosition = Vector3.MoveTowards(transform.position, target, 0.02f);
//		entityRB.MovePosition(newPosition);
//	}

//	protected void Kill()
//	{
//		Player.Instance.AddPoints(pointsTakenAfterKilling);
//		StartCoroutine(RespawnAfterSeconds(3));
//		entityRB.velocity = new Vector2(50, 0);
//	}

//	private IEnumerator RespawnAfterSeconds(int seconds)
//	{
//		yield return new WaitForSeconds(seconds);
//		isAlive = true;
//		Respawn();
//	}

//	void SetNewTargetPosition()
//	{
//		target = new Vector3(transform.position.x, Random.Range(minYPosition, maxYPosition), transform.position.z);
//	}

//	void Shoot()
//	{
//		if (GameInfo.Instance.isPlaying)
//		{
//			EnemyBall enemyBall = GameInfo.Instance.GetEnemyBall();


//			Vector3 _hippoDirection = Player.Instance.gameObject.transform.position - transform.position;
//			_hippoDirection = _hippoDirection.normalized;
//			_hippoDirection.z = 0;
//			enemyBall.velocity = _hippoDirection * enemyBall.speed * ballSpeedMultiplier;
//			enemyBall.transform.position = shootPos.position;
//		}
//	}

//	private void Update()
//	{
//		SetAnimationByVelocity();
//		if (!GameInfo.Instance.isPlaying) StopAllCoroutines();
//		if (transform.position.y > maxYPosition)
//		{
//			entityRB.velocity = -Vector3.up * speed;
//		}
//		else if (transform.position.y < minYPosition)
//		{
//			entityRB.velocity = Vector3.up * speed;
//		}
//	}

//	IEnumerator TimedShoot()
//	{
//		while (isAlive && GameInfo.Instance.isPlaying)
//		{
//			yield return new WaitForSeconds(5f);
//			if (canShoot) Shoot();
//		}
//	}


//	IEnumerator RandomMove()
//	{
//		while (isAlive && GameInfo.Instance.isPlaying)
//		{
//			Vector3 _velocityAfterChanging;
//			float num = Random.Range(0f, 1f);
//			if (num > 0.7f) entityRB.velocity = -entityRB.velocity;
//			_velocityAfterChanging = entityRB.velocity;
//			if (num < 0.3f)
//			{
//				entityRB.velocity = Vector3.zero;
//				yield return new WaitForSeconds(1.5f);
//				entityRB.velocity = _velocityAfterChanging;
//			}
//			yield return new WaitForSeconds(1f);
//		}
//	}
//	public override void TakeDamage()
//	{
//		isAlive = false;
//		print("dead");
//		particles.Play();
//		if (--currentHealth <= 0)
//		{
//			Kill();
//		}
//	}

//}



