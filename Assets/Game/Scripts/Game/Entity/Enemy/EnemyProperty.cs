﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnowPunk
{
	[CreateAssetMenu(fileName = "NewEnemyProperty", menuName = "Create new Enemy Property Set")]
	public class EnemyProperty : EnemyPropertiesBase
	{
		public float timeToRespawn;
	}
}