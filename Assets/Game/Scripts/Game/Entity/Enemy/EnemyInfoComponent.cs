﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnowPunk
{
	public class EnemyInfoComponent : EntityInfoComponent
	{
		protected int pointsByKill;
		public int PointByKill
		{
			get => pointsByKill; 
			set 
			{ 
				if(value > 0)
				{
					pointsByKill = value;
				}
			}
		}

		protected float timeToRespawn;
		public float TimeToRespawn
		{
			get => timeToRespawn;

			set
			{
				if(value > 0)
				{
					timeToRespawn = value;
				}
			}
		}
	}
}
