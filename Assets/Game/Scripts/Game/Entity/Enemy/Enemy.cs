﻿using System.Collections;
using UnityEngine;

namespace SnowPunk
{

	public partial class Enemy : IPoolable
	{
		public bool isUsing { get; set; }

		public void OnBecameUnUsed()
		{
			PoolController.instance.InvokeOnBecameUnusedEvent(this);
		}
	}
	public partial class Enemy : Entity
	{
		protected new EnemyInfoComponent InfoComponent;
		private void Awake()
		{
			InitEnemy();
		}

		protected void InitEnemy()
		{
			EnemyProperty property = InLevelEnemyProterties.instance.GetRandomProperty();
			AnimationComponent.Skin = GameInfoHolder.instance.GetRandomSkinName();
			MoveComponent.Speed = property.Speed;
			ShootComponent.ProjectileSpeedMultiplier = property.ProjectileSpeedMultiplier;
		}

		public void TakeDamage(int damage)
		{
			InfoComponent.CurrentHP -= damage;
			if(InfoComponent.CurrentHP <= 0)
			{
				GamePlayEvents.OnEnemyKill.Invoke(InfoComponent.PointByKill);
				StartCoroutine(Respawn());	
			}
		}

		IEnumerator Respawn()
		{
			MoveComponent.SetVelocity(Vector2.right);
			
			yield return new WaitForSeconds(InfoComponent.TimeToRespawn * 1.2f);
			
			transform.position = InLevelEnemyProterties.instance.GetRandomSpawnPoint().position;
			
			if(transform.position.y > 0) MoveComponent.SetVelocity(Vector3.down);
			else MoveComponent.SetVelocity(Vector3.up);
		}

	}
}