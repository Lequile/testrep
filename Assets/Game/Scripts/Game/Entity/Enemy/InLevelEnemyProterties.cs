﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnowPunk
{
	public class InLevelEnemyProterties : MonoBehaviour
	{
		public static InLevelEnemyProterties instance;

		public EnemyProperty[] enemyProperties;

		public Transform[] SpawnPoint;

		public EnemyProperty GetRandomProperty()
		{
			return enemyProperties[Random.Range(0, enemyProperties.Length)];
		}

		public Transform GetRandomSpawnPoint()
		{
			return SpawnPoint[Random.Range(0, SpawnPoint.Length)];
		}

		private void Start()
		{
			instance = this;
		}

	}
}