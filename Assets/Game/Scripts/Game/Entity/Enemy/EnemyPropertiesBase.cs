﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnowPunk
{
    [CreateAssetMenu(fileName = "NewEnemyProperty", menuName = "Create new Enemy Property Set")]
    public class EnemyPropertiesBase : EntityProperties
    {
        public float TimeToMove;
        public float ProjectileSpeedMultiplier;
        public new float Speed => Speed;
    }
}