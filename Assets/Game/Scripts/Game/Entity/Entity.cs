﻿using UnityEngine;

namespace SnowPunk
{
	public abstract class Entity : MonoBehaviour
	{

		[SerializeField] protected EntityAnimationComponent AnimationComponent { get; private set; }
		[SerializeField] protected EntityMoveComponent MoveComponent { get; private set; }
		[SerializeField] protected EntityShootComponent ShootComponent { get; private set; }
		[SerializeField] protected EntityInfoComponent InfoComponent { get; private set; }

		private void Awake()
		{
			InitEntity();
		}

		protected virtual void InitEntity()
		{
			AnimationComponent = GetComponent<EntityAnimationComponent>();
			AnimationComponent.Init();
			MoveComponent = GetComponent<EntityMoveComponent>();
			MoveComponent.Init();
			InfoComponent = GetComponent<EntityInfoComponent>();
			ShootComponent = GetComponent<EntityShootComponent>();
		}

		

	}
}