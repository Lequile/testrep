﻿using UnityEngine;

namespace SnowPunk
{
    public class EntityProperties : ScriptableObject
    {
        public float Speed;
        public float ReloadTime;
    }
}