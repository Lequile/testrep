﻿#if !NO_PSV_ADS
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CAS;

namespace PSV.ADS
{
    public sealed class SmallBannerLifecycle : Core.SmallBannerAdLifecycle
    {
        /* 
         * Enable the required override logic. (Set #if true)
         * All regions can be not override the basic logic if it is not required!
         */

#if true // Enable banner lifecycle in project.
        [RuntimeInitializeOnLoadMethod]
        public static void StaticAwake()
        {
            AwakeStatic.Done( typeof( SmallBannerLifecycle ) );
            new SmallBannerLifecycle(); // Instance will be saved in base constructor
        }
#endif 
        
#if true // Visible banner.
        private readonly HashSet<string> not_allowed_scenes = new HashSet<string>()
        {
            Scenes.PurchaseMeNew,
            Scenes.RateMeScene,
        };

        protected override bool IsVisibleOnScene( string scene_name )
        {
            if (not_allowed_scenes.Contains( scene_name ))
                return false;
            return base.IsVisibleOnScene( scene_name ); // Base return default True.
        }
#endif

#if false // Postion banner
        /// <summary>
        /// Change here ad position for certain scenes, scenes not listed here will use default_position
        /// </summary>
        private readonly Dictionary<string, AdPosition>
            position_override_scenes = new Dictionary<string, AdPosition>()
            {
                //{ Scenes.MainMenu.ToString(), AdPosition.TopLeft }
            };

        protected override AdPosition GetPositionOnScene( string scene_name )
        {
            if (position_override_scenes.ContainsKey( scene_name ))
                return position_override_scenes[scene_name];
            //return AdPosition.Bottom_Right; // You can return default position here
            return base.GetPositionOnScene( scene_name ); // Base return default position.
        }
#endif
    }
}
#endif