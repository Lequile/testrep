namespace PSV
{
    /// <summary>This file is generated automatically</summary>
    public enum StreamGroup
    {
        FX = 0,
        VOICE = 1,
        MUSIC = 2,
    }
}
