namespace PSV
{
    /// <summary>This file can be generated automatically in PSVSettings</summary>
    public enum Scenes
    {
        None = 0,
        MainMenu = 1,
        RateMeScene = 2,
        PurchaseMeNew = 3,

        GameScene = 4,

        NewAdsDemo = 5, // For teset only
    }
}
