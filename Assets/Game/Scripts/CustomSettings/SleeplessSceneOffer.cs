﻿using System.Collections.Generic;

namespace PSV.SceneManagement
{
    [InitializeSceneOffer]
    public sealed class SleeplessSceneOffer : SleeplessSceneBaseOffer
    {
        /// <summary>
        /// List scenes on which the screen should not be turned off. Like cut-scenes.
        /// </summary>
        private readonly HashSet<string> slepless_scenes = new HashSet<string>()
        {
            //Scenes.Sample,
        };

        public override HashSet<string> ScenesList()
        {
            return slepless_scenes;
        }

        /* 
        * Enable required override logic. (Set #if true)
        * All regions can be not override the basic logic if it is not required!
        */

#if false // Sleep conditions for scenes
        /// <summary>
        /// Call for each scene.
        /// </summary>
        public override bool IsNotSleep( string target_scene )
        {
            /*
             * You can invert Contains slepless_scenes for allow sleep for specified scenes. 
             */
            return base.IsNotSleep( target_scene );
        }
#endif

#if false // Settings
        /// <summary>
        /// Disable logic SleeplessSceneOffer on Never Sleep Screen.
        /// Call once on initialization.
        /// </summary>
        public override bool IsNeverSleepScreen()
        {
            /*
             * Recommended leave False for allow the screen to sleep. 
             * And to define scenes on which the screen should not sleep.
             */
            return false;
        }
#endif
    }
}