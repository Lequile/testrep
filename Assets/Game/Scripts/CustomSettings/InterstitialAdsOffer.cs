﻿#if !NO_PSV_ADS
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace PSV.SceneManagement
{
    [InitializeSceneOffer]
    public sealed class InterstitialAdsOffer : ADS.Core.InterstitialAdsBaseOffer
    {
        /// <summary>
        /// list here scenes which wouldn't show big banner if we will leave them
        /// </summary>
        private readonly HashSet<string> not_allowed_interstitial = new HashSet<string>()
        {
            Scenes.RateMeScene,
            Scenes.PurchaseMeNew,
        };

        public override HashSet<string> ScenesList()
        {
            return not_allowed_interstitial;
        }

        /* 
         * Enable required override logic. (Set #if true)
         * All regions can be not override the basic logic if it is not required!
         */

#if false // Override show logic
        public override bool CanBeShown( string current_scene )
        {
            return base.CanBeShown( current_scene ); // check not contains current scene in list
        }

        protected override void OnShow( string current_scene )
        {

        }
#endif

#if false // Settings interstitial
        public override bool IsForceShow( string current_scene )
        {
            return base.IsForceShow( current_scene ); // Return false
        }

        public override bool IsShowHomeAds( string current_scene )
        {
            return base.IsShowHomeAds( current_scene ); // Return false
        }
#endif
    }
}
#endif