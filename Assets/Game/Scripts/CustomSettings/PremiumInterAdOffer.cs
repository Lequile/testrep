﻿using System.Collections;
using System.Collections.Generic;
using PSV.ADS;
using UnityEngine;

namespace PSV.SceneManagement
{
    [InitializeSceneOffer]
    public class PremiumInterAdOffer : PremiumInterAdBaseOffer
    {
        private readonly HashSet<string> showInterAfterScenes = new HashSet<string>
        {
            // Gameplay scene
        };

        public override HashSet<string> ScenesList()
        {
            return showInterAfterScenes;
        }
    }
}