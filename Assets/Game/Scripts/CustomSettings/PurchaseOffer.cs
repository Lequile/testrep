﻿using UnityEngine;
using System.Collections.Generic;
using PSV.IAP;

namespace PSV.PurchaseDialogue
{
    [SceneManagement.InitializeSceneOffer]
    public sealed class PurchaseOffer : PurchaseBaseOffer
    {
        /// <summary>
        /// List here scenes after which purchase dialog will be shown.
        /// </summary>
        private readonly HashSet<string> purchase_dialogue_after = new HashSet<string>()
        {
            //Scenes.MainMenu,
        };

        public override HashSet<string> ScenesList()
        {
            return purchase_dialogue_after;
        }

        public override string OfferScene()
        {
            return Scenes.PurchaseMeNew.ToString();
        }

        /* 
         * Enable required override logic. (Set #if true)
         * All regions can be not override the basic logic if it is not required!
         */

#if false // Override default settings.
        public PurchaseOffer()
        {
            // Base default product is SKU_ADMOB but you can set any other!
            purchase_product = Products.SKU_ADMOB.ToString();
        }
#endif

#if false // Override conditions of shown
        public override float Interval( string current_scene )
        {
            return base.Interval( current_scene ); // Base show interval equal 60 seconds but you can set any other!
        }
#endif

#if false // Result of shown
        public override bool CanBeShown( string current_scene )
        {
            /* Example
             * you can place here extra conditions 
             * if you want to show dialog after certain number of scenes passed - this counter can be increased here.
             */
            return base.CanBeShown( current_scene ); // Base logic should participate in the condition
        }

        public override OfferReply OnShow( string current_scene )
        {
            /* Example
             * you can manage here dialog shown condition
             */
            return base.OnShow( current_scene );
        }

        public override void OnComplete()
        {
            base.OnComplete();
        }
#endif
    }
}
