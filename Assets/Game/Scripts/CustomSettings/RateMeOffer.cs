﻿using UnityEngine;
using PSV;
using System.Collections.Generic;

namespace RateMePlugin
{
    [PSV.SceneManagement.InitializeSceneOffer]
    public sealed class RateMeOffer : RataMeBaseOffer
    {
        //private int rate_us_counter = 0;            //for counting unloading of the scenes in rate_me_after_end list
        //private const int rate_us_interval = 3;     //after which scene will be able to show dialog

        /// <summary>
        /// list here scenes which will call rate_me dialog on end
        /// </summary>
        private readonly HashSet<string> rate_me_after_end = new HashSet<string>()
        {
            //Scenes.MainMenu,
        };

        public override HashSet<string> ScenesList()
        {
            return rate_me_after_end;
        }

        public override string OfferScene()
        {
            return Scenes.RateMeScene.ToString();
        }

        /* 
        * Enable required override logic. (Set #if true)
        * All regions can be not override the basic logic if it is not required!
        */

#if false 
        public override float Interval( string current_scene )
        {
            return base.Interval( current_scene ); // Base show interval equal 60 seconds but you can set any other!
        }
#endif

#if false // Extra conditions of shown
        public override bool CanBeShown( string current_scene )
        {
            /* Example
             * can place here extra conditions 
             * if you want to show dialog after certain number of scenes passed - this counter can be increased here, 
             * but due to time limit in RateMe itself you should compare the counter by equals or more than value (counter >= val)
             * 
             * will work only if CanShowRateUs() returned true - only scenes in rate_me_after_end will cause calling this method
             * but still there is no guarantee that dialog will be shown each time (there is time limit and count of positive/negative rates and rejects)
             * 
             * return ++rate_us_counter >= rate_us_interval; //will work after first three scenes
             */

            return base.CanBeShown( current_scene ); // Base logic should participate in the condition
        }

        public override OfferReply OnShow( string current_scene )
        {
            /* Example
             * you can manage here dialog shown condition
             * 
             * rate_us_counter = 0;	//will reset counter to zero for keeping rate_us_interval actual
             */
            return base.OnShow( current_scene ); // Leave base logic it in place.
        }

        public override void OnComplete()
        {
            base.OnComplete(); // Leave base logic it in place.
        }
#endif
    }
}
